var uploadImage = require('uploader/uploadFile.js');

const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

// =====================时间戳=================
/** 
 * 和PHP一样的时间戳格式化函数 
 * @param {string} format 格式 
 * @param {int} timestamp 要格式化的时间 默认为当前时间 
 * @return {string}   格式化的时间字符串 
 */
function dateManager(format, timestamp) {
  var a, jsdate = ((timestamp) ? new Date(timestamp * 1000) : new Date());
  var pad = function (n, c) {
    if ((n = n + "").length < c) {
      return new Array(++c - n.length).join("0") + n;
    } else {
      return n;
    }
  };
  var txt_weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  var txt_ordin = { 1: "st", 2: "nd", 3: "rd", 21: "st", 22: "nd", 23: "rd", 31: "st" };
  var txt_months = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  var f = {
    // Day 
    d: function () { return pad(f.j(), 2) },
    D: function () { return f.l().substr(0, 3) },
    j: function () { return jsdate.getDate() },
    l: function () { return txt_weekdays[f.w()] },
    N: function () { return f.w() + 1 },
    S: function () { return txt_ordin[f.j()] ? txt_ordin[f.j()] : 'th' },
    w: function () { return jsdate.getDay() },
    z: function () { return (jsdate - new Date(jsdate.getFullYear() + "/1/1")) / 864e5 >> 0 },

    // Week 
    W: function () {
      var a = f.z(), b = 364 + f.L() - a;
      var nd2, nd = (new Date(jsdate.getFullYear() + "/1/1").getDay() || 7) - 1;
      if (b <= 2 && ((jsdate.getDay() || 7) - 1) <= 2 - b) {
        return 1;
      } else {
        if (a <= 2 && nd >= 4 && a >= (6 - nd)) {
          nd2 = new Date(jsdate.getFullYear() - 1 + "/12/31");
          return date("W", Math.round(nd2.getTime() / 1000));
        } else {
          return (1 + (nd <= 3 ? ((a + nd) / 7) : (a - (7 - nd)) / 7) >> 0);
        }
      }
    },

    // Month 
    F: function () { return txt_months[f.n()] },
    m: function () { return pad(f.n(), 2) },
    M: function () { return f.F().substr(0, 3) },
    n: function () { return jsdate.getMonth() + 1 },
    t: function () {
      var n;
      if ((n = jsdate.getMonth() + 1) == 2) {
        return 28 + f.L();
      } else {
        if (n & 1 && n < 8 || !(n & 1) && n > 7) {
          return 31;
        } else {
          return 30;
        }
      }
    },

    // Year 
    L: function () { var y = f.Y(); return (!(y & 3) && (y % 1e2 || !(y % 4e2))) ? 1 : 0 },
    //o not supported yet 
    Y: function () { return jsdate.getFullYear() },
    y: function () { return (jsdate.getFullYear() + "").slice(2) },

    // Time 
    a: function () { return jsdate.getHours() > 11 ? "pm" : "am" },
    A: function () { return f.a().toUpperCase() },
    B: function () {
      // peter paul koch: 
      var off = (jsdate.getTimezoneOffset() + 60) * 60;
      var theSeconds = (jsdate.getHours() * 3600) + (jsdate.getMinutes() * 60) + jsdate.getSeconds() + off;
      var beat = Math.floor(theSeconds / 86.4);
      if (beat > 1000) beat -= 1000;
      if (beat < 0) beat += 1000;
      if ((String(beat)).length == 1) beat = "00" + beat;
      if ((String(beat)).length == 2) beat = "0" + beat;
      return beat;
    },
    g: function () { return jsdate.getHours() % 12 || 12 },
    G: function () { return jsdate.getHours() },
    h: function () { return pad(f.g(), 2) },
    H: function () { return pad(jsdate.getHours(), 2) },
    i: function () { return pad(jsdate.getMinutes(), 2) },
    s: function () { return pad(jsdate.getSeconds(), 2) },
    //u not supported yet 

    // Timezone 
    //e not supported yet 
    //I not supported yet 
    O: function () {
      var t = pad(Math.abs(jsdate.getTimezoneOffset() / 60 * 100), 4);
      if (jsdate.getTimezoneOffset() > 0) t = "-" + t; else t = "+" + t;
      return t;
    },
    P: function () { var O = f.O(); return (O.substr(0, 3) + ":" + O.substr(3, 2)) },
    //T not supported yet 
    //Z not supported yet 

    // Full Date/Time 
    c: function () { return f.Y() + "-" + f.m() + "-" + f.d() + "T" + f.h() + ":" + f.i() + ":" + f.s() + f.P() },
    //r not supported yet 
    U: function () { return Math.round(jsdate.getTime() / 1000) }
  };

  return format.replace(/[\ ]?([a-zA-Z])/g, function (t, s) {
    var ret = "";
    if (t != s) {
      // escaped 
      ret = s;
    } else if (f[s]) {
      // a date function exists 
      ret = f[s]();
    } else {
      // nothing special 
      ret = s;
    }
    return ret;
  });
}


const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

module.exports = {
  formatTime: formatTime,
  fixedImgUrl,
  uploadImgManager,
  chooseImgAblumManager,
  dateManager,
  isEmpty,
  hasPrefix,
  hasSuffix,
  getImgWithFileName,
}

// 1.获取当前的图片地址
var imgBaseUrl = "https://chire.oss-cn-hangzhou.aliyuncs.com/";
function fixedImgUrl(params){
  
  var newUrl = "";
  
  if (!isEmpty(params.url)){        // 1. 判断是否有拼接参数
    newUrl = params.url;
    var hasP = hasPrefix(newUrl, "http");
    if (hasP == true){
      newUrl = params.url;
    } else {
      if (!isEmpty(params.width)) {
        newUrl = params.url + "?x-oss-process=image/resize,m_fill,w_" + params.width + ",h_" + params.height + ",limit_0/auto-orient,1/format,png";
      } else {
        newUrl = params.url
      }
    }
  } else {
    newUrl = params;
  }
  
  var str = "";
  var hasP = hasPrefix(newUrl, "http");

  if (hasP == false){
    str = decodeUnicode(imgBaseUrl + newUrl);
  } else {
    str = newUrl;
  }
  return str;

}


function decodeUnicode(str) {
  return encodeURI(str);
}


// 判断字符串是否为空
function isEmpty(obj) {
  if (typeof obj == "undefined" || obj == null || obj == "") {
    return true;
  } else {
    return false;
  }
}
// 判断字符串是否为某字符串开头
function hasPrefix(mainStr,str){
  var fdStart = mainStr.indexOf(str);
  if (fdStart == 0) {
    return true;
  } else if (fdStart == -1) {
    return false;
  }
}

// 判断字符串是否为某字符串结尾
function hasSuffix(str, target) {
  var start = str.length - target.length;
  var arr = str.substr(start, target.length);
  if (arr == target) {
    return true;
  }
  return false;
}




// 上传图片方法WX
function chooseImgAblumManager(count){
  return new Promise(function(resolve,reject){
    wx.chooseImage({
      count: count,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function(res) {
        resolve(res.tempFilePaths);
      },
      fail: function(res) {},
      complete: function(res) {},
    })
  });
}

// 上传图片到OSS
function uploadImgManager(filesList){
  return new Promise(function (resolve, reject) {
    // 0.获取容器
    var filesStrList = [];
    // 1.获取图片
    var tempFilePaths = filesList;
    // 2.获取当前时间
    var nowTime = "users"

    // 3.上传
    for (var i = 0; i < tempFilePaths.length; i++) {
      //显示消息提示框
      wx.showLoading({
        title: '上传中' + (i + 1) + '/' + tempFilePaths.length,
        mask: true
      })

      //上传图片

      uploadImage(tempFilePaths[i], 'smart/' + nowTime + '/',
        function (result) {
          filesStrList.push(result);
        
          wx.hideLoading();
          // 最后一张上传成功后进行抛出
          if (filesStrList.length == tempFilePaths.length) {
            resolve(filesStrList);
          }  else {
          }
        }, function (result) {
          wx.hideLoading()
        }
      )
    }
  });
}

// 根据图片后缀获取到图片信息
function getImgWithFileName(firstUrl) {
  // 2.第一张图片
  var firstImg = "";
  if (hasSuffix(firstUrl, "docx")) {
    firstImg = "doc";
  } else if (hasSuffix(firstUrl, "doc")) {
    firstImg = "doc";
  } else if (hasSuffix(firstUrl, "png")) {
    firstImg = "png";
  } else if (hasSuffix(firstUrl, "gif")) {
    firstImg = "gif";
  } else if (hasSuffix(firstUrl, "html")) {
    firstImg = "html";
  } else if (hasSuffix(firstUrl, "mp3")) {
    firstImg = "mp3";
  } else if (hasSuffix(firstUrl, "mp4")) {
    firstImg = "mp4";
  } else if (hasSuffix(firstUrl, "ppt")) {
    firstImg = "ppt";
  } else if (hasSuffix(firstUrl, "psd")) {
    firstImg = "psd";
  } else if (hasSuffix(firstUrl, "ttf")) {
    firstImg = "ttf";
  } else if (hasSuffix(firstUrl, "txt")) {
    firstImg = "txt";
  } else if (hasSuffix(firstUrl, "xls") || hasSuffix(firstUrl, "xlsx")) {
    firstImg = "xls";
  } else {
    firstImg = "com";
  }
  var mainImg = fixedImgUrl("smallprogream/file/" + firstImg + ".png");
  return mainImg;
}

