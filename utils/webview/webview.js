var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var shareUrl = options.url;
    that.shareUrl = shareUrl;

    var shareName = options.name;

    if (shareName){
      that.shareName = shareName;
    } else {
      that.shareName = "炽热教育";
    }
    
    // 1. 获取当前的地址
    var tempParams = options.params;
    if (appInstance.util.isEmpty(tempParams) != true){
      console.log("1");
      var str1 = tempParams.replace('#', '=');
      var mainUrl = options.url + "?" + str1;
      console.log(mainUrl);

      this.setData({
        url: mainUrl,
        shareUrl: mainUrl,
      })
    } else {
      console.log("2");
      console.log(options.url);
      this.setData({
        url: options.url,
        shareUrl: options.url,
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

    var mainShareUrl = "/utils/webview/webview?url="+this.shareUrl;
    var mainShareName = this.shareName;


    return {
      title: mainShareName,
      path:mainShareUrl,
      imageUrl:"http://chire.oss-cn-hangzhou.aliyuncs.com/9470Subject/%E7%A7%91%E7%9B%AE20190521163623.png?x-oss-process=image/resize,m_fill,w_50,h_50,limit_0/auto-orient,1/format,png",
    }
  }
})