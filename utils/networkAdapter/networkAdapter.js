
const baseURL = 'www.chire.net'  //base URL
const fixBaseUrl = "https://" + baseURL + "/";

var netToneMap = "https://www.chire.net/50toneMap.html";          // 五十音图

var HomeBanner = "ChireInter/Window/View/Window_Img_List.ashx";
var MottoList = "ChireInter/Motto/Motto_List.ashx";

// 登录注册
var UserLogin = "ChireInter/Login/User_Login.ashx";
var UserBalance = "ChireInter/Login/User_Balance.ashx";

// 科目
var SubjectList = "ChireInter/Subject/Subject_List.ashx";

// 活动列表
var ActivityList = "ChireInter/Activity/Activity_List.ashx";
var ActivityDetail = "ChireInter/Activity/Activity_Detail.ashx";
var ActivityImgDel = "ChireInter/Activity/Activity_Img_Del.ashx";
var ActivityDel = "ChireInter/Activity/Activity_Del.ashx";

// 学生
var StudentList = "ChireInter/Students/Students_List.ashx";
var StudentAdd = "ChireInter/Students/Students_Add.ashx";
var StudentUpdate = "ChireInter/Students/Students_Update.ashx";
var StudentSearch = "ChireInter/Students/Student_Search.ashx";
// 协议
var DelegateList = "ChireInter/Delegate/Delegate_List.ashx";
// 留言
var Wechat_Notice = "Wechat/Wechat_Notice_List.ashx";
// 老师登录
var Teacher_ThirdLogin = "ChireInter/Login/Teacher_ThirdLogin.ashx";

// 老师列表
var Teacher_list = "ChireInter/Teacher/Teacher_List.ashx";
var Teacher_Add = "ChireInter/Teacher/Teacher_Add.ashx";
var TeacherUpdate = "ChireInter/Teacher/Teacher_Update.ashx"

// 班级
var Class_List = "ChireInter/Class/Class_List.ashx";
var Class_Add = "ChireInter/Class/Class_Add.ashx";
var Class_Detail = "ChireInter/Class/Class_Detail.ashx";
var Class_Img_Del = "ChireInter/Class/Class_Img_Del.ashx";
var Class_Del = "ChireInter/Class/Class_Del.ashx";

// 修改个人信息
var Teacher_Edit = "ChireInter/Teacher/Teacher_Edit.ashx";

// 考试信息
var Exam_File_Add = "ChireInter/Exam/Exam_File_Add.ashx";
var Exam_File_List = "ChireInter/Exam/Exam_File_List.ashx";
var Exam_Add = "ChireInter/Exam/Exam_Add.ashx";
var Exam_Detail = "ChireInter/Exam/Exam_Detail.ashx";
var Exam_File_Detail = "ChireInter/Exam/Exam_File_Detail.ashx";
var Exam_Img_Del = "ChireInter/Exam/Exam_Img_Del.ashx";
var Exam_File_Update = "ChireInter/Exam/Exam_File_Update.ashx";
var Exam_Chart_List = "ChireInter/Exam/Exam_Chart_List.ashx";
var Exam_File_Del = "ChireInter/Exam/Exam_File_Del.ashx"

// 活动列表
var ActivityList = "ChireInter/Activity/Activity_List.ashx";
var ActivityAdd = "ChireInter/Activity/Activity_Add.ashx";

// 科目
var SubjectList = "ChireInter/Subject/Subject_List.ashx";
var SubjectDel = "ChireInter/Subject/Subject_Del.ashx";
var SubjectAdd = "ChireInter/Subject/Subject_Add.ashx";
var SubjectInfo = "ChireInter/Subject/Subject_Info.ashx";
var SubjectUpdate = "ChireInter/Subject/Subject_Update.ashx";

// wechat
var WechatInterface = "ChireInter/Wechat/Wechat_SmallProjectAuth.ashx";

// 添加事件

// 工具
var StudyToolTranslate = "ChireInter/Tool/Translate/Translate.ashx";
var ToolListAdd = "ChireInter/Tool/Tool_Add.ashx";
var ToolList = "ChireInter/Tool/Tool_List.ashx";
var ToolDetail = "ChireInter/Tool/Tool_Detail.ashx";
var Tool_Update = "ChireInter/Tool/Tool_Update.ashx"
// 签到
var Sign_Status = "ChireInter/Sign/Sign_Status.ashx";
var Sign_Login = "ChireInter/Sign/Sign_Login.ashx";
var Sign_Today_Class = "ChireInter/Sign/Sign_Today_Class.ashx";
// 事件
var Event_Add = "ChireInter/Event_calendar/Event_Add.ashx";
var Event_List = "ChireInter/Event_calendar/Event_List.ashx";

// 消息
var News_Type_Add = "ChireInter/News/News_Type_Add.ashx";
var News_Type_List = "ChireInter/News/News_Type_List.ashx";
var News_Add = "ChireInter/News/News_Add.ashx";
var News_List = "ChireInter/News/News_List.ashx";
var News_Type_Detail = "ChireInter/News/News_Type_Detail.ashx"
var News_type_Del = "ChireInter/News/News_Type_Del.ashx"
var News_type_Update = "ChireInter/News/News_Type_Update.ashx"
var News_Detail = "ChireInter/News/News_Detail.ashx";
var News_Update = "ChireInter/News/News_Update.ashx";
// 建议
var Contact = "ChireInter/Window/Contact/Window-Contact.ashx";

// 家长绑定
var Identity_Auth_Notice = "ChireInter/Identity/Identity_Auth_Notice.ashx";
var Identity_Auth = "ChireInter/Identity/Identity_Auth.ashx";

// 价目表
var PriceMenuList = "ChireInter/PriceMenu/PriceMenu_List.ashx";
var PriceMenuAdd = "ChireInter/PriceMenu/PriceMenu_Add.ashx";
var PriceMenuInfo = "ChireInter/PriceMenu/PriceMenu_Info.ashx";
var PriceMenuDel = "ChireInter/PriceMenu/PriceMenu_Del.ashx";
var PriceMenuUpdate = "ChireInter/PriceMenu/PriceMenu_Update.ashx";

// 充值
var RechargeList = "ChireInter/Recharge/Recharge_List.ashx";
var RechargeAdd = "ChireInter/Recharge/Recharge_Add.ashx";
var RechargeDetail = "ChireInter/Recharge/Recharge_Detail.ashx";

// 资质
var QualificationsAdd = "ChireInter/Qualifications/Qualifications_Add.ashx";
var QualificationsList = "ChireInter/Qualifications/Qualifications_List.ashx";
var QualificationsDelete = "ChireInter/Qualifications/Qualifications_Delete.ashx";

// Banner
var BannerAdd = "ChireInter/Banner/Banner_Add.ashx";
var BannerList = "ChireInter/Banner/Banner_List.ashx";
var BannerDel = "ChireInter/Banner/Banner_Del.ashx";
var BannerDetail = "ChireInter/Banner/Banner_Detail.ashx";
// 作业
var WorkAdd = "ChireInter/Work/Work_Add.ashx";
var workList = "ChireInter/Work/Work_List.ashx";
var workStudent = "ChireInter/Work/Work_Student_List.ashx";
var Work_Single_AllStudent = "ChireInter/Work/Work_Single_AllStudent.ashx";
var Work_Back = "ChireInter/Work/Work_Back.ashx";
var Work_Student_Back = "ChireInter/Work/Work_Student_Back.ashx"

// 认证
var AuthList = "ChireInter/Auth/Auth_List.ashx";
var AuthAdd = "ChireInter/Auth/Auth_Add.ashx";

// 订单
var Order_Student = "ChireInter/Order/Order_Single_List.ashx";

// 视频
var Video_List = "ChireInter/Video/video_list.ashx"
// 试听


// 1. function
function fetchManager(url,params){
  var fixUrl = dymicUrl(url);
  var newUrl = fixBaseUrl + fixUrl;
  var nParams = newParams(params);
  console.log(newUrl+"======"+nParams);
  return new Promise(function (resolve, reject){
    wx.request({
      url: newUrl,
      data: nParams,
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      method: 'POST',
      dataType: 'json',
      responseType: 'text',
      success: function(res) {
        if (res.data.errCode == 200){   // 表示请求成功
          resolve(res.data.data);
        } else {
          var err = res.data.errMsg;
          var infoError = { errCode : "-1", errMsg: err};
          resolve(infoError);
        }
      },
      fail: function(res) {
        // console.log(res);
      },
      complete: function(res) {
        console.log(res);
      },
    })

  });
}

// 2. 数据加密
const newParams = (params)=>{
  var appInstance = getApp();

  let smartParams = {};
  if (appInstance.util.isEmpty(appInstance.globalData.userInfo.open_id) != true){
    smartParams.id = appInstance.globalData.userInfo.open_id;
    smartParams.openid = appInstance.globalData.userInfo.open_id;
  }


  Object.assign(smartParams,params);
  if (appInstance.util.isEmpty(params.html) != true) {
    smartParams.html = encodeURI(params.html);
  }
  console.log(smartParams);
  return smartParams;
}

// 3. 返回获取字符串 返回url
const dymicUrl = (url)=>{
  if (url == 'HomeBanner'){
    return HomeBanner;
  } else if (url == 'MottoList'){
    return MottoList;
  } else if (url == 'SubjectList'){
    return SubjectList;
  } else if (url == 'ActivityList'){
    return ActivityList;
  } else if (url == 'StudentList'){
    return StudentList;
  } else if (url == 'DelegateList'){
    return DelegateList;
  } else if (url == 'Teacher_ThirdLogin'){
    return Teacher_ThirdLogin;
  } else if (url == 'Teacher_list'){
    return Teacher_list;
  } else if (url == 'Class_List'){
    return Class_List;
  } else if (url == 'Teacher_Edit'){
    return Teacher_Edit;
  } else if (url == 'Wechat_Notice'){
    return Wechat_Notice;
  } else if (url == 'Exam_File_Add'){
    return Exam_File_Add;
  } else if (url == 'Exam_File_List'){
    return Exam_File_List;
  } else if (url == 'Exam_Add'){
    return Exam_Add;
  } else if (url == 'ActivityList'){
    return ActivityList;
  } else if (url == 'ActivityAdd'){
    return ActivityAdd;
  } else if (url == 'SubjectList'){
    return SubjectList;
  } else if (url == 'WechatInterface'){
    return WechatInterface;
  } else if (url == 'UserLogin'){
    return UserLogin;
  } else if (url == 'StudyToolTranslate'){
    return StudyToolTranslate;
  } else if (url == 'Class_Add'){
    return Class_Add;
  } else if (url == 'Class_Detail'){
    return Class_Detail;
  } else if (url == 'Sign_Status'){
    return Sign_Status;
  } else if (url == 'Sign_Login'){
    return Sign_Login;
  } else if (url == 'StudentAdd'){
    return StudentAdd;
  } else if (url == 'StudentUpdate'){
    return StudentUpdate;
  } else if (url == 'Teacher_Add'){
    return Teacher_Add;
  } else if (url == 'SubjectDel'){
    return SubjectDel;
  } else if (url == 'SubjectAdd'){
    return SubjectAdd;
  } else if (url == 'SubjectInfo'){
    return SubjectInfo;
  } else if (url == 'Class_Img_Del'){
    return Class_Img_Del;
  } else if (url == 'ActivityDetail'){
    return ActivityDetail;
  } else if (url == 'ActivityImgDel'){
    return ActivityImgDel;
  } else if (url == 'Event_Add'){
    return Event_Add;
  } else if (url =='StudentSearch'){
    return StudentSearch;
  } else if (url == 'Sign_Today_Class'){
    return Sign_Today_Class;
  } else if (url == 'News_Type_Add'){
    return News_Type_Add;
  } else if (url == 'News_Type_List'){
    return News_Type_List;
  } else if (url == 'News_Add'){
    return News_Add;
  } else if (url == 'News_List'){
    return News_List;
  } else if (url == 'Contact'){
    return Contact;
  } else if (url == 'Identity_Auth_Notice'){
    return Identity_Auth_Notice;
  } else if (url == 'Identity_Auth'){
    return Identity_Auth;
  } else if (url == 'PriceMenuList'){
    return PriceMenuList;
  } else if (url == 'PriceMenuAdd'){
    return PriceMenuAdd;
  } else if (url == 'PriceMenuInfo'){
    return PriceMenuInfo;
  } else if (url == 'PriceMenuDel'){
    return PriceMenuDel;
  } else if (url == 'RechargeAdd'){
    return RechargeAdd;
  } else if (url == 'RechargeList'){
    return RechargeList;
  } else if (url == 'RechargeDetail'){
    return RechargeDetail;
  } else if (url == 'PriceMenuUpdate'){
    return PriceMenuUpdate;
  } else if (url == 'News_Type_Detail'){
    return News_Type_Detail;
  } else if (url =='News_type_Del'){
    return News_type_Del;
  } else if (url == 'News_type_Update'){
    return News_type_Update;
  } else if (url == 'ActivityDel'){
    return ActivityDel;
  } else if (url == 'SubjectUpdate'){
    return SubjectUpdate;
  } else if (url == 'Class_Del'){
    return Class_Del;
  } else if (url == 'Event_List'){
    return Event_List;
  } else if (url == 'QualificationsAdd'){
    return QualificationsAdd;
  } else if (url == 'QualificationsList'){
    return QualificationsList;
  } else if (url == 'QualificationsDelete'){
    return QualificationsDelete;
  } else if (url == 'BannerAdd'){
    return BannerAdd;
  } else if (url =='BannerList'){
    return BannerList;
  } else if (url == 'BannerDel'){
    return BannerDel;
  } else if (url == 'BannerDetail'){
    return BannerDetail;
  } else if (url == 'WorkAdd'){
    return WorkAdd;
  } else if (url =='workList'){
    return workList;
  } else if (url == 'ToolList'){
    return ToolList;
  } else if (url == 'ToolListAdd'){
    return ToolListAdd;
  } else if (url == 'workStudent'){
    return workStudent;
  } else if (url == 'UserBalance'){
    return UserBalance;
  } else if (url == 'Exam_Detail'){
    return Exam_Detail;
  } else if (url == 'Exam_File_Detail'){
    return Exam_File_Detail;
  } else if (url == 'Exam_Img_Del'){
    return Exam_Img_Del;
  } else if (url == 'Exam_File_Update'){
    return Exam_File_Update;
  } else if (url == 'Exam_Chart_List'){
    return Exam_Chart_List;
  } else if (url == 'ToolDetail'){
    return ToolDetail;
  } else if (url == 'Tool_Update'){
    return Tool_Update;
  } else if (url == 'AuthList'){
    return AuthList;
  } else if (url == 'AuthAdd'){
    return AuthAdd;
  } else if (url == 'NewsDetail'){
    return News_Detail;
  } else if (url == 'NewsUpdate'){
    return News_Update;
  } else if (url == 'TeacherUpdate'){
    return TeacherUpdate;
  } else if (url == 'Order_Student'){
    return Order_Student;
  } else if (url == 'Exam_File_Del'){
    return Exam_File_Del;
  } else if (url == 'Work_Single_AllStudent'){
    return Work_Single_AllStudent;

  } else if (url == 'Work_Back'){
    return Work_Back;
  } else if (url == 'Work_Student_Back'){
    return Work_Student_Back;
  } else if (url == 'Video_List'){
    return Video_List;
  }
  else {
    return url;
  }
}

module.exports = {
  fetchManager,       // 网络请求方法
  fixBaseUrl,
}
