var network = require('networkAdapter.js');
/*
  微信登录
*/
function wxLoginManager() {
  return new Promise(function (resolve, reject) {
    wx.login({
      success: function (res) {
        if (res.code) {
          resolve(res);
        } else {
          reject(false);
        }
      },
      fail: function (res) {
        reject(false);
      },
      complete: function (res) { },
    })
  });
};

/*
  本地登录方法
*/
function loginServerManager(){
  return new Promise(function (resolve, reject) {
    var that = this;
    network.fetchManager('Teacher_ThirdLogin', { "login_type": "0", "phone": "15669969617" }).then(res => {
      resolve(res);
  
      // // 1. 头像变更
      // let avatarInfo = {
      //   url: appInstance.globalData.userInfo.avatar,
      //   width: 150,
      //   height: 150
      // };

      // appInstance.globalData.userInfo.avatar = appInstance.appUntilManager("fixedImgUrl", avatarInfo);

      // this.setData({
      //   userInfo: appInstance.globalData.userInfo,
      // })
      // this.userInfo = appInstance.globalData.userInfo;
    });




  });
}


module.exports = {
  wxLoginManager,             // 微信登录方法
  loginServerManager,         // 本地登录方法
}