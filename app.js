var networkAdapter = require('utils/networkAdapter/networkAdapter.js')
var untilAdapter = require('utils/util.js')


// 枚举
var UserType = {
  User_TypeNormal: 0,             // 普通人
  User_TypeTeacher: 1,            // 老师
  User_TypeStudent: 2,            // 学生
  User_TypeParent: 3,             // 家长
  User_TypeAdmin: 4,              // 管理员
}
var mainLogo= "https://chire.oss-cn-hangzhou.aliyuncs.com/smallprogream/logo/logo.png";
var sublogo = "https://chire.oss-cn-hangzhou.aliyuncs.com/smallprogream/logo/sublogo.png";


//app.js
App({
  // 授权的模板id
  // 1. 消息中心的推送
  dingyueAllMsg: 'SaYc5JSVjcAsctAIcmWpVXmuTOD8onfRz5xDEIQVAcQ',          // 3.【推送消息】
  // 2. 作业通知 - 家长 学生
  dingyueStudentWork: "YoXlVo7khe_Czjx2lbhCewZQPGx4dYVratveJODOHZU",          // 【有】【作业通知】
  // 3.7点提醒今天有课
  dingyueStudentMorningMsg: "9rO6VxDMjQxEW46sGq8S0Y0TswvVKuJ9w9ZuS-liM_Q",   // 【有】【早起学习提醒】
  // 4. 家长绑定学生  - 学生
  dingyueStudentParentJoin: "r9UmJUJBHJLhYlDqjr41gYsMfT95GSX79O3tlswEF7A",     // 【有】2.【家长进行绑定】
  // 5. 学生签到 - 家长
  dingyuePrentSign: "TOe48jXKxVGVZRIu2DuNa3dWU_tyJXoqXE3QFDcezgM",               //【有】打卡提醒
  // 6. 学生忘记签到 - 学生
  dingyueNoSign:"dJo1DsrU3-NYu7ScU_rtEo1_pIaCX-wQ1aEPf1ILvRM",                 // 【有】忘记签到
  // 7.加入班级提醒
  dingyueAdminShenqing: "5vAoRRS1FEc8V3vAkpYN7anN4aTpidj58Nkg1m6eeBM",         // 【有】【加入班级申请】
  // 8.请假 
  dingyueAllQingjia: "KfBTT9oNW4DzHczsHYRtBct1h8Iwelvkki5rGarTlwk",           // 【有】请假-老师，家长 管理员都要知道
  // 9.添加课时不足
  dingyueAllKeshibuzu: "Tk69g6JL-nlLtf5g-ObrgLzyUAFm1CcfzT0ztEE4E6w",     // 【有】【课时不足】
  // 10.课时购买通知
  dingyueStudentKechengGoumai: "jKpMHrvoilDc4jKOFWmwytb1E-W5k--Fmae2HWpD-vM",// 【有】【课时购买】



  onLaunch: function () {
   
  },
 
  globalData: {
    userInfo: {
      id:"",                                                // 编号
      avatar: sublogo,                                      // 头像
      birthday:"",                                          // 生日
      gender: 1,                                            // 性别
      device: "",                                           //设备号
      hasLogin:true,                                        // 是否登录
      nick: "请登录",                                        // 昵称
      hasLogin:false,                                       // 表示是否登录
      open_id:"",                                            // openId
      user_type:-1,                                           // 登录类型
      login_type:"",                                        // 登录类型
      sex:"",
      money:0,
      phone: "", 
      teacherInfo: {},                                       // 老师信息
      studentInfo: {},                                       // 学生信息
      parentInfo: {}                                        // 家长信息
    }
  },

  // 网络请求方法
  appFetchManager:function(url,params){
    return new Promise(function (resolve, reject) {
      networkAdapter.fetchManager(url,params).then(res=>{
        resolve(res);
      });
    });
  },

  // 根据图片
  appUntilManager:function(method,params){
    if (method == "fixedImgUrl"){
      return untilAdapter.fixedImgUrl(params);
    }
  },
  util: require('./utils/util'),
  network: require('utils/networkAdapter/networkAdapter'),
  interfaceManager : require('utils/networkAdapter/interface.js'),

  _time: {}, //当前学期周数

  // 作业信息
  page_work:{
    workList:[],
    workUserList:[],
    workType:0,
  },
  // 课程时间
  page_class:{
    classTimeList:[],
  }
  ,
  tempHtml:"",




  
})