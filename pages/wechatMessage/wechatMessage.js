var appInstance = getApp();
var pageNumber = 1;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    wechatMessage: [],
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getMessageInfoManager(pageNumber);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    // 1. 页码自增
    pageNumber = pageNumber + 1;
    // 2. 
    this.getMessageInfoManager(pageNumber);
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },


  getMessageInfoManager:function(page){
    var that = this;
    console.log(page);
    appInstance.network.fetchManager('Wechat_Notice', { "page": page,"size":"20"}).then    (res => {
      var temp3 = [];
      if (this.wechatMessage){
        temp3 =[...this.wechatMessage, ...res.list];
      } else {
        temp3 = res.list;
      }
      console.log(res.list);
      this.wechatMessage = temp3;
      this.setData({
        wechatMessage: temp3,
      })
    });
  }
})