var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    student_header: appInstance.appUntilManager("fixedImgUrl","smallprogream/student/xs.png"),
    search_sign: appInstance.appUntilManager("fixedImgUrl","smallprogream/student/search-sign.png"),
    search_clear: appInstance.appUntilManager("fixedImgUrl", "smallprogream/student/clear.png"),
    common_up: appInstance.appUntilManager("fixedImgUrl", "smallprogream/student/up.png"),
    mainDisplay:true,
    studentList:[],
    studentMainList:[],     // 已经筛选过的学生信息
    srarchInput:"",         // 搜索框内的文字
    inputInfo:"",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getStudentList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },

  // main——最优
  bindOpenList: function (e) {
    var index = parseInt(e.currentTarget.dataset.index);
    console.log();
    this.data.studentList[index].display = !this.data.studentList[index].display;
    this.setData({
      studentList: this.data.studentList,
    })
  },


  // 进行获取我的学生
  getStudentList:function(e){
    var that = this;
    appInstance.appFetchManager("StudentList", "").then(res => {
      var tempArr = res.list;
      tempArr.map(function (item) {
        var items = item;
        items.display = false;
      });
      that.data.studentMainList = tempArr,
      that.setData({
        studentList: tempArr,
      })
    });
  },

  bindSearchInput:function(e){
    console.log(e);
    this.data.inputInfo = e.detail.value;
    if (appInstance.util.isEmpty(this.data.inputInfo) != true){
      this.setData({
        mainDisplay: false,
      })
    } else {

      this.setData({
        studentList:this.data.studentMainList,
        mainDisplay: true,
      })  
    }


    // this.setData({
    //   'header.inputValue': e.detail.value,
    //   'main.total': 0,
    //   'main.sum': 0,
    //   'main.page': 0,
    //   'main.message': '上滑加载更多',
    //   'testData': []
    // });
    // if (!this.data.messageObj.messageDisplay) {
    //   this.setData({
    //     'messageObj.messageDisplay': true,
    //     'messageObj.message': ''
    //   });
    // }
    // return e.detail.value;
  },
  searchBtnClick:function(e){
    this.searchStudentListManager();
  },

  // 进行获取我的学生
  searchStudentListManager: function (e) {
    
    var that = this;
    var params = { keyword: this.data.inputInfo};
console.log(params);
    appInstance.appFetchManager("StudentSearch", params ).then(res => {
      var tempArr = res.list;
      tempArr.map(function (item) {
        var items = item;
        items.display = false;
      });
      that.setData({
        studentList: tempArr,
      })
    });
  },


})