var appInstance = getApp();
var mainType = "";
var transferHtml = "";
Page({
  data: {
    alignType: "left",
    formats: {},
    readOnly: false,
    placeholder: '开始输入...',
    editorHeight: 300,
    keyboardHeight: 0,
    isIOS: false,
    chooseUserTypeHasShow: false,
    chooseColorType: "",
    // 颜色色值
    colorData: {
      //基础色相，即左侧色盘右上顶点的颜色，由右侧的色相条控制
      hueData: {
        colorStopRed: 255,
        colorStopGreen: 0,
        colorStopBlue: 0,
      },
      //选择点的信息（左侧色盘上的小圆点，即你选择的颜色）
      pickerData: {
        x: 0, //选择点x轴偏移量
        y: 480, //选择点y轴偏移量
        red: 0,
        green: 0,
        blue: 0,
        hex: '#000000'
      },
      //色相控制条的位置
      barY: 0
    },
    rpxRatio: 1 //此值为你的屏幕CSS像素宽度/750，单位rpx实际像素
  },
  readOnlyChange() {
    this.setData({
      readOnly: !this.data.readOnly
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // 1. 获取色值
    //设置rpxRatio
    const that = this
    wx.getSystemInfo({
      success(res) {
        that.setData({
          rpxRatio: res.screenWidth / 750
        })
      }
    })


    const platform = wx.getSystemInfoSync().platform
    const isIOS = platform === 'ios'
    this.setData({
      isIOS
    })
    this.updatePosition(0)
    let keyboardHeight = 0
    wx.onKeyboardHeightChange(res => {
      if (res.height === keyboardHeight) return
      const duration = res.height > 0 ? res.duration * 1000 : 0
      keyboardHeight = res.height
      setTimeout(() => {
        wx.pageScrollTo({
          scrollTop: 0,
          success() {
            that.updatePosition(keyboardHeight)
            that.editorCtx.scrollIntoView()
          }
        })
      }, duration)

    })


    // 0.获取传入的type
    if (appInstance.util.isEmpty(options.type) != true) {
      mainType = options.type;
    }

      if (appInstance.util.isEmpty(appInstance.tempHtml) != true) {
        transferHtml = appInstance.tempHtml;
        appInstance.tempHtml = "";
        console.log(appInstance.tempHtml);
      }
  },
  updatePosition(keyboardHeight) {
    const toolbarHeight = 60
    const {
      windowHeight,
      platform
    } = wx.getSystemInfoSync()
    let editorHeight = keyboardHeight > 0 ? (windowHeight - keyboardHeight - toolbarHeight) : (windowHeight - toolbarHeight)
    this.setData({
      editorHeight,
      keyboardHeight
    })
  },
  calNavigationBarAndStatusBar() {
    const systemInfo = wx.getSystemInfoSync()
    const {
      statusBarHeight,
      platform
    } = systemInfo
    const isIOS = platform === 'ios'
    const navigationBarHeight = isIOS ? 44 : 48
    return statusBarHeight + navigationBarHeight
  },
  onEditorReady() {
    const that = this
    wx.createSelectorQuery().select('#editor').context(function(res) {
      that.editorCtx = res.context;


      console.log("====初始化");
      console.log(transferHtml);
      // 判断是否有传入的值
      if (appInstance.util.isEmpty(transferHtml) != true) {
        that.editorCtx.setContents({
          html: transferHtml,
          success: (res) => {
            console.log(res);
            transferHtml = "";
            appInstance.tempHtml = "";
          },
          fail: (res) => {
            console.log(res);
          }
        })
      }
    }).exec()
  },

  format(e) {
    console.log(e);
    let {
      name,
      value
    } = e.target.dataset
    if (!name) return
    console.log('format', name, value)
    if (name == "align") {
      this.setData({
        alignType: value,
      })
    }
    this.editorCtx.format(name, value)

  },
  // 输入内容
  onStatusChange(e) {
    const formats = e.detail
    this.setData({
      formats
    })
  },
  // 失去光标
  blur() {
    this.editorCtx.blur()
  },
  //清除样式
  removeFormat() {
    this.editorCtx.removeFormat()
  },
  // 撤销
  undoFormt() {
    this.editorCtx.undo()
  },
  // 清空
  clear() {
    this.editorCtx.clear({
      success: function(res) {
        console.log("clear success")
      }
    })
  },
  //插入分割线
  insertDivider() {
    this.editorCtx.insertDivider({
      success: function() {
        console.log('insert divider success')
      }
    })
  },
  // 插入日期
  insertDate() {
    const date = new Date()
    const formatDate = `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()}`
    this.editorCtx.insertText({
      text: formatDate
    })
  },

  // 插入图片
  insertImage() {
    var that = this;
    // 1. 选择图片
    appInstance.util.chooseImgAblumManager(1).then(res => {
      // 进行上传
      appInstance.util.uploadImgManager(res).then(res => { // 2. 上传图片
        var selectedImg = res;

        console.log(selectedImg[0], '图片上传之后的数据')
        that.editorCtx.insertImage({
          src: selectedImg[0],
          success: function() {
            console.log('insert image success')
          }
        })
      });
    });
  },

  // 颜色选择
  colorChooseManager: function(res) {
    console.log(res);
    this.data.chooseColorType = res.currentTarget.id;
    this.showDialog();
  },
  //选择改色时触发（在左侧色盘触摸或者切换右侧色相条）
  onChangeColor(e) {
    //返回的信息在e.detail.colorData中
    this.setData({
      colorData: e.detail.colorData
    })
  },
  /*** 绑定用户状态*/
  // 打开绑定用户弹窗
  showDialog: function() {
    this.setData({
      chooseUserTypeHasShow: true,
    })
  },
  // 关闭绑定用户弹窗
  closeDialog: function() {
    this.setData({
      chooseUserTypeHasShow: false,
    })
  },
  colorChooseSuccessed: function(res) {
    this.closeDialog();
    var colorHex = this.data.colorData.pickerData.hex;

    var info = {};
    if (this.data.chooseColorType == "bgcolor") {
      info = {
        target: {
          dataset: {
            name: "backgroundColor",
            value: colorHex
          }
        }
      }
    } else if (this.data.chooseColorType == "color") {
      info = {
        target: {
          dataset: {
            name: "color",
            value: colorHex
          }
        }
      };
    }

    this.format(info);
  },

  //  添加
  editAddManager: function() {
    // 
    this.editorCtx.getContents({
      success: function(res) {
        console.log('insert divider success')

        // 4. 返回
        var pages = getCurrentPages();
        var prevPage = pages[pages.length - 2];
        var that = this;
        if (mainType == "activity") {
          prevPage.setData({
            desInput: res.html,
          })
        } else if (mainType == "news") {
          prevPage.setData({
            newsDetail: res.html,
          })
        } else if (mainType == "banner") {
          prevPage.setData({
            desInput: res.html,
          })
        } else if (mainType == "work") {
          prevPage.setData({
            tempBeizhu: res.html,
          })
        } else if (mainType == "studentWork"){
          prevPage.setData({
            workUploadHtml: res.html,
          })
        }
        wx.navigateBack({
          delta: 1,
        });



        console.log(res)
      }
    })
  }
})