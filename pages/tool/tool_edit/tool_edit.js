var appInstance = getApp();
var transferId = "";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imageList:[],
    nameInput:"",
    toolIndexInput:"",
    switchInput:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (appInstance.util.isEmpty(options.id) != true){
      transferId = options.id;
      this.setData({
        transferId:options.id,
      })
      this.sendRequestToGetToolDetail();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },

  nameInputManager :function(e){
    this.data.nameInput = e.detail.value;
  },
  toolIndexInputManager:function(e){
    this.data.toolIndexInput = e.detail.value;
  },

  // 图片选择器
  chooseFiles: function (e) {
    var that = this;
    // 1. 选择图片
    appInstance.util.chooseImgAblumManager(9).then(res => {
      // 进行上传
      appInstance.util.uploadImgManager(res).then(res => { // 2. 上传图片
        that.imageList = res;
        that.setData({
          imageList: res,
        })
      });
    });
  },

  // 图片点击
  imgSelectedManager: function (e) {
    var selectedImgUrl = e.currentTarget.id;
    console.log(selectedImgUrl);
    var that = this;
    wx.showActionSheet({
      itemList: ['查看图片', '删除图片'],
      success: function (res) {
        if (!res.cancel) {
          if (res.tapIndex == 0) { // 图片放大
            that.imgShowsManager(selectedImgUrl);
          } else if (res.tapIndex == 1) { // 删除图片
            if (appInstance.util.isEmpty(transferId) == true) {
              that.imgDeleteManager(selectedImgUrl);
            } else {
              that.imgDeleteManager(selectedImgUrl);
            }
          }
        }
      }
    })
  },
  // 图片放大
  imgShowsManager: function (selectedImg) {
    var that = this;
    console.log(this.data.imageList);
    wx.previewImage({
      current: selectedImg,
      urls: that.data.imageList,
    })
  },
  // 删除图片
  imgDeleteManager: function (selectedImg) {
    var that = this;
    var tempImgList = [];
    tempImgList = this.data.imageList;

    var selectedIndex = tempImgList.indexOf(selectedImg);
    tempImgList.splice(selectedIndex, 1);

    this.imageList = tempImgList;
    this.setData({
      imageList: tempImgList,
    })
  },

  sendRequestToAddInfo:function(){
    var imgs = this.data.imageList[0];
    var params = { name: this.data.nameInput, img: imgs, index: this.data.toolIndexInput ,
      important: this.data.switchInput == false?0:1};
    appInstance.network.fetchManager('ToolListAdd', params).then(res => {
      wx.navigateBack({});
    });
  },
  addManager:function(){
    this.sendRequestToAddInfo();
  },
  deleteManager:function(){

  },
  switchChanged:function(e){
    this.setData({
      switchInput:e.detail.value,
    })
  },



 /**
   * 获取当前的工具详情
   */
  sendRequestToGetToolDetail:function(){
    var params = {
      toolId:transferId,
    };
    var that = this;
    appInstance.network.fetchManager('ToolDetail', params).then(res => {
      var infoImg = res.img;
      var tempImgList = [];
      tempImgList.push(appInstance.appUntilManager("fixedImgUrl", infoImg));
      that.setData({
        nameInput:res.name,
        toolIndexInput:res.index,
        switchInput:(res.important == 0?false:true),
        imageList: tempImgList,
      })
    });
  },

  /**
     * 修改当前工具
     */
  sendRequestToGetToolUpdate: function () {
    var imgs = this.data.imageList[0];
    var params = {
      toolId:transferId,
      name: this.data.nameInput, img: imgs, index: this.data.toolIndexInput,
      important: this.data.switchInput == false ? 0 : 1
    };
    var that = this;
    appInstance.network.fetchManager('Tool_Update', params).then(res => {
      wx.navigateBack({
        
      });
    });
  },

  updateManager:function(){
    this.sendRequestToGetToolUpdate()
  }

})