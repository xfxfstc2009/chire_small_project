var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    toolList:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.sendRequestGetList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  actionManager:function(){
    wx.navigateTo({
      url: 'tool_edit/tool_edit',
    })
  },
  itemTapManager:function(e){
    wx.navigateTo({
      url: 'tool_edit/tool_edit?id='+e.currentTarget.id,
    })
  },

  sendRequestGetList: function () {
    var params = {  };
    var that = this;
    appInstance.network.fetchManager('ToolList', params).then(res => {
      var tempList = res.list;
      tempList.map(function (item) {
        let imgInfo = {
            url: item.img,
            width: 200,
            height: 200
          };
      

        item.img = appInstance.appUntilManager("fixedImgUrl", imgInfo);
      });
      that.setData({
        toolList: tempList,
      })
    });
  },
})