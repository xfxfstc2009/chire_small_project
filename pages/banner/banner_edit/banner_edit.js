var appInstance = getApp();
var transferId = "";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 名称输入
    nameInput: "",
    // 详情输入
    desInput: "",
    // 图片列表
    imageList: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (appInstance.util.isEmpty(options.id) != true){
      transferId = options.id;
      this.setData({
        transferId:options.id,
      })
      this.sendRequestToGetBannerInfo();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  /**
   * 名称输入
   */
  nameInputManager: function(e) {
    this.data.nameInput = e.detail.value;
  },
  /**
   * url输入
   */
  directInputManager: function(e) {
    this.data.desInput = e.detail.value;
  },
  actionToAddDetail:function(e){
    console.log("====" + this.data.desInput);
    appInstance.tempHtml = this.data.desInput;
    wx.navigateTo({
      url: '../../editor/editor?type=banner',
    })
  },


  /**
   * 选择图片
   */
  chooseFiles: function () {
    var that = this;
    // 1. 选择图片
    appInstance.util.chooseImgAblumManager(1).then(res => {
      // 进行上传
      appInstance.util.uploadImgManager(res).then(res => {          // 2. 上传图片
        that.imageList = res;
        that.setData({
          imageList: res,
        })
      });
    });
  },

  // 图片点击
  imgSelectedManager: function (e) {
    var selectedImgUrl = e.currentTarget.id;
    var that = this;
    wx.showActionSheet({
      itemList: ['查看图片', '删除图片'],
      success: function (res) {
        if (!res.cancel) {
          if (res.tapIndex == 0) {   // 图片放大
            that.imgShowsManager(selectedImgUrl);
          } else if (res.tapIndex == 1) {    // 删除图片
              that.imgDeleteManager(selectedImgUrl);
          }
        }
      }
    })
  },
  // 图片放大
  imgShowsManager: function (selectedImg) {
    var that = this;
    wx.previewImage({
      current: selectedImg,
      urls: that.data.imageList,
    })
  },
  // 删除图片
  imgDeleteManager: function (selectedImg) {
    var that = this;
    var tempImgList = [];
    tempImgList = this.data.imageList;

    var selectedIndex = tempImgList.indexOf(selectedImg);
    tempImgList.splice(selectedIndex, 1);

    this.imageList = tempImgList;
    this.setData({
      imageList: tempImgList,
    })
  },

  /**
   * 添加
   */
  addManager:function(e){
    var params = { "name": this.data.nameInput, "html": this.data.desInput,"img":this.data.imageList[0]};
    appInstance.appFetchManager("BannerAdd",params).then(res=>{
      wx.navigateBack({});
    });
  },
  /**
   * 获取详情
   */
  sendRequestToGetBannerInfo: function() {
    var params = {"id":transferId};
    var that = this;
    appInstance.appFetchManager('BannerDetail', params).then(res => {
      var imgTempList = [];
      imgTempList.push(res.img);
      that.setData({
        nameInput:res.name,
        desInput:res.html,
        imageList: imgTempList,
      });
    })
  },
  /**
   * 删除
   */
  deleteManager:function() {
    var params = {"id":transferId};
    appInstance.appFetchManager('BannerDel', params).then(res => {
      wx.navigateBack({});
    })
  }
})