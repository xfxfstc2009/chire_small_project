var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bannerTypeList:["使用中","已失效"],
    bannerList:[],
    segmentStatus:"使用中",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.sendRequestToGetBannerListManager();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  /**
   * 获取banner列表
   */
 
  addManager:function(){
    wx.navigateTo({
      url: 'banner_edit/banner_edit',
    })
  },
  sendRequestToGetBannerListManager(){
    var params = { invalid: this.data.segmentStatus};
    var that = this;
    appInstance.network.fetchManager('BannerList',params).then(res=>{
      that.setData({
        bannerList:res.list,
      })
    });
  },
  bannerType:function(e){
    console.log(e);
    if (e.currentTarget.id == '使用中'){
      this.data.segmentStatus = "1";
    } else {
      this.data.segmentStatus = "0";
    }
    this.sendRequestToGetBannerListManager();
  },
  bannerActionClick:function(e){
    console.log(e.currentTarget.id);
    wx.navigateTo({
      url: 'banner_edit/banner_edit?id='+e.currentTarget.id,
    })
  }
})