var WxParse = require('../../../utils/wxParse/wxParse.js');
var appInstance = getApp();
var transferId = "";
Page({

  /**
   * 页面的初始数据
   */
  data: {

    article: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (appInstance.util.isEmpty(options.id) != true) {
      transferId = options.id;
      this.sendRequestToGetDetail();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  sendRequestToGetDetail: function () {
    var params = { id: transferId };
    var that = this;
    appInstance.network.fetchManager('BannerDetail', params).then(res => {
      var articles = res.html;
      console.log(articles);
      articles = articles.replace(/<img/g, '<img style="max-width:100%;height:auto" ')
      articles = articles.replace(/frameborder="0"/g, '')
      articles = articles.replace(/allowfullscreen="true"/g, '')
      articles = articles.replace(/\iframe/g, 'video')


      console.log(articles);
      that.setData({
        article: articles,
      })
    })
  }
})