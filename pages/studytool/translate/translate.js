var appInstance = getApp();
var userInputInfo = "";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showText:"",

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
   /**
   * 翻译按钮点击
   */
  fanyiManager:function(){
    this.sendRequestToFanYiManager();
  },

    /**
   * 翻译接口
   */
  sendRequestToFanYiManager:function(){
    var fanyiMode = {};
    fanyiMode.fromUserName = "1";
    fanyiMode.info = userInputInfo;

    var that = this;
    appInstance.network.fetchManager('StudyToolTranslate', fanyiMode).then(res => {
      that.data.showText = res.info;
      that.setData({
        showText: res.info,
      })
    });
  },
  inputManager:function(e){
    userInputInfo = e.detail.value;
  },
  // 长按复制
  longtapManager:function(e){
    var that = this;
    wx.setClipboardData({
      data: that.data.showText,
      success(res) {
        wx.getClipboardData({
          success(res) {
            console.log(res.data) // data
          }
        })
      }
    })
  }

})