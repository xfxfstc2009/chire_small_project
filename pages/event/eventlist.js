var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    eventList:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.sendRequestToGetEventListManager();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },

  /**
   * 获取事件列表
   */
  sendRequestToGetEventListManager:function(){
    var that = this;
    appInstance.network.fetchManager('Event_List', {}).then(res => {
      var tempList = res.list;
      tempList.map(function (item) {
        let imgInfo = {
          url: "smallprogream/event/event_ablum_hlt.png",
          width: 200,
          height: 200
        };

        item.ablum = appInstance.appUntilManager("fixedImgUrl", imgInfo);
      });

      that.setData({
        eventList: tempList,
      })
    });
  },

  addEvent:function(){
    wx.navigateTo({
      url: 'eventadd/eventadd',
    })
  }
})