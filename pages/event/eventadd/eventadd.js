
var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 1. 循环次数
    roundTimes:["事件执行次数","仅一次","每周一次"],
    roundTimesIndex:0,
    // 2.选择班级列表
    classList:[],
    classIndex:0,
    classPicker:[],
    // 3.事件时间
    eventDayInput:"",
    eventTimeInput:"",

    // 4. 名称
    nameInput:"",

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.sendRequestToGetClassList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  },

  nameInputManager:function(e){
    this.setData({
      nameInput: e.detail.value,
    })
  },

 /**
   * 执行次数
   */
  roundTimesChange:function(e){
    this.setData({
      roundTimesIndex: e.detail.value,
    })
  },
 /**
   * 日期picker 修改
   */
  datePickerChange:function(e){
    this.setData({
      eventDayInput:e.detail.value,
    })
  }  ,

   /**
   * 时间picker 修改
   */
  timePickerChange: function (e) {
    console.log(e);
    this.setData({
      eventTimeInput:e.detail.value,
    })
  }  ,

    /**
   * 接口请求
   */
  // 获取班级列表
  sendRequestToGetClassList:function(e){
    var that = this;
    appInstance.network.fetchManager('Class_List', {}).then(res => {
      
      var tempList = res.list;
      var tempClassPicker = [];
      tempList.map(function (item) {
        tempClassPicker.push(item.name + "[" + item.marks + "]");
        item.pickerName = item.name + "[" + item.marks + "]";
      });

      that.setData({
        classList: tempList,
        classPicker: tempClassPicker,
      })
    });
  },
  // 班级picker 进行
  classChange:function(e){
    this.setData({
      classIndex: e.detail.value,
    })
  },
  // 添加事件
  addEventManager:function(){
    var classTempId = this.data.classList[this.data.classIndex].id;
    var params = { name: this.data.nameInput, datetime: this.data.eventDayInput, classId: classTempId,type:1};

    appInstance.network.fetchManager('Event_Add',params).then(res=>{
      wx.navigateBack({});
    });
  }
})