var appInstance = getApp();
var signStatusModel = {};     // 签到状态model
var selectedSignInfo = {};    // 今天开课的时间信息
Page({

  /**
   * 页面的初始数据
   */
  data: {
    classStartDaojishiModel:{},        /**<课程开始倒计时 */
    classEndDaojishiModel: {},        /**<课程结束倒计时 */
    qiandaodaojishiHour:0,
    qiandaodaojishiMin:0,
    qiantuidaojishiHour: 0,
    qiantuidaojishiMin: 0,
    signStatus:1,                       // - 1表示没课  1 表示有课
    errTitle:"",
    // 签到签退方法
    hasSignIn:false,
    signInModel:{},
    hasSignOut:false,
    signOutModel: {},
    hasSignOutBtnEnable:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getCurrentSignStatus();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    this.endSetInter();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
   /**
   * 获取当前签到状态
   */
  getCurrentSignStatus:function(){
    var that = this;
    var params = { openid: appInstance.globalData.userInfo.studentInfo.openid };
    appInstance.network.fetchManager('Sign_Status', params).then(res => {

      if (appInstance.util.isEmpty(res.errCode) != false) {   // 表示今天有课
        signStatusModel = res;
        // 1. 判断是否有签到签退
        // 【1】.已签到
        if (appInstance.util.isEmpty(signStatusModel.sign_in.id) != true){  
        console.log("有in价值");
          that.setData({
            hasSignIn:true,
            signInModel:signStatusModel.sign_in,
            signStatusModel:res
          })
        } else {          // 【表示没签到】
          // 倒计时
          var daojishiModel = that.classJisuanShijianchaWithNoSignin(res);
          // 1. 判断是否超时
          if (daojishiModel.day < 0 || daojishiModel.hour < 0 || daojishiModel.min < 0) {
            // 表示已超时
            console.log(daojishiModel.hour);
            console.log(daojishiModel.min);
            console.log("超时");
            that.setData({
              currentState: 0,
              signStatus: 1,
              qiandaodaojishiHour: daojishiModel.hour,
              qiandaodaojishiMin: daojishiModel.min,
            })
          } else {
            console.log("没超时");
            // 表示没超时
            that.setData({
              signStatus: 1,
              currentState: 0,

              qiandaodaojishiHour: daojishiModel.hour,
              qiandaodaojishiMin: daojishiModel.min,
            })
            // 2. 开启计时器
            that.startSetInter();
          }
        }
        // 【2】已签退
        if (appInstance.util.isEmpty(signStatusModel.sign_out.id) != true) {  // 表示有值
          console.log("有out价值");
          that.setData({
            hasSignOut: true,
            signOutModel: signStatusModel.sign_out,
            signStatusModel: res
          })
        } else {
          console.log("没签退值");
          // 倒计时
          var daojishiModel = that.classJisuanShijianchaWithSignOut(res);

        console.log("=====================");
          console.log(daojishiModel);

          // 1. 判断是否超时
          // 【表示还没下课】
          if (daojishiModel.day >= 0 && daojishiModel.hour >= 0 && daojishiModel.min >= 0) {
            console.log("======润肺物权法");
            that.setData({
              hasSignOutBtnEnable: false,                   // 表示
              currentState: 0,
              signStatus: 1,
              qiantuidaojishiHour: daojishiModel.hour,
              qiantuidaojishiMin: daojishiModel.min,
            })
            // 2. 开启计时器
            that.endSetInter();
            that.endClassSetInter();
          } else {      // 【表示已下课】
            that.endSetInter();
            that.setData({
              hasSignOutBtnEnable:true,
              signStatus: 1,
              currentState: 0,
            })
          }
        }

       
      } else {
        this.setData({
          signStatus: -1,
          errTitle: "今日没课，或联系授课老师进行设置。"
        })
      }
    })
  },
  // 1. 拿到课以后进行
  classJisuanShijianchaWithNoSignin:function(res){
    var currentWeekDay = this.getTodayWeekDay();

    // 1. 获取到今天上的课是几点的课
    var timeBeginList = res.classModel.classtimelist;

    var selectedClassTime = {};
    for (var i = 0 ; i < timeBeginList.length;i++){
      var singleInfo = timeBeginList[i];
      if (singleInfo.week == currentWeekDay){
        selectedClassTime = singleInfo;
      }
    }
    this.setData({
      selectedSignInfo: selectedClassTime,
    })

    // 2. 计算差值
    if (appInstance.util.isEmpty(selectedClassTime.begintime) != true){
      var currentTime = appInstance.util.formatTime(new Date());
      var targetTempTime = appInstance.util.formatTime(new Date());
      var index = targetTempTime.lastIndexOf(" ");
      var targetYear = targetTempTime.substring(0, index);
      var targetTime = targetYear + " " + selectedClassTime.begintime+":00";
      var tempClassStartDaojishiModel = this.shijiancha(currentTime, targetTime);
      this.data.classStartDaojishiModel = tempClassStartDaojishiModel;
      return tempClassStartDaojishiModel;
    } else {
      console.log("===没有");
      return null;
    }
  },

  // 2.拿到课以后进行计算签退倒计时
  classJisuanShijianchaWithSignOut: function (res) {
    // 获取到今天
    var currentWeekDay = this.getTodayWeekDay();

    // 1. 获取到今天上的课是几点的课
    var timeBeginList = res.classModel.classtimelist;

    var selectedClassTime = {};
    for (var i = 0; i < timeBeginList.length; i++) {
      var singleInfo = timeBeginList[i];
      if (singleInfo.week == currentWeekDay) {
        selectedClassTime = singleInfo;
      }
    }
    this.setData({
      selectedSignInfo: selectedClassTime,
    })

    // 2. 计算差值
    if (appInstance.util.isEmpty(selectedClassTime.endtime) != true) {
      var currentTime = appInstance.util.formatTime(new Date());
      var targetTempTime = appInstance.util.formatTime(new Date());
      var index = targetTempTime.lastIndexOf(" ");
      var targetYear = targetTempTime.substring(0, index);
      var targetTime = targetYear + " " + selectedClassTime.endtime + ":00";
      var tempclassEndDaojishiModel = this.shijiancha(currentTime, targetTime);
      this.data.classEndDaojishiModel = tempclassEndDaojishiModel;
      return tempclassEndDaojishiModel;
    } else {
      console.log("===没有");
      return null;
    }
  },

 /**
   * 进行签到&签退
   */
  signTask:function(){
    var that = this;
    var signType = 0;
    if (appInstance.util.isEmpty(signStatusModel.sign_in.id) == true){      // 表是没有签到过
      signType = 1;
    } else {
      signType = 3;
    }
    var params = { signType: signType};
    console.log(params);
    appInstance.network.fetchManager('Sign_Login', params).then(res => {
      if (signType == 1){     // 表示已经签到
        // 关闭计时器 
        that.endSetInter();
      }
      // 重新更新状态
      that.getCurrentSignStatus();
    });
  },

  signManager:function(){
    var mainTitle = "";
    var mainContent = "";
    if (this.data.hasSignIn != true){
      mainTitle = "签到";
      mainContent = "是否进行签到，签到后进行计算课时，并且记录到系统。"
    } else if (this.data.hasSignOut != true){
      mainTitle = "签退";
      mainContent = "请确认是否进行签退，如有绑定家长。签退信息将会推送到家长手机。"
    } else {
      mainTitle = "您已成功打卡";
      mainContent = "您已成功签退"
    }
  var that = this;
    wx.showModal({
      title: mainTitle,
      content: mainContent,
      confirmText: "确定",
      cancelText: "取消",
      success: function (res) {
        console.log(res);
        if (res.confirm) {
          that.signTask();
        } else {
          
        }
      }
    });
  },
  /**
   * =================================== 工具 ===================================
   */
  // 获取今天礼拜几
  getTodayWeekDay:function(){
    var today = new Date().getDay();
    var week = ['0', '1', '2', '3', '4', '5', '6'];
    return week[today];
  },

  shijiancha: function (faultDate, completeTime) {
    var stime = Date.parse(new Date(faultDate));
    var etime = Date.parse(new Date(completeTime));
    var usedTime = etime - stime; //两个时间戳相差的毫秒数
    var days = Math.floor(usedTime / (24 * 3600 * 1000));
    //计算出小时数
    var leave1 = usedTime % (24 * 3600 * 1000); //计算天数后剩余的毫秒数
    var hours = Math.floor(leave1 / (3600 * 1000));
    //计算相差分钟数
    var leave2 = leave1 % (3600 * 1000); //计算小时数后剩余的毫秒数
    var minutes = Math.floor(leave2 / (60 * 1000));
    var dayStr = days == 0 ? "" : days + "天";
    var hoursStr = hours == 0 ? "" : hours + "时";
    var time = dayStr + hoursStr + minutes + "分";
    var timeModel = { "day": days, "hour": hours, "min": minutes,"time":time};
    return timeModel;
  },

  //开始订单计时器
  startSetInter: function () {
    this.endSetInter();

    var that = this;
    that.data.setInter = setInterval(
      function () {
        // 进行计算
        that.data.classStartDaojishiModel.min = that.data.classStartDaojishiModel.min - 1;
        if (that.data.classStartDaojishiModel.min <= 0){
          that.data.classStartDaojishiModel.min = 59;
          that.data.classStartDaojishiModel.hour = that.data.classStartDaojishiModel.hour - 1;
        }
        that.setData({
          qiandaodaojishiHour: that.data.classStartDaojishiModel.hour,
          qiandaodaojishiMin: that.data.classStartDaojishiModel.min,
        });
      }, 1000 * 60);
  },

  //开始签退计时器
  endClassSetInter: function () {
    var that = this;
    that.data.setInter = setInterval(
      function () {
        console.log("签退倒计时");
        // 进行计算
        that.data.classEndDaojishiModel.min = that.data.classEndDaojishiModel.min - 1;
        if (that.data.classEndDaojishiModel.min <= 0) {
          that.data.classEndDaojishiModel.min = 59;
          that.data.classEndDaojishiModel.hour = that.data.classEndDaojishiModel.hour - 1;
          if (that.data.classEndDaojishiModel.hour < 0){
            console.log("签退=====12=3=12=3=");
            // 表示可以签退
            that.endSetInter();
            that.setData({
              hasSignOutBtnEnable: true,
              signStatus: 1,
              currentState: 0,
            })
          }
        }
        that.setData({
          qiantuidaojishiHour: that.data.classEndDaojishiModel.hour,
          qiantuidaojishiMin: that.data.classEndDaojishiModel.min,
        });
      }, 1000 * 60);
  },

  //清除计时器 即清除setInter
  endSetInter: function () {
    var that = this;
    clearInterval(that.data.setInter)
  },


  goSignOutStatus:function(){
    console.log()
    this.endSetInter();
    this.setData({
      hasSignOutBtnEnable: true,
    })
  }
})