var appInstance = getApp();
var uploadImage = require('../../../utils/uploader/uploadFile.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hasRenzhen: false, // 判断是否是认证
    // 1.头像
    avatarInput: "",
    // 1.1昵称
    nickInput: "",
    // 2.名字
    nameInput: "",
    // 3.性别
    genderPicker: ["请选择性别", "男", "女"],
    genderIndex: 0,
    // 4.生日
    birthdayInput: "",
    // 5. 手机号
    phoneNumberInput: "",
    //6.学校
    schoolInput: "",
    // 7.签名
    markInput: "",
    // 8.生活照
    lifePhotos: [],
    // 9.certificates
    
    certPhotoArr: [],
  },

  /*** 生命周期函数--监听页面加载*/
  onLoad: function(options) {

    console.log(appInstance.globalData);
    // 1.进行加载
    this.setData({
      avatarInput: appInstance.globalData.userInfo.avatar,
      nickInput: appInstance.globalData.userInfo.nick,
      hasRenzhen: appInstance.globalData.userInfo.has_renzhen == "1" ? true : false,
    })

    if (appInstance.globalData.userInfo.teacherInfo != null) {
      // 性别
      var genderTemp = 0;
      if (appInstance.globalData.userInfo.teacherInfo.gender == "男" || appInstance.globalData.userInfo.teacherInfo.gender == 1) {
        genderTemp = 1;
      } else if (appInstance.globalData.userInfo.teacherInfo.gender == "女" || appInstance.globalData.userInfo.teacherInfo.gender == 2) {
        genderTemp = 2;
      }
      this.data.lifePhotos = appInstance.globalData.userInfo.teacherInfo.lifephotoArr;
      this.data.certPhotoArr = appInstance.globalData.userInfo.teacherInfo.certPhotoArr;
      this.setData({
        nameInput: appInstance.globalData.userInfo.teacherInfo.name,
        genderIndex: genderTemp,
        birthdayInput: appInstance.globalData.userInfo.teacherInfo.birthday,
        phoneNumberInput: appInstance.globalData.userInfo.teacherInfo.phone,
        schoolInput: appInstance.globalData.userInfo.teacherInfo.school,
        markInput: appInstance.globalData.userInfo.teacherInfo.remark,
        lifePhotos: appInstance.globalData.userInfo.teacherInfo.lifephotoArr,
        certPhotoArr: appInstance.globalData.userInfo.teacherInfo.certPhotoArr,
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    console.log(appInstance.globalData.userInfo);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  /*** 输入名字 */
  nameInputManager: function(e) {
    this.data.nameInput = e.detail.value,
      this.setData({
        nameInput: e.detail.value,
      })
  },
  /**
   * 选择性别
   */
  genderPickerSelected: function(e) {
    this.setData({
      genderIndex: e.detail.value,
    })
  },
  /**
   * 生日修改
   */
  birthdayChange: function(e) {
    this.setData({
      birthdayInput: e.detail.value,
    })
  },
  /*** 输入手机号 */
  phoneNumberInputManager: function(e) {
    this.data.phoneNumberInput = e.detail.value,
      this.setData({
        phoneNumberInput: e.detail.value,
      })
  },
  /*** 学校输入 */
  schoolInputManager: function(e) {
    this.data.schoolInput = e.detail.value,
      this.setData({
        schoolInput: e.detail.value,
      })
  },
  /*** 输入备注 */
  remarkInputManager: function(e) {
    this.data.markInput = e.detail.value,
      this.setData({
        markInput: e.detail.value,
      })
  },
  /**
   * 头像点击进行修改头像
   */
  headerTapManager: function() {
    var that = this;
    appInstance.util.chooseImgAblumManager(1).then(res => { // 1. 选择图片
      appInstance.util.uploadImgManager(res).then(res => { // 2. 上传图片
        that.updateUserInfoManager({
          avatar: res
        }); // 3. 进行修改
      });
    });
  },
  /**
   * 上传生活照
   */
  lifePhotosManager: function() {
    var that = this;
    appInstance.util.chooseImgAblumManager(9).then(res => {
      appInstance.util.uploadImgManager(res).then(imgUrlArr => {
       
        var tempPhoro = that.data.lifePhotos;
        var tempImgPhoto = [];
        tempImgPhoto = imgUrlArr;

        var arr3 = tempImgPhoto.concat(tempPhoro);
        // 获取到上传的列表数组
        that.data.lifePhotos = arr3;
        that.setData({
          lifePhotos: arr3,
        })
      })
    })
  },

  // 我的照片
  previewImageWithLife: function(e) {
    var selectedImgUrl = e.currentTarget.id;
    var that = this;
    wx.showActionSheet({
      itemList: ['查看图片', '删除图片'],
      success: function (res) {
        if (!res.cancel) {
          if (res.tapIndex == 0) {   // 图片放大
            that.imgShowsManager(selectedImgUrl,that.data.lifePhotos);
          } else if (res.tapIndex == 1) {    // 删除图片
        
            that.imgDeleteManagerWithLife(selectedImgUrl);
          }
        }
      }
    })
  },


  // 图片放大
  imgShowsManager: function (selectedImg,allImg) {
    var that = this;
    wx.previewImage({
      current: selectedImg,
      urls: allImg,
    })
  },

  // 删除图片
  imgDeleteManagerWithLife: function (selectedImg) {
    var that = this;
    var tempImgList = [];
    tempImgList = this.data.lifePhotos;

    var selectedIndex = tempImgList.indexOf(selectedImg);
    tempImgList.splice(selectedIndex, 1);

    this.lifePhotos = tempImgList;
    this.setData({
      lifePhotos: tempImgList,
    })
  },


  //证书
  previewImageWithCert: function(e) {
    var selectedImgUrl = e.currentTarget.id;
    var that = this;
    wx.showActionSheet({
      itemList: ['查看图片', '删除图片'],
      success: function (res) {
        if (!res.cancel) {
          if (res.tapIndex == 0) {   // 图片放大
            that.imgShowsManager(selectedImgUrl, that.data.certPhotoArr);
          } else if (res.tapIndex == 1) {    // 删除图片

            that.imgDeleteManagerWithCert(selectedImgUrl);
          }
        }
      }
    })
  },

  // 删除图片
  imgDeleteManagerWithCert: function (selectedImg) {
    var that = this;
    var tempImgList = [];
    tempImgList = this.data.certPhotoArr;

    var selectedIndex = tempImgList.indexOf(selectedImg);
    tempImgList.splice(selectedIndex, 1);

    this.certPhotoArr = tempImgList;
    this.setData({
      certPhotoArr: tempImgList,
    })
  },

  /*** 上传资历*/
  certTapManager: function(e) {
    var that = this;
    appInstance.util.chooseImgAblumManager(9).then(res => {
      appInstance.util.uploadImgManager(res).then(imgUrlArr => {

        var tempPhoro = that.data.certPhotoArr;

        var arr3 = tempPhoro.concat(imgUrlArr);
        // 获取到上传的列表数组
        that.data.certPhotoArr = arr3;

        // 获取到上传的列表数组
        that.setData({
          certPhotoArr: arr3,
        })
      })
    })
  },

  // 修改信息
  updateUserInfoManager(params) {
    var that = this;
    appInstance.appFetchManager("Teacher_Edit", params).then(res => {
      if (params.avatar != "") {
        appInstance.globalData.userInfo.avatar = params.avatar;
        that.setData({
          userInfo: appInstance.globalData.userInfo,
          avatarInput: params.avatar,
        })
      }
    });
  },

  chooseImage: function(e) {
    var that = this;
    appInstance.util.chooseImgAblumManager(9).then(res => {
      appInstance.util.uploadImgManager(res).then(imgUrlArr => {
        // 获取到上传的列表数组
        console.log(imgUrlArr);
        that.setData({
          lifePhotos: imgUrlArr,
        })
      })
    })
  },

  itemclick: function(e) {
    console.log("item");
    console.log(e);
  },


  uploadManager: function(res) {

    // 1.获取图片
    var tempFilePaths = res.tempFilePaths;
    // 2.获取当前时间
    var nowTime = "users"

    // 3.上传
    for (var i = 0; i < res.tempFilePaths.length; i++) {
      //显示消息提示框
      wx.showLoading({
        title: '上传中' + (i + 1) + '/' + res.tempFilePaths.length,
        mask: true
      })

      //上传图片
      //你的域名下的/cbb文件下的/当前年月日文件下的/图片.png
      //图片路径可自行修改
      uploadImage(res.tempFilePaths[i], 'smart/' + nowTime + '/',
        function(result) {
          console.log("======上传成功图片地址为：", result);
          wx.hideLoading();
        },
        function(result) {
          console.log("======上传失败======", result);
          wx.hideLoading()
        }
      )
    }
  },

  /*** 添加老师申请*/
  actionManager: function(e) {
    // 1. 生活照
    var diplomaInput = "";
    var lifePhotosStr = "";
    if (this.data.lifePhotos.length >= 1) {
      for (var i = 0; i < this.data.lifePhotos.length; i++) {
        var singleUrl = this.data.lifePhotos[i];
        if (i == this.data.lifePhotos.length - 1) {
          lifePhotosStr = lifePhotosStr + singleUrl;
        } else {
          lifePhotosStr = lifePhotosStr + singleUrl + ",";
        }
      }
    }
    // 2.证书
    var certStr = "";
    if (this.data.certPhotoArr.length >= 1) {
      diplomaInput = this.data.certPhotoArr[0];
      for (var i = 0; i < this.data.certPhotoArr.length; i++) {
        var singleUrl = this.data.certPhotoArr[i];
        if (i == this.data.certPhotoArr.length - 1) {
          certStr = certStr + singleUrl;
        } else {
          certStr = certStr + singleUrl + ",";
        }
      }
    }
    // lifePhotos

    var that = this;
    var params = {
      openid: appInstance.globalData.userInfo.open_id,
      avatar: appInstance.globalData.userInfo.avatar,
      name: this.data.nameInput,
      gender: this.data.genderIndex,
      birthday: this.data.birthdayInput,
      phone: this.data.phoneNumberInput,
      school: this.data.schoolInput,
      diploma: this.data.certPhotoArr[0],
      remark: this.data.markInput,
      lifephotos: lifePhotosStr,
      certphotos: certStr,
    };
    console.log(params);
    appInstance.appFetchManager("Teacher_Add", params).then(res => {
      if (that.data.hasRenzhen == false) {
        wx.showModal({
          title: '提示',
          content: '等待管理员进行审核。',
          confirmText: "确认",
          success: function(res) {
            // 4. 返回
            var pages = getCurrentPages();
            var prevPage = pages[pages.length - 2];
            var tempUserInfo = appInstance.globalData.userInfo;
            tempUserInfo.user_type = -2;
  
            prevPage.setData({
              userInfo: tempUserInfo,
            })
           
            wx.navigateBack({
              delta: 1,
            });

          }
        });
      } else {
        wx.showToast({
          title: '修改成功',
        })
      }
    });
  },

  updateManager: function() {

    // 1. 生活照
    var diplomaInput = "";
    var lifePhotosStr = "";
    if (this.data.lifePhotos.length >= 1) {
      for (var i = 0; i < this.data.lifePhotos.length; i++) {
        var singleUrl = this.data.lifePhotos[i];
        if (i == this.data.lifePhotos.length - 1) {
          lifePhotosStr = lifePhotosStr + singleUrl;
        } else {
          lifePhotosStr = lifePhotosStr + singleUrl + ",";
        }
      }
    }
    // 2.证书
    var certStr = "";
    if (this.data.certPhotoArr.length >= 1) {
      diplomaInput = this.data.certPhotoArr[0];
      for (var i = 0; i < this.data.certPhotoArr.length; i++) {
        var singleUrl = this.data.certPhotoArr[i];
        if (i == this.data.certPhotoArr.length - 1) {
          certStr = certStr + singleUrl;
        } else {
          certStr = certStr + singleUrl + ",";
        }
      }
    }
    // lifePhotos

    var that = this;
    var params = {
      openid: appInstance.globalData.userInfo.open_id,
      avatar: appInstance.globalData.userInfo.avatar,
      name: this.data.nameInput,
      gender: this.data.genderIndex,
      birthday: this.data.birthdayInput,
      phone: this.data.phoneNumberInput,
      school: this.data.schoolInput,
      diploma: this.data.certPhotoArr[0],
      remark: this.data.markInput,
      lifephotos: lifePhotosStr,
      certphotos: certStr,
    };
    console.log(params);
    appInstance.appFetchManager("TeacherUpdate", params).then(res => {
      var infoParams = params;
      infoParams.lifephotoArr = that.data.lifePhotos;
      infoParams.certPhotoArr = that.data.certPhotoArr;
      appInstance.globalData.userInfo.teacherInfo = infoParams;
      wx.showToast({
        title: '修改成功',
      })
    });
  }
})



//  删除生活照
function authWithTeacher() {
  return new Promise(function (resolve, reject) {
    var params = { id: transferId };
    var that = this;
    appInstance.network.fetchManager('ActivityDetail', params).then(res => {
      var articles = res.des;

      articles = articles.replace('<img', '<img style="max-width:100%;height:auto" ')
      console.log(articles);
      that.setData({
        article: articles,
      })
    })
  });
}