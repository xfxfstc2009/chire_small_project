var appInstance = getApp();
var uploadImage = require('../../../utils/uploader/uploadFile.js');
var util = require('../../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    files: []
  },
  chooseImage: function (e) {
    var that = this;

wx.chooseImage({
  count: 9,
  sizeType: ['original', 'compressed'],
  sourceType: ['album', 'camera'],
  success: function(res) {
    that.uploadManager(res);
    that.setData({
      files: that.data.files.concat(res.tempFilePaths)
    });

  },
  fail: function(res) {
    console.log(res);
  },
  complete: function(res) {
    console.log(res);
  },
})

  },
  previewImage: function (e) {
    wx.previewImage({
      current: e.currentTarget.id, // 当前显示图片的http链接
      urls: this.data.files // 需要预览的图片http链接列表
    })
  },
  uploadManager:function(res){

    // 1.获取图片
    var tempFilePaths = res.tempFilePaths;
    // 2.获取当前时间
    var nowTime = "users"

    // 3.上传
    for (var i = 0; i < res.tempFilePaths.length; i++) {
      //显示消息提示框
      wx.showLoading({
        title: '上传中' + (i + 1) + '/' + res.tempFilePaths.length,
        mask: true
      })

      //上传图片
      //你的域名下的/cbb文件下的/当前年月日文件下的/图片.png
      //图片路径可自行修改
      uploadImage(res.tempFilePaths[i], 'smart/' + nowTime + '/',
        function (result) {
          console.log("======上传成功图片地址为：", result);
          wx.hideLoading();
        }, function (result) {
          console.log("======上传失败======", result);
          wx.hideLoading()
        }
      )
    }
  },



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },



})