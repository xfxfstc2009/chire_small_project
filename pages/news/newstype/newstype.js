var appInstance = getApp();
var transferId = "";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    newsTypeId:"",        //消息类型id
    newsTypeName:"",          // 消息名称
    fileList:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取上个页面的id
    if (appInstance.util.isEmpty(options.id) != true) {
      transferId = options.id;
      this.setData({
        transferId:options.id,
      })
      this.sendRequestToGetNewsTypeInfo();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },


  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  // 图片选择器
  chooseFiles: function (e) {
    var that = this;
    // 1. 选择图片
    appInstance.util.chooseImgAblumManager(2).then(res => {
      // 进行上传
      appInstance.util.uploadImgManager(res).then(res => {          // 2. 上传图片
        var tempImgList = that.data.fileList;
        tempImgList.push.apply(tempImgList,res);
        
        that.data.fileList = tempImgList;
        that.setData({
          fileList: tempImgList,
        })
      });
    });
  },

  // 图片点击
  imgSelectedManager: function (e) {
    var selectedImgUrl = e.currentTarget.id;
    var that = this;
    wx.showActionSheet({
      itemList: ['查看图片', '删除图片'],
      success: function (res) {
        if (!res.cancel) {
          if (res.tapIndex == 0) {   // 图片放大
            that.imgShowsManager(selectedImgUrl);
          } else if (res.tapIndex == 1) {    // 删除图片
            that.imgDeleteManager(selectedImgUrl);
          }
        }
      }
    })
  },
  // 图片放大
  imgShowsManager: function (selectedImg) {
    var that = this;
    wx.previewImage({
      current: selectedImg,
      urls: that.data.fileList,
    })
  },
  // 删除图片
  imgDeleteManager: function (selectedImg) {
    var that = this;
    var tempImgList = [];
    tempImgList = this.data.fileList;

    var selectedIndex = tempImgList.indexOf(selectedImg);
    tempImgList.splice(selectedIndex, 1);

    this.imageList = tempImgList;
    this.setData({
      fileList: tempImgList,
    })
  },




  // 输入信息
  newsTypeInput:function(e){
    this.data.newsTypeName = e.detail.value;
  },


  /*** 科目删除接口*/
  sendRequestToDelSubject: function () {
    appInstance.network.fetchManager('SubjectDel', params).then(res => {
      that.sendRequestToGetSubjectList();
    })
  },


  /*** 按钮点击方法*/
  actionManager: function () {
    if (appInstance.util.isEmpty(this.data.newsTypeId) == true) {
      // 创建新的消息类型
      this.sendRequestToAddNewsType();
    } else {

    }
  },

  sendRequestToGetSubjectInfo: function (e) {
    var that = this;
    var params = { id: e };

    appInstance.network.fetchManager('SubjectInfo', params).then(res => {
      console.log(res);
      var imgList = [];
      var imgMainUrl = appInstance.util.fixedImgUrl(res.img, "");

      imgList.push(imgMainUrl);
      that.setData({
        subjectName: res.name,
        subjectId: e,
        files: imgList,
      })
    });
  },
  /*** 获取当前的消息类型*/
  sendRequestToGetNewsTypeInfo:function(){
    var that = this;
    var params = { id: transferId};
    console.log(params);
    appInstance.network.fetchManager('News_Type_Detail', params).then(res => {
      that.setData({
        newsTypeId:res.id,
        newsTypeName:res.name,
        fileList:res.ablum,
      })
    });
  },
    /*** 修改当前的图片*/
  imgDeleteWithUploadManager:function(){

  },
  /*** 修改方法*/
  updateManager:function(){
    var that = this;

    var ablumUrl = "";
    for (var i = 0; i < this.data.fileList.length; i++) {
      var singleUrl = this.data.fileList[i];
      if (i == this.data.fileList.length - 1) {
        ablumUrl = ablumUrl + singleUrl;
      } else {
        ablumUrl = ablumUrl + singleUrl + ",";
      }
    }

    var params = {id:transferId, name: this.data.newsTypeName, ablum: ablumUrl };

    console.log(params);
    appInstance.network.fetchManager('News_type_Update', params).then(res => {
      wx.navigateBack({});
    })
  },

  /*** 删除方法*/
  deleteManager:function(){
    var that = this;
    var params = { id: transferId };
    appInstance.network.fetchManager('News_type_Del', params).then(res => {
      wx.navigateBack({
        
      });
    });
    
  },
  /*** 添加方法*/
  addManager:function(){
    this.sendRequestToAddNewsType();
  },


  /*** 消息类型添加接口*/
  sendRequestToAddNewsType: function () {
    var that = this;

    var ablumUrl = "";
    for (var i = 0;i<this.data.fileList.length;i++){
      var singleUrl = this.data.fileList[i];
      if(i == this.data.fileList.length - 1){
        ablumUrl = ablumUrl+singleUrl;
      } else {
        ablumUrl = ablumUrl + singleUrl + ",";
      }
    }
    
    var params = { name: this.data.newsTypeName, ablum: ablumUrl };
    appInstance.network.fetchManager('News_Type_Add', params).then(res => {
      wx.navigateBack({});
    })
  },
})