var appInstance = getApp();
var transferId = "";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    newsName: "",
    newsDetail: "",
    newsTypeIndex: 0,
    newsLinkUrl: "",
    imageList: [],
    newsTypeList: [],
    newsTypePicker: [],
    hasPush:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (appInstance.util.isEmpty(options.id) != true) {
      transferId = options.id;
      this.setData({
        transferId: options.id,
      })
      this.editGetNewsDetailManager()
    }

    this.sendRequestToGetNewsType();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  /**
   * 输入名字
   */
  nameInputManager: function(e) {
    this.data.newsName = e.detail.value;
  },
  /**
   * 输入消息详情
   */
  newsDetailInputManager: function(e) {
    this.data.newsDetail = e.detail.value;
  },

  newsLinkInputManager: function(e) {
    this.data.newsLinkUrl = e.detail.value;
  },
  switchChanged:function(e){
    this.setData({
      hasPush: e.detail.value,
    })
  },
  /**
   * 消息类型修改
   */
  newsTypeChangeManager: function(e) {
    this.data.newsTypeIndex = e.detail.value;
    var that = this;
    that.setData({
      newsTypeIndex: e.detail.value,
    })
  },
  /**
   * 选择图片信息
   */
  chooseFiles: function(e) {
    var that = this;
    // 1. 选择图片
    appInstance.util.chooseImgAblumManager(9).then(res => {
      // 进行上传
      appInstance.util.uploadImgManager(res).then(res => { // 2. 上传图片
        that.imageList = res;
        that.setData({
          imageList: res,
        })
      });
    });
  },

  /**
   * 获得消息类型
   */
  sendRequestToGetNewsType: function(e) {
    var that = this;
    appInstance.network.fetchManager('News_Type_List', '').then(res => {
      var tempNewsTypePickerList = [];
      res.list.map(function(item) {
        tempNewsTypePickerList.push(item.name);
      });

      this.setData({
        newsTypeList: res.list,
        newsTypePicker: tempNewsTypePickerList,
      });
    });
  },

  /**
   * 获得发布消息
   */
  sendRequestToAddNewsInfo: function() {
    var that = this;
    var newsTypeModel = this.data.newsTypeList[this.data.newsTypeIndex];

    var params = {
      name: this.data.newsName,
      html: this.data.newsDetail,
      type: newsTypeModel.linkId,
      ablum: this.data.imageList[0],
      link: this.data.newsLinkUrl,
      haspush: this.data.hasPush == true ? 1 : 0,
    };

    console.log(params);
    appInstance.network.fetchManager('News_Add', params).then(res => {
      wx.navigateBack({

      });
    });
  },
  actionToAddDetail: function() {
    appInstance.tempHtml = this.data.newsDetail;

    wx.navigateTo({
      url: '../../editor/editor?type=news',
    })
  },

  // 获取当前的消息
  editGetNewsDetailManager: function() {

    var that = this;
    var params = {
      newsId: transferId
    }
    console.log(params);
    appInstance.network.fetchManager('NewsDetail', params).then(res => {
      var tempImgLise = [];
      tempImgLise.push(res.ablum);
      that.setData({

        newsName: res.name,
        newsDetail: res.des,
        newsLinkUrl: res.link,
        imageList: tempImgLise
      })
    });
  },
  // 修改当前的消息
  updateManager: function() {
    var that = this;
    var newsTypeModel = this.data.newsTypeList[this.data.newsTypeIndex];

    var params = {
      newsId: transferId,
      name: this.data.newsName,
      html: this.data.newsDetail,
      type: newsTypeModel.linkId,
      ablum: this.data.imageList[0],
      link: this.data.newsLinkUrl
    };

    appInstance.network.fetchManager('NewsUpdate', params).then(res => {
      wx.showToast({
        title: '消息修改成功',
      })
      wx.navigateBack({
        
      });
    });
  }

})