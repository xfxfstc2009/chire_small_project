var page = 1;
var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    segmentList: [] ,
    currentId:'',
    newsList:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.sendRequestToGetMessageTypeList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  /**
   * segmentList点击
   */
  segmentListTap:function(e){
    var selectedItem = {};
    for (var i = 0 ; i < this.data.segmentList.length;i++){
      var singleInfo = this.data.segmentList[i];
      if (e.currentTarget.id == singleInfo.linkId){
        selectedItem = singleInfo;
        break;
      }
    }
    if (selectedItem.linkId == 'add'){
      wx.navigateTo({
        url: '../newstype/newstype',
      })
      return;
    } else {
      this.setData({
        currentId: selectedItem.linkId,
      });
      this.sendRequestToGetMessageListManager(selectedItem.linkId);
    }
  },

  /**
   * 添加消息
   */
  actionButtonTapManager:function(){
    wx.navigateTo({
      url: '../newsadd/newsadd',
    })
  },
  segmentListLongTap:function(e){
    wx.navigateTo({
      url: '../newstype/newstype?id=' + e.currentTarget.id,
    })
  },

   /**
   * 接口,获取接口列表
   */
  sendRequestToGetMessageTypeList:function(){
    var that = this;
    appInstance.network.fetchManager('News_Type_List', '').then(res => {
      var infoList = res.list;
      infoList.push({id:"100000",name:"添加消息类型",createtime:"",linkId:"add"});
      var firstItem = infoList[0];

      that.setData({
        currentId:firstItem.linkId,
        segmentList: infoList,
      })

      // 1. 类型列表获取后进行获取消息列表
      that.sendRequestToGetMessageListManager(firstItem.linkId);
    });
  },
     /**
   * 接口,获取消息列表
   */
  sendRequestToGetMessageListManager:function(type){
    var that = this;
    var params = { page: page, size: 20, newstype: type};
    console.log(params);
    appInstance.network.fetchManager('News_List',params).then(res=>{
      this.setData({
        newsList: res.list,
      })
    })
  },
   newsEditManager:function(e){
     wx.navigateTo({
       url: '../newsadd/newsadd?id='+e.currentTarget.id,
     })

  }
})