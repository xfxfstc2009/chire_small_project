
var appInstance = getApp();
var page = 1;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    endding:"没有更多消息了",
    segmentList: [],
    newsList:[],
    currentId:"",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    this.sendRequestToGetMessageTypeList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {  
   var items_img = appInstance.appUntilManager("fixedImgUrl", "smallprogream/news/filterList/all.png");
  
    // this.getDelegateList();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },

  changeFilter:function(e){
    console.log(e);
    this.data.currentId = e.currentTarget.id;
    this.setData({
      currentId: e.currentTarget.id,
    })
    console.log(this.data.currentId);
    this.sendRequestToGetMessageListManager(e.currentTarget.id);

  },

  changeFilterDisabled:function(e){
    this.data.currentId = e.currentTarget.id;
    this.setData({
      currentId: e.currentTarget.id,
    })
    this.sendRequestToGetMessageListManager(e.currentTarget.id);
  },

  // 获取代理信息
  getDelegateList:function(e){
    var that = this;
    appInstance.appFetchManager('DelegateList', '').then(res => {
      var tempArr = [];
        res.list.map(function (item) {
        let singleInfo = {
          id: item.id,
          name: item.name,
          url: appInstance.appUntilManager("fixedImgUrl", item.url),
          datetime: item.datetime,
          info: item.info,
          icon: "http://chire.oss-cn-hangzhou.aliyuncs.com/9470Subject/%E7%A7%91%E7%9B%AE20190521163623.png?x-oss-process=image/resize,m_fill,w_50,h_50,limit_0/auto-orient,1/format,png",
        };
        tempArr.push(singleInfo);
      });

      console.log(tempArr);
      that.setData({
        newsList: tempArr,
      })
    });
  },

  smart:function(e){
    wx.navigateTo({ url: '../../utils/webview/webview' })
  },


  /**
   * 获取当前类型
   */
  sendRequestToGetMessageTypeList: function () {
    var that = this;
    appInstance.network.fetchManager('News_Type_List', '').then(res => {
      var infoList = res.list;

      infoList.map(function(item){
        var hltImg = "";
        var norImg = "";

        if (item.ablum.length > 1){
          hltImg = item.ablum[1];
          norImg = item.ablum[0];
        } else {
          hltImg = item.ablum[0];
          norImg = item.ablum[0];
        }

       let hltimgInfo = {
          url: hltImg,
          width: 150,
          height: 150
        };

        let norimgInfo = {
          url: norImg,
          width: 150,
          height: 150
        };
        item.items_hlt_img = appInstance.appUntilManager("fixedImgUrl", hltimgInfo);
        item.items_nor_img = appInstance.appUntilManager("fixedImgUrl", hltimgInfo);
      });

      var firstItem = infoList[0];

      that.setData({
        currentId: firstItem.linkId,
        segmentList: infoList,
      })

      // 第一次获取列表信息
      that.sendRequestToGetMessageListManager(firstItem.linkId);
    });
  },
  /**
   * 接口,获取消息列表
   */
  sendRequestToGetMessageListManager: function (type) {
    var that = this;
    var params = { page: page, size: 20, newstype: type };
    appInstance.network.fetchManager('News_List', params).then(res => {
      var tempList = res.list;
      
      tempList.map(function (item) {
        var items = item;

        let imgInfo = {
          url: items.ablum,
          width: 100,
          height: 100
        };

        items.ablum = appInstance.appUntilManager("fixedImgUrl", imgInfo);
      });

      this.setData({
        newsList: tempList,
      })
    })
  },
  newsActionTap:function(e){
    var messageId = e.currentTarget.id;
    wx.navigateTo({
      url: 'newsDetail/newsDetail?id='+messageId,
    })
  }
})