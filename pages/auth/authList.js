var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bannerTypeList: ["认证学生", "认证老师", ],
    studentList: [],
    teacherList: [],
    mainList: [],
    segmentStatus: "认证学生",
    authTypeHasShow: false,
    chooseOpenId : ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.sendRequestToGetAuthList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  /**
   * 获取banner列表
   */
  sendRequestToGetAuthList: function() {
    var params = {};
    var that = this;
    appInstance.network.fetchManager('AuthList', params).then(res => {
     var tempMainList = [];
      if (that.data.segmentStatus == "认证学生") {
        tempMainList = res.studentList;
      } else if (that.data.segmentStatus == "认证老师") {
        tempMainList = res.teacherList;
      }
      that.setData({
        mainList: tempMainList,
        studentList: res.studentList,
        teacherList: res.teacherList
      })
    });
  },

  segmentListTap: function(e) {
    console.log(e);
    if (e.currentTarget.id == '认证学生') {
      this.data.segmentStatus = "认证学生";
      this.setData({
        segmentStatus: "认证学生",
        mainList: this.data.studentList,
      })
    } else {
      this.data.segmentStatus = "认证老师";
      this.setData({
        segmentStatus: "认证老师",
        mainList: this.data.teacherList,
      })
    }

  },
  authBannerActionManager: function(e) {
    console.log(e.currentTarget.id);
    this.data.chooseOpenId = e.currentTarget.id;
    this.setData({
      authTypeHasShow: true,
    })
  },
  /*** 绑定用户状态*/
  // 打开绑定用户弹窗
  showDialog: function() {
    this.setData({
      authTypeHasShow: true,
    })
  },
  // 关闭绑定用户弹窗
  closeDialog: function() {
    this.setData({
      authTypeHasShow: false,
    })
  },


  userTypeChooseManager: function() {
    var that = this;
    wx.showModal({
      title: '确认',
      content: '认证后无法取消，仅能通过管理员取消',
      showCancel: true,
      cancelText: '取消',
      cancelColor: '',
      confirmText: '确认',
      confirmColor: '',
      success: function(res) {
        that.authManager();
      },
      fail: function(res) {},
      complete: function(res) {},
    })
  },

  authManager: function() {
    var type = "";
    if (this.data.segmentStatus == "认证学生") {
      type = "student";
    } else if (this.data.segmentStatus == "认证老师") {
      type = "teacher";
    }
    var params = {
      type: type,
      userOpenId: this.data.chooseOpenId
    };
    var that = this;
    console.log(params);
    appInstance.network.fetchManager('AuthAdd', params).then(res => {
      wx.showToast({
        title: '已认证',
        icon: 'success',
        duration: 3000
      });
      that.sendRequestToGetAuthList();
      that.closeDialog();
    });
  }
})