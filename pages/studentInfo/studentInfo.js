var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    avatarInput: "",
    // 1. 初始化 picker 数据
    nameInput: "", //  姓名
    // 2. 生日
    birthdayInput: "", // birthdayInput
    // 3. 手机号
    phoneInput: "", // 手机号
    // 4. 院校
    schoolInput: "", // 院校
    // 5.parent手机
    parentPhoneInput: "", // 父母手机号
    // 6.mark
    markInput: "", // 备注输入
    // 8.files
    files: [], // 我的试卷
    // 9.性别
    genderPicker: ["请选择性别", "男", "女"],
    genderIndex: 0,
    // 10.选择班级
    classPicker: [],
    classNamePicker: [],
    classIndex: 0,
    classIdInput: "",
    // 11.按钮名称
    actionButtonName: "确定"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(appInstance.globalData.userInfo);

    // 1.获取班级列表
    this.sendRequestToGetClassList();


    this.setData({
      userInfo: appInstance.globalData.userInfo,
    })

 
    // 3. 如果是已经登录进行数据渲染
    if (appInstance.globalData.userInfo.user_type == 2) {         // 当前是否已经登录
      var tempGenderIndex = 0;
      if (appInstance.globalData.userInfo.studentInfo.gender == '男' || appInstance.globalData.userInfo.studentInfo.gender == '1') {
        tempGenderIndex = 1;
      } else if (appInstance.globalData.userInfo.studentInfo.gender == '女' || appInstance.globalData.userInfo.studentInfo.gender == '2') {
        tempGenderIndex = 2;
      }
      this.setData({
        avatarInput: appInstance.globalData.userInfo.studentInfo.avatar,        // 1. 头像
        nameInput: appInstance.globalData.userInfo.studentInfo.name,            // 2. 名称
        birthdayInput: appInstance.globalData.userInfo.studentInfo.birthday,    // 3. 我的生日         
        phoneInput: appInstance.globalData.userInfo.studentInfo.phone,          // 4. 学生手机
        schoolInput: appInstance.globalData.userInfo.studentInfo.school,        // 5. 学校
        parentPhoneInput: appInstance.globalData.userInfo.studentInfo.parentnumber,// 6.父母手机
        markInput: appInstance.globalData.userInfo.studentInfo.mark,            // 7.备注
        genderIndex: tempGenderIndex,                                           // 性别
      })
    } else {
      // 设置未登录
      this.setData({
        avatarInput: appInstance.globalData.userInfo.avatar,
        genderIndex: 0,
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  /** 输入名字*/
  nameInputManager: function(e) {
    this.data.nameInput = e.detail.value;
  },
  /** 输入手机*/
  phoneInputManager: function(e) {
    this.data.phoneInput = e.detail.value;
  },
  /** 输入学校*/
  schoolInputManager: function(e) {
    this.data.schoolInput = e.detail.value;
  },
  /** 输入家长手机*/
  parentPhoneInputManager: function(e) {
    this.data.parentPhoneInput = e.detail.value;
  },
  /** 输入备注*/
  markInputManager: function(e) {
    this.data.markInput = e.detail.value;
  },
  /** 性别选择*/
  genderPickerSelected: function(e) {
    this.data.genderIndex = e.detail.value;
    var that = this;
    this.setData({
      genderIndex: e.detail.value,
    })
  },
  /** 生日选择*/
  birthdayChange: function(e) {
    appInstance.globalData.userInfo.birthday = e.detail.value;
    this.data.birthdayInput = e.detail.value;
    var that = this;
    this.setData({
      birthdayInput: e.detail.value,
    })
  },
  /** 选择头像*/
  headerTapManager: function() {
    var that = this;
    appInstance.util.chooseImgAblumManager(1).then(res => { // 1. 选择图片
      appInstance.util.uploadImgManager(res).then(res => { // 2. 上传图片
        that.data.avatarInput = res[0];
        that.setData({
          avatarInput: res[0],
        })
      });
    });
  },
  /** 选择班级*/
  classSelected: function(e) {
    this.data.classIndex = e.detail.value;
    this.setData({
      classIndex: e.detail.value,
    })
    this.data.classIdInput = this.data.classPicker[this.data.classIndex].id
  },
  /**
   * 进行点击按钮
   */
  actionButtonTapManager: function(e) {
    if (appInstance.globalData.userInfo.studentInfo == null) {
      // 添加学生信息
      this.sendRequestToAddStudent();
    } else {
      // 修改学生信息
      this.sendRequestToUpdateStudent();
    }
  },
  /**
   * 获取班级列表
   */
  sendRequestToGetClassList: function() {
    var that = this;
    appInstance.appFetchManager("Class_List", "").then(res => {
      var tempClassNamePickerList = [];
      that.data.classPicker = res.list;

      res.list.map(function(item) {
        tempClassNamePickerList.push(item.name);
      });

      that.setData({
        classNamePicker: tempClassNamePickerList,
      });
      // 默认设置第一个班级
      that.data.classIdInput = res.list[that.data.classIndex].id;
    });
  },
  /**
   * 学生信息添加
   */
  sendRequestToAddStudent: function() {
    var that = this;
    var params = {
      name: this.data.nameInput,
      gender: this.data.genderIndex,
      phone: this.data.phoneInput,
      school: this.data.schoolInput,
      parentnumber: this.data.parentPhoneInput,
      avatar: this.data.avatarInput,
      mark: this.data.markInput,
      classid: this.data.classIdInput,
      openid: appInstance.globalData.userInfo.open_id,
      birthday: this.data.birthdayInput,
    };

    appInstance.appFetchManager("StudentAdd", params).then(res => {
      appInstance.globalData.userInfo.studentInfo = res;
      appInstance.globalData.userInfo.user_type = 2;
      // 进行绑定userInfo
      wx.setStorageSync("userInfo", appInstance.globalData.userInfo);

      wx.navigateBack({});
    });
  },
  /**
   * 学生信息修改
   */
  sendRequestToUpdateStudent: function(key, value) {
    var that = this;

    var params = {
      avatar: this.data.avatarInput,
      name: this.data.nameInput,
      gender: this.data.genderIndex,
      birthday: this.data.birthdayInput,
      parentnumber: this.data.parentPhoneInput,
      mark: this.data.markInput,
      phone: this.data.phoneInput,
      school: this.data.schoolInput,
      classid: this.data.classIdInput,
      openid: appInstance.globalData.userInfo.open_id,
    };
    appInstance.appFetchManager("StudentUpdate", params).then(res => {
      appInstance.globalData.userInfo.studentInfo.avatar = params.avatar;
      appInstance.globalData.userInfo.studentInfo.name = params.name;
      appInstance.globalData.userInfo.studentInfo.gender = params.gender;
      appInstance.globalData.userInfo.studentInfo.birthday = params.birthday;
      appInstance.globalData.userInfo.studentInfo.parentnumber = params.parentnumber;
      appInstance.globalData.userInfo.studentInfo.mark = params.mark;
      appInstance.globalData.userInfo.studentInfo.phone = params.phone;
      appInstance.globalData.userInfo.studentInfo.school = params.school;
      appInstance.globalData.userInfo.studentInfo.classid = params.classid;
      // 进行绑定userInfo
      wx.setStorageSync("userInfo", appInstance.globalData.userInfo);

      wx.navigateBack({});
    });
  },
})