
var appInstance = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    logo: appInstance.mainLogo,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      logo: "https://chire.oss-cn-hangzhou.aliyuncs.com/smallprogream/logo/logo.png",
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  /**
   * 用户登录方法
   */
  authLoginManager :function(e){

    var that = this;
    // 1. 获取到当前用户信息
    appInstance.globalData.userInfo = e.detail;
    appInstance.globalData.cloudID = e.detail.cloudID;
    appInstance.globalData.encryptedData = e.detail.encryptedData;
    appInstance.globalData.iv = e.detail.iv;
    appInstance.globalData.signature = e.detail.signature;
    
    appInstance.interfaceManager.wxLoginManager().then(res=>{
      // 1. 表示微信登录成功
      if (res != false){
        appInstance.js_code = res.code;
        // 2. 进行本地登录
        var loginModel = {};
        loginModel.avatar = e.detail.userInfo.avatarUrl;
        loginModel.nick = e.detail.userInfo.nickName;
        loginModel.sex = e.detail.userInfo.gender;
        loginModel.spCode = res.code;
        loginModel.login_type = 3;

        appInstance.network.fetchManager('UserLogin', loginModel).then(res => {

          appInstance.globalData.userInfo = res;
          appInstance.globalData.userInfo.hasLogin = true;
          if (appInstance.globalData.userInfo.user_type == 1){    //  老师
            appInstance.globalData.userInfo.userType_img = appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/user_type/center_user_type_teacher.gif");
          } else if (appInstance.globalData.userInfo.user_type == 2) {    //  学生
            appInstance.globalData.userInfo.userType_img = appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/user_type/center_user_type_student.gif");
          }
          
          console.log("登录号");
          console.log(appInstance.globalData.userInfo);

          // 登录成功保存userinfo
          wx.setStorageSync("openId", appInstance.globalData.userInfo.open_id);

          wx.navigateBack();
        }); 
        
      } else {
        console.log("表示微信登录失败");
      }
    });
  }  ,
  getUserInfoManager:function(){
    appInstance.network.fetchManager('Teacher_ThirdLogin', { "login_type": "0", "phone": "15669969617" }).then(res => {
      appInstance.globalData.userInfo = res;
      // 1. 头像变更
      appInstance.globalData.userInfo.avatar = appInstance.appUntilManager("fixedImgUrl", appInstance.globalData.userInfo.avatar),
      console.log(appInstance.globalData);
    });

  },


  loginManager:function(){
    wx.login({
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {
        console.log(res);
      },
    })
  },

  thirdLoginManager:function(){

      
  }
})