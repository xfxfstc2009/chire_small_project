var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    examList: [], // 考试内容
    segmentList: [], // 班级列表
    currentId: "", // 当前选择的
    hasNeedReload:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.sendRequestToGetClassList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (this.data.hasNeedReload == true){
      this.sendRequestToGetExamList();
      this.data.hasNeedReload = false;
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  btnTap: function() {
    wx.navigateTo({
      url: '../examadd/examadd',
    })
  },

  /** 【segment】
   * 获取班级列表
   */
  sendRequestToGetClassList: function() {
    var that = this;
    appInstance.network.fetchManager('Class_List', {}).then(res => {
      var firstItem = res.list[0];
      that.data.currentId = firstItem.id;
      that.setData({
        currentId: firstItem.id,
        segmentList: res.list,
      })
      that.sendRequestToGetExamList();
    });
  },

  /**
   * segmentList点击
   */
  segmentListTap: function(e) {
    console.log("====");
    console.log(e);
    this.setData({
      currentId: e.currentTarget.id,
    });
    this.sendRequestToGetExamList();
  },


  /**
   * 获取试题列表
   */
  sendRequestToGetExamList: function() {
    var that = this;
    var param = { classId: this.data.currentId};
    console.log(param);
    appInstance.network.fetchManager('Exam_File_List', param).then(res => {
      var tempFileList = res.list;

      tempFileList.map(function(item) {
        var datetimelist = item.datetime.split(" ")
        item.date = datetimelist[0];

        // 1. 获取到第一张图片
        var firstUrl = "";
        if (item.filesList.length > 0){
          firstUrl = item.filesList[0];
        } else {
          firstUrl = "";
        }

        // 2.第一张图片
        var firstImg = "";
        if (appInstance.util.hasSuffix(firstUrl, "docx")) {
          firstImg = "doc";
        } else if (appInstance.util.hasSuffix(firstUrl, "doc")) {
          firstImg = "doc";
        } else if (appInstance.util.hasSuffix(firstUrl, "png")) {
          firstImg = "png";
        } else if (appInstance.util.hasSuffix(firstUrl, "gif")) {
          firstImg = "gif";
        } else if (appInstance.util.hasSuffix(firstUrl, "html")) {
          firstImg = "html";
        } else if (appInstance.util.hasSuffix(firstUrl, "mp3")) {
          firstImg = "mp3";
        } else if (appInstance.util.hasSuffix(firstUrl, "mp4")) {
          firstImg = "mp4";
        } else if (appInstance.util.hasSuffix(firstUrl, "ppt")) {
          firstImg = "ppt";
        } else if (appInstance.util.hasSuffix(firstUrl, "psd")) {
          firstImg = "psd";
        } else if (appInstance.util.hasSuffix(firstUrl, "ttf")) {
          firstImg = "ttf";
        } else if (appInstance.util.hasSuffix(firstUrl, "txt")) {
          firstImg = "txt";
        } else if (appInstance.util.hasSuffix(firstUrl, "xls") || appInstance.util.hasSuffix(firstUrl, "xlsx")) {
          firstImg = "xls";
        } else {
          firstImg = "com";
        }

        item.img = appInstance.appUntilManager("fixedImgUrl", "smallprogream/file/" + firstImg + ".png");
      });
      that.setData({
        examList: tempFileList,
      })
    });
  },


  // 测验内容点击
  examItemClick: function(e) {
    //1. 获取当前的exam Item
    var selectedItem = {};
    this.data.examList.map(function(item) {
      if (item.name == e.currentTarget.id) {
        selectedItem = item;
      }
    });
    var that = this;

var infoTempList = [];
    infoTempList.push('编辑学生成绩');
    infoTempList.push('编辑文档');

    wx.showActionSheet({
      itemList: ['编辑学生成绩', "编辑文档"],
      itemColor: "#333333",
      success: function(res) {
        if (!res.cancel) {
         if (res.tapIndex == 0) {
            wx.navigateTo({
              url: '../score/exam_score?classid=' + that.data.currentId + " &examid=" + selectedItem.id,
            })
          } else if (res.tapIndex == 1) {
            console.log(selectedItem);
            wx.navigateTo({
              url: '../examfileadd/examfileadd?classid=' + that.data.currentId + "&examid=" +selectedItem.id,
            })
          }
        }
      },
      fail: function(res) {},
      complete: function(res) {},
    })


  },
  addExamInfoManager: function(e) {
    var url = '../examfileadd/examfileadd?classid' + this.data.currentId;
    console.log(url);
    wx.navigateTo({
      url: '../examfileadd/examfileadd?classid=' + this.data.currentId
    })
  },
})