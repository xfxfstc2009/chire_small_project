var appInstance = getApp();
var transferClassid = "";
var transferExamId = "";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    files: [],
    infoName: "",
    beizhuInfo: "",
    beizhuCount: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (appInstance.util.isEmpty(options.classid) != true) {
      transferClassid = options.classid;
      this.setData({
        transferClassid: options.classid,
      })
    }
    if (appInstance.util.isEmpty(options.examid) != true) {
      transferExamId = options.examid;
      this.setData({
        transferExamId: options.examid,
      })
    }

    console.log(options);
    if (appInstance.util.isEmpty(options.examid) != true && appInstance.util.isEmpty(options.classid) != true) {
      this.sendRequestToGetExamDetail();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  /**
   * 获取输入框内容
   */
  getInfoName: function(e) {
    this.setData({
      infoName: e.detail.value,
    })
  },
  /**
   * 获取备注内容
   */
  getBeizhuInfo: function(e) {
    this.setData({
      beizhuCount: e.detail.value.length,
      beizhuInfo: e.detail.value,
    })
  },


  /**
   * 选择文件
   */
  chooseFiles: function() {
    var that = this;
    wx.chooseMessageFile({
      count: 10,
      type: "all",
      success(res) {
        // 选择文件后进行上传
        that.fileUploadingManager(res.tempFiles);
      }
    })
  },


  /**
   * 进行上传图片
   */
  fileUploadingManager: function(filesArr) {
    var filesTempArr = [];
    for (var i = 0, len = filesArr.length; i < len; ++i) {
      var filesSingleStr = filesArr[i].path;
      filesTempArr.push(filesSingleStr);
    }
    var that = this;

    appInstance.util.uploadImgManager(filesTempArr).then(res => {
      var uploaderFileTempList = that.data.files;

      for (var i = 0; i < res.length; i++) {
        var singleInfo = res[i];
        var imgInfo = { img: appInstance.util.getImgWithFileName(singleInfo), url: singleInfo};
        uploaderFileTempList.push(imgInfo);
      }

      that.setData({
        files: uploaderFileTempList,        
      })
    });
  },


  /**
   * 上传地址
   */
  uploadInfoManager: function() {
    var that = this;
    that.sendRequestToUploadInfoManager()
  },

  /**
   * 进行上传考试文件
   */
  sendRequestToUploadInfoManager() {
    var that = this;
    var filesListStr = "";
    for (var i = 0; i < this.data.files.length; i++) {
      var singleItem = this.data.files[i];
      var singleUrl = singleItem.url;
      if (i == this.data.files.length - 1) {
        filesListStr = filesListStr + singleUrl;
      } else {
        filesListStr = filesListStr + singleUrl + ",";
      }
    }

    var params = {
      name: this.data.infoName,
      url: filesListStr,
      beizhu: this.data.beizhuInfo,
      classId: transferClassid,
    };

    appInstance.network.fetchManager('Exam_File_Add', params).then(res => {
      wx.navigateBack({});
    });
  },

  /**
   * 根据当前的examid获取测验信息
   */
  sendRequestToGetExamDetail: function() {
    var that = this;
    var params = {
      examId: transferExamId,
      classid: transferClassid
    };
    appInstance.network.fetchManager('Exam_File_Detail', params).then(res => {
     var tempFileList = [];
      res.filesList.map(function (item) {
        console.log(item);
        console.log(appInstance.util.getImgWithFileName(item));


        var nItem = { img: appInstance.util.getImgWithFileName(item), url: item};
        tempFileList.push(nItem);
      });


      that.setData({
        infoName: res.name,
        beizhuInfo: res.beizhu,
        beizhuCount: res.beizhu.length,
        files: tempFileList,
      })
    });
  },

  /**
   * 查看文件详情
   */
  previewImage: function(e) {
    console.log(e);
    var url = appInstance.appUntilManager("fixedImgUrl", e.currentTarget.id);
    wx.navigateTo({
      url: '../../../utils/webview/webview?url=' + e.currentTarget.id
    })
  },

  /**
   * 修改文件
   */
  updateInfoManager:function(){

    var that = this;
    var filesListStr = "";
    for (var i = 0; i < this.data.files.length; i++) {
      var singleItem = this.data.files[i];
      var singleUrl = singleItem.url;
      if (i == this.data.files.length - 1) {
        filesListStr = filesListStr + singleUrl;
      } else {
        filesListStr = filesListStr + singleUrl + ",";
      }
    }

    var params = {
      name: this.data.infoName,
      url: filesListStr,
      beizhu: this.data.beizhuInfo,
      classId: transferClassid,
      fileId:transferExamId,
      examId:transferExamId,
    };

    appInstance.network.fetchManager('Exam_File_Update', params).then(res => {
      wx.navigateBack({});
    });
  },
  // 删除文件
  deleteManager:function(){
    var params = {
      id: transferExamId,
    };
    console.log(params);
    appInstance.network.fetchManager('Exam_File_Del', params).then(res => {
      // 4. 返回
      var pages = getCurrentPages();
      var prevPage = pages[pages.length - 2];
      var that = this;
     
        prevPage.setData({
          hasNeedReload: true,
        })
    
      wx.navigateBack({
        delta: 1,
      });

    });
  }
})