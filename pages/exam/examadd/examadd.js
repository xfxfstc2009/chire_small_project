var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    chanelTypes: [{ name: "学习资料",value:0, checked:true},
      { name: "考试试题", value: 1,checked: false },
      { name: "公司协议", value: 2, checked: false },
    ],
    files:[],
    infoName:"",
    uploaderFile:"",
    chanelType:"",
    beizhuInfo:"",
    beizhuCount:0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  /**
   * 获取输入框内容
   */
  getInfoName:function(e){
    this.setData({
      infoName:e.detail.value,
    })
  },
   /**
   * 获取备注内容
   */
  getBeizhuInfo:function(e){
    this.setData({
      beizhuCount:e.detail.value.length,
      beizhuInfo:e.detail.value,
    })
  },

  /**
   * 选择
   */
  checkboxChange:function(e){
    var radioItems = this.data.chanelTypes;
    for (var i = 0, len = radioItems.length; i < len; ++i) {
      radioItems[i].checked = radioItems[i].value == e.detail.value;
    }

    this.setData({
      chanelTypes: radioItems
    });
  },

    /**
   * 上传地址
   */
  uploadInfoManager:function(){
    var that = this;
    that.sendRequestToUploadInfoManager()
  },

    /**
   * 选择文件
   */
  chooseFiles:function(){
    var that = this;
    wx.chooseMessageFile({
      count:10,
      type:"all",
      success(res){
        that.setData({
          files: that.data.files.concat(res.tempFiles)
        })
        that.fileUploadingManager(res.tempFiles);
      }
    })
  },
  /**
   * 进行上传图片
   */
  fileUploadingManager:function(filesArr){
    var filesTempArr = [];
    for (var i = 0, len = filesArr.length; i < len; ++i) {
      var filesSingleStr = filesArr[i].path;
      filesTempArr.push(filesSingleStr);
    }
    var that = this;
    appInstance.util.uploadImgManager(filesTempArr).then(res => {
      that.setData({
        uploaderFile:res,
      })
    });
  },
  /**
   * 进行上传图片
   */
  sendRequestToUploadInfoManager(nameInfo , urlInfo){
    var that = this;
    appInstance.network.fetchManager('Exam_File_List', { name: nameInfo,url:urlInfo}).then(res=>{

    });
  }
})