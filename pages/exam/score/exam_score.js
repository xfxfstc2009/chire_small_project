var appInstance = getApp();
var transferClassId = "";
var transferExamId = "";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    examScoreList:[],           // 获取到的成绩信息
    currentStudentId: "",       // 选择的学生的ud
    currentStudent:{},          // 选中的学生
    scoreInput: "",             // 分数输入内容
    scoreFile: [],
    markInput: "",
    beizhuCount:0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options);
    if (appInstance.util.isEmpty(options.classid) != true) {
      transferClassId = options.classid;
    }
    if (appInstance.util.isEmpty(options.examid) != true) {
      transferExamId = options.examid;

    }
    console.log(transferExamId);
    console.log(transferClassId);

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.sendRequestToGetCurrentExamScore();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  // segmnen 点击
  segmentListTap:function(e){
    // 1. 搜索获取到当前的学生信息
    var selectedStudentItem = {};
    this.data.examScoreList.map(function (item) {
      if (item.student.id == e.currentTarget.id){
        selectedStudentItem = item;
      }
    });


    
    this.setData({
      currentStudentId:e.currentTarget.id,
      currentStudent: selectedStudentItem,
      // 清空上个学生的信息
      scoreInput: selectedStudentItem.score, 
      scoreFile: selectedStudentItem.imgList,
      markInput: selectedStudentItem.marks,
      beizhuCount: selectedStudentItem.marks.length,
    })
  },

  // 分数
  scoreInputManager: function(e) {
    console.log(e);
    // 1. 获取输入内容
    this.data.scoreInput = e.detail.value;
  },

  // 备注
  markInputManager:function(e){
    console.log(e);
    // 1. 获取输入内容
    this.data.markInput = e.detail.value;
    this.setData({
      beizhuCount: this.data.markInput.length,
    })
  },

  /**
  * 选择图片
  */
  chooseFiles: function () {
    var that = this;
    // 1. 选择图片
    appInstance.util.chooseImgAblumManager(9).then(res => {
      // 进行上传
      appInstance.util.uploadImgManager(res).then(res => {          // 2. 上传图片
        that.data.scoreFile = res;
        console.log(res);
        that.setData({
          scoreFile: res,
        })
      });
    });
  },

  // 图片点击
  imgSelectedManager: function (e) {
    var selectedImgUrl = e.currentTarget.id;
    var that = this;
    wx.showActionSheet({
      itemList: ['查看图片', '删除图片'],
      success: function (res) {
        if (!res.cancel) {
          if (res.tapIndex == 0) {   // 图片放大
            that.imgShowsManager(selectedImgUrl);
          } else if (res.tapIndex == 1) {    // 删除图片
            if (appInstance.util.isEmpty(transferClassId) == true) {
              that.imgDeleteManager(selectedImgUrl);
            } else {
              that.sendRequestToDelImg(selectedImgUrl,that.data.currentStudent.linkId);
            }
          }
        }
      }
    })
  },
  // 图片放大
  imgShowsManager: function (selectedImg) {
    var that = this;
    wx.previewImage({
      current: selectedImg,
      urls: that.data.scoreFile,
    })
  },
  // 删除图片
  imgDeleteManager: function (selectedImg) {
    var that = this;
    var tempImgList = [];
    tempImgList = this.data.scoreFile;

    var selectedIndex = tempImgList.indexOf(selectedImg);
    tempImgList.splice(selectedIndex, 1);

    this.imgList = tempImgList;
    this.setData({
      scoreFile: tempImgList,
    })
  },

  
  /*
    获取当前学生成绩
  */
  sendRequestToGetCurrentExamScore:function(){
    var that = this;
    var params = {
      examId: transferExamId,
      classId:transferClassId
    };
    appInstance.network.fetchManager('Exam_Detail', params).then(res => {
      var currentStudentTemp= res.list[0];
      this.setData({
        examScoreList:res.list,
        currentStudentId: res.list[0].student.id,
        currentStudent: currentStudentTemp,
        scoreFile: currentStudentTemp.imgList,
        markInput: currentStudentTemp.marks,
        scoreInput: currentStudentTemp.score,
        beizhuCount: currentStudentTemp.marks.length,
      })
    });
  },
/*
    添加学生成绩内容
  */
  addScoreManager: function () {
    this.sendRequestToAddScoreInfoManager();
  },

  sendRequestToAddScoreInfoManager: function () {
    var that = this;
    var fileUrlInfoStr = "";
    for (var i = 0; i < this.data.scoreFile.length; i++) {
      var info = this.data.scoreFile[i];
      if (i == this.data.scoreFile.length - 1) {
        fileUrlInfoStr = fileUrlInfoStr + info;
      } else {
        fileUrlInfoStr = fileUrlInfoStr + info + ",";
      }
    }
    var params = {
      studentId: this.data.currentStudentId,
      examId: transferExamId,
      score: this.data.scoreInput,
      marks: this.data.markInput,
      fileUrl: fileUrlInfoStr,
      classId:transferClassId
    };
    console.log(params);
    
    appInstance.network.fetchManager('Exam_Add', params).then(res => {
      that.data.currentStudent.imgList = this.data.scoreFile;
      that.data.currentStudent.marks = this.data.markInput;
      that.data.currentStudent.score = this.data.scoreInput;

      var tempStudentList = that.data.examScoreList;

      tempStudentList.map(function (item) {
        if (item.student.id == that.data.currentStudentId) {
          item = that.data.currentStudent;
        }
      });

      that.setData({
        examScoreList:tempStudentList,
      })

    });
  },

  // 删除学生图片
  sendRequestToDelImg(img ,linkId){
    var params = { img: img, linkId:linkId};
    var that = this;

    appInstance.network.fetchManager('Exam_Img_Del', params).then(res => {
      var tempCurrentStudent = that.data.currentStudent;
      // 1. 找到对应的学生对应图片删除
      var selectedIndex = tempCurrentStudent.imgList.indexOf(img);
      tempCurrentStudent.imgList.splice(selectedIndex, 1);

      console.log(tempCurrentStudent);

      // 2. 将所有的学生更新
      var tempExamScoreList = that.data.examScoreList;
      tempExamScoreList.map(function (item) {
        if (item.student.id == that.data.currentStudentId) {
          item = tempCurrentStudent
        }
      });

      console.log(tempExamScoreList);
      // 3. 更新所有的学生
      that.setData({
        examScoreList: tempExamScoreList,
        scoreFile: tempCurrentStudent.imgList,
      })
    });
    
  }



})