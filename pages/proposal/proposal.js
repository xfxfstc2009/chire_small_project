var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    inputInfo:"",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  inputManager:function(e){
    this.data.inputInfo = e.detail.value;
  },
  sendRequestToUpload:function(){
    var params = { name:"匿名",address:"其他",link:"其他",title:"标题",des:this.data.inputInfo};
    var that = this;
    appInstance.network.fetchManager('Contact', params).then(res => {
      that.showSuccessedAlert();
    })
  },

  // 
  showSuccessedAlert:function(e){
    wx.showModal({
      title: '提交成功',
      content: '感谢你的意见与建议。我们将会仔细分析采纳。',
      confirmText: "确定",
      success: function (res) {
        console.log(res);
        if (res.confirm) {
          wx.navigateBack({
            
          });
        } else {
          
        }
      }
    });
  }

})