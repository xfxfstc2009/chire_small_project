  var appInstance = getApp();
  Page({

    /**
     * 页面的初始数据
     */
    data: {
      transferId:"",
      subjectName:"",
      files:[],
      selectedUrl:"",
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
      if (appInstance.util.isEmpty(this.data.transferId) == true) {
        this.data.transferId = options.id;
        this.setData({
          transferId : options.id,
        })
        this.sendRequestToGetSubjectInfo(options.id);
      }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
      
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
      
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
      
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
      
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
      
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
      
    },
    

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
      
    },

    /**
     * 输入内容
     */
    subjectNameInput: function (e) {
      this.data.subjectName = e.detail.value;
    },
    /**
     * 选择照片
     */
    chooseFiles:function(e){
      var that = this;
      // 1. 选择图片
      appInstance.util.chooseImgAblumManager(1).then(res => {
        // 进行上传
        appInstance.util.uploadImgManager(res).then(res => {          // 2. 上传图片
          that.data.files = res;
          that.data.selectedUrl = that.data.files[0];
          that.setData({
            files:res,
          })
        });
      });
    },
    /**
     * 图片放大
     */
    previewImage:function(e){
      console.log(e);
      wx.previewImage({
        current: e.currentTarget.id, // 当前显示图片的http链接
        urls: this.data.files // 需要预览的图片http链接列表
      })
    },

  /*** 科目修改*/
  updateManager:function(e){
    this.sendRequestToUpdateSubject();
  },
  /*** 科目修改接口*/
  sendRequestToUpdateSubject: function (e) {
    var that = this;
    var params = { id: this.data.subjectId, name: this.data.subjectName, img: that.data.selectedUrl };
    console.log(params);
    appInstance.network.fetchManager('SubjectUpdate', params).then(res => {
      wx.navigateBack({});
    })
  },
  /*** 科目添加接口*/
  addManager:function(e){
    this.sendRequestToAddSubject();
  },
  
  /*** 科目添加接口*/
  sendRequestToAddSubject: function () {
    var that = this;
    var params = { name: this.data.subjectName, img: that.data.selectedUrl };
    appInstance.network.fetchManager('SubjectAdd', params).then(res => {
      wx.navigateBack({});
    })
  },
  /*** 科目删除方法*/
    deleteManager:function(e){
    this.sendRequestToDelSubject();
  },
  /*** 科目删除接口*/
  sendRequestToDelSubject: function () {
    var that = this;
    var params = { id: this.data.transferId };
    appInstance.network.fetchManager('SubjectDel', params).then(res => {
      wx.navigateBack({});
    })
  },
  /*** 获取到科目详情*/
  sendRequestToGetSubjectInfo: function (e) {
    var that = this;
    var params = { id: e };

    appInstance.network.fetchManager('SubjectInfo', params).then(res => {
      var imgList = [];
      var imgMainUrl = "";
      if (appInstance.util.isEmpty(res.img)!=true){
        imgMainUrl = appInstance.util.fixedImgUrl(res.img, "");;
        imgList.push(imgMainUrl);
        this.data.selectedUrl = imgList[0];
      }
    
      that.setData({
        subjectName: res.name,
        subjectId: e,
        files: imgList,
      })
    });
  },
})