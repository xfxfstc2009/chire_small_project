var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    subjectList:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.sendRequestToGetSubjectList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  /**
   * 添加科目信息
   */
  addSubjectManager:function(){
    wx.navigateTo({
      url: 'subjectadd/subjectadd',
    })
  },
  /**
  * 获取科目列表
  */
  sendRequestToGetSubjectList: function () {
    var that = this;
    appInstance.network.fetchManager('SubjectList', {}).then(res => {
      var tempArr = res.list;
      
      tempArr.map(function (item) {
        let imgInfo = {
          url: item.img,
          width: 60,
          height: 60
        };

        item.ablum = appInstance.appUntilManager("fixedImgUrl", imgInfo);
        item.time = appInstance.util.dateManager('Y-m-d', item.createTime);
      });
      
      that.setData({
        subjectList: tempArr,
      })
    });
  },

  // 显示actionSheet
  subjectTapManager: function (e) {
    var selectedId = e.currentTarget.id;
    var that = this;
    wx.showActionSheet({
      itemList: ['查看科目', '删除科目'],
      success: function (res) {
        if (!res.cancel) {

          if (res.tapIndex == 0) {   // 查看科目
            wx.navigateTo({
              url: 'subjectadd/subjectadd?id='+selectedId,
            })
          } else if (res.tapIndex == 1) {    // 删除科目
            that.sendRequestToDeleteSubject(selectedId);
          }
        }
      }
    })
  },
  // 1. 删除科目
  sendRequestToDeleteSubject:function(e){
    var that = this;
    var params = { id: e };
    appInstance.network.fetchManager('SubjectDel', params).then(res => {
      that.sendRequestToGetSubjectList();
    })
  }


  
})
