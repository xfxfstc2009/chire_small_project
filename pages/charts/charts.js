
let Charts = require('../../utils/charts/wxchart/wxcharts.js')
var lineChart = null;

Page({
  data: {
    deviceH: 0,
    deviceW: 0,
    date: ''
  },
  onLoad: function (options) {
    let _this = this

    wx.getSystemInfo({
      success: function (res) {
        _this.setData({
          deviceH: res.windowHeight,
          deviceW: res.windowWidth,
        })
      }
    })

    //获取系统当前时间
    let that = this
    var myDate = new Date();
    var date = myDate.toLocaleDateString();
    console.log(date)
    that.setData({
      date: date,  //选择时间
    })

    // 折线图
    var windowWidth = '', windowHeight = '';    //定义宽高
    try {
      var res = wx.getSystemInfoSync();    //试图获取屏幕宽高数据
      windowWidth = res.windowWidth / 750 * 700   //以设计图750为主进行比例算换
      windowHeight = res.windowWidth / 750 * 500    //以设计图750为主进行比例算换
    } catch (e) {
      console.error('getSystemInfoSync failed!');   //如果获取失败
    }

    lineChart = new Charts({
      canvasId: 'areacanvas',
      type: 'line',
      animation: true,  //是否开启动画
      enableScroll:true,
      categories: ['06', '07', '08', '09', '10', '11', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12'],
      series: [{
        name: '月教师好评情况',
        data: [15, 20, 45, 37, 40, 80, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25],
        format: function (val) {
          return val.toFixed(2) + '分';
        }
      }],
      xAxis: {   //是否隐藏x轴分割线
        disableGrid: true,
      },
      yAxis: {
        title: '教师好评总分数',
        format: function (val) {
          return val.toFixed(2);
        },
      },
      width: windowWidth, //图表展示内容宽度
      height: windowHeight, //图表展示内容高度
      dataLabel: true,    //是否在图表上直接显示数据
      dataPointShape: true, //是否在图标上显示数据点标志
      extra: {
        lineStyle: 'curve'  //曲线
      },
    });
  },
  touchHandler: function (e) {
    lineChart.showToolTip(e, {
      // background: '#7cb5ec',
      format: function (item, category) {
        return category + ' ' + item.name + ':' + item.data
      }
    });
  },
  //初始化图表
  initGraph: function () {
    new Charts({
      canvasId: 'pieCanvas',
      type: 'pie',
      series: [{
        name: '成交量1',
        data: 15,
      }, {
        name: '成交量2',
        data: 35,
      }, {
        name: '成交量3',
        data: 78,
      }, {
        name: '成交量4',
        data: 63,
      }],
      width: this.data.deviceW,
      height: this.data.deviceH,
      dataLabel: false
    });

  },

  onReady: function () {
    this.initGraph()
  },
  touchstart: function (e) {

    //console.info(e);

    lineChart.scrollStart(e);//开始滚动

  },

  touchmove: function (e) {

    //console.info(e);

    lineChart.scroll(e);

  },

  touchend: function (e) {

    //console.info(e);

    lineChart.scrollEnd(e);

  },

})