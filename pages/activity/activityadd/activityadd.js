var appInstance = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    desLength:0,
    imgList:[],
    nameInput:"",
    desInput:"",
    transferId : "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    if (appInstance.util.isEmpty(options.id) != true){
      this.data.transferId = options.id;
      this.setData({
        transferId: options.id,
      })
    } 
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    if(appInstance.util.isEmpty(this.data.transferId) != true){
      this.sendRequestToGetActivityDetailManager();
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log(this.data.desInput);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },

  /**
   * 选择图片
   */
  chooseFiles:function(){
    var that = this;
    // 1. 选择图片
    appInstance.util.chooseImgAblumManager(9).then(res=>{
      // 进行上传
      appInstance.util.uploadImgManager(res).then(res => {          // 2. 上传图片
        that.imgList = res;
        that.setData({
            imgList:res,
        })
      });
    });
  },

  // 图片点击
  imgSelectedManager: function (e) {
    var selectedImgUrl = e.currentTarget.id;
    var that = this;
    wx.showActionSheet({
      itemList: ['查看图片', '删除图片'],
      success: function (res) {
        if (!res.cancel) {
          if (res.tapIndex == 0) {   // 图片放大
            that.imgShowsManager(selectedImgUrl);
          } else if (res.tapIndex == 1) {    // 删除图片
            if (appInstance.util.isEmpty(this.data.transferId) == true) {
              that.imgDeleteManager(selectedImgUrl);
            } else {
              that.sendRequestToDelImgManager(selectedImgUrl);
            }
          }
        }
      }
    })
  },
  // 图片放大
  imgShowsManager: function (selectedImg) {
    var that = this;
    wx.previewImage({
      current: selectedImg,
      urls: that.data.imgList,
    })
  },
  // 删除图片
  imgDeleteManager: function (selectedImg) {
    var that = this;
    var tempImgList = [];
    tempImgList = this.data.imgList;

    var selectedIndex = tempImgList.indexOf(selectedImg);
    tempImgList.splice(selectedIndex, 1);

    this.imgList = tempImgList;
    this.setData({
      imgList: tempImgList,
    })
  },


  /** * 名字输入*/
  nameInputManager:function(e){
    this.setData({
      nameInput: e.detail.value,
    })
  },

  /** * 内容输入*/
  desInputManager:function(e){
    this.setData({
      desInput:e.detail.value,
      desLength:e.detail.value.length,
    })
  },
  
  /*** 添加活动*/
  addManager: function () {
    this.sendRequestToAddActivityManager(this.data.nameInput, this.data.desInput, this.data.imgList);
  },

  sendRequestToAddActivityManager:function(titleInfo,desInfo,imgArr){
    var that = this;
    var imgStr = "";
    for (var i = 0, len = imgArr.length; i < len; i++) {
      var infoStr = imgArr[i];
      if (i == imgArr.length - 1){
        imgStr = imgStr + infoStr;
      } else {
        imgStr = imgStr + infoStr + ",";
      }
    }
    var params = { title: titleInfo, html: desInfo, imgs: imgStr };
    appInstance.network.fetchManager('ActivityAdd', params ).then(res => {
      wx.navigateBack({});
    });
  },

  /*** 查看活动详情*/
  sendRequestToGetActivityDetailManager: function (e) {
    var that = this;
    var params = {id:this.data.transferId};
    appInstance.network.fetchManager('ActivityDetail',params).then(res=>{
      var tempImgList = [];
      res.imgList.map(function (item) {
        var imgUrl = appInstance.appUntilManager("fixedImgUrl", item);
        tempImgList.push(imgUrl);
      });
      
      that.setData({
        imgList: tempImgList,
        nameInput:res.title,
        desInput:res.des,
        desLength:res.des.length,
      })
    });
  },
  
  /*** 删除图片*/
  sendRequestToDelImgManager:function(img){
    var that = this;
    var params = {id:this.data.transferId,img:img};
    appInstance.network.fetchManager('ActivityImgDel',params).then(res=>{
      that.imgDeleteManager(img);
    });
  },
  /*** 删除活动*/
  deleteManager:function(){
    this.sendRequestToDelActivityManager();
  },
  sendRequestToDelActivityManager: function () {
    var that = this;
    var params = { id: this.data.transferId };
    appInstance.network.fetchManager('ActivityDel', params).then(res => {
      wx.navigateBack({})
    })
  },

  actionToAddDetail:function(){
    var params = {};
    wx.navigateTo({
      url: '../../editor/editor?type=activity',
    })
  }
})
