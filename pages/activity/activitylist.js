var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    activityList: [],

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.sendRequestGetActivityList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  /**
   * 添加活动
   */
  actionClickItemsManager: function(e) {
    wx.navigateTo({
      url: 'activityadd/activityadd?id=' + e.currentTarget.id,
    })
  },
  addActivityManager: function(e) {
    wx.navigateTo({
      url: 'activityadd/activityadd',
    })
  },

  /**
   * 活动列表
   */
  sendRequestGetActivityList: function() {
    var that = this;
    appInstance.network.fetchManager('ActivityList', {}).then(res => {
      console.log(res);
      var tempArr = res.list;
      tempArr.map(function(item) {
        var items = item;
        if (items.imgList.length > 0){
          items.ablum = appInstance.appUntilManager("fixedImgUrl", items.imgList[0]);
        } 
        
        items.time = appInstance.util.dateManager('Y-m-d', items.timeInter);
      });

      that.setData({
        activityList: tempArr,
      })
    })
  }

})