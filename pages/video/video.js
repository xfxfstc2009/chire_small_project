var appInstance = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    videoList:[],
    // 当前视频Url
    currentVideo:{},
    // 当前页码
    currentPage:1,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.sendRequestToGetVideoList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.data.currentPage =1;
    this.sendRequestToGetVideoList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    this.data.currentPage ++;
    this.sendRequestToGetVideoList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  /**
   * 接口-获取视频信息
   */
  sendRequestToGetVideoList: function() {
    var that = this;
    var paramas = {"page":this.data.currentPage,"size":"10"};
    appInstance.network.fetchManager('Video_List', paramas).then(res => {
      var tempList = res.list;

      tempList.map(function(item) {
        var items = item;
        if (appInstance.util.isEmpty(items.ablum) != true) {
          let imgInfo = {
            url: items.ablum,
            width: 150,
            height: 150
          };
          items.ablum = appInstance.appUntilManager("fixedImgUrl", imgInfo);
        }
      });

      
      if (that.data.currentPage == 1){
        that.setData({
          videoList: tempList
        })
      } else {
        that.setData({
          videoList: that.data.videoList.concat(tempList)
        })
      }
     
      
      if (this.videoList!=null){
        var firstItem = that.data.videoList[0];
        console.log(firstItem);
      }      
    });
  },
  
  videoTapManager: function(e) {
    var singleItemId = e.currentTarget.id;

    var selectedItem={};

    this.data.videoList.map(function(item) {
      if (item.id == singleItemId){
        selectedItem = item;
        console.log("123");
      }
    });
      
    this.setData({
      currentVideo:selectedItem
    })

  }

})