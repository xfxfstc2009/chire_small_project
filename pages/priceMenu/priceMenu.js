var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    priceMenuList:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.sendRequestToGetPriceMenuInfoManager();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  /**
   * 接口获取价目表
   */
  sendRequestToGetPriceMenuInfoManager:function(){
    var that = this;
    appInstance.network.fetchManager('PriceMenuList', {}).then(res => {
   
      that.setData({
        priceMenuList: res.list,
      })
    })
  },
   /**
   * 添加价目表
   */
  actionAddPriceMenu:function(){
    wx.navigateTo({
    url: 'priceMenuAdd/priceMenuAdd',
    })
  },
  /**
   * 价目表点击
   */
  priceMenuItemClick:function(e){
    wx.navigateTo({
      url: 'priceMenuAdd/priceMenuAdd?id=' + e.currentTarget.id,
    })
  }

})