var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    nameInput:"",       // 价目名称
    priceInput:"",      // 价目价格
    des:"",             // 价目详情
    priceMenuItemList:[],
    chooseAddPriceInfoShow:false, // 输入价目信息显示方法
    priceMenuItemInput:"",
    transferId:"",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (appInstance.util.isEmpty(options.id) != true){
      this.data.transferId = options.id;
      this.setData({
        transferId : options.id,
      })
      // 1.进行更新数据
      this.inferfaceGetPriceMenuInfoManager(options.id);
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (e) {
    
  },
  // 输入信息
  nameInputManager: function (e){
    this.data.nameInput = e.detail.value;
  },
  // 价目价格
  priceInputManager:function(e){
    this.data.priceInput = e.detail.value;
  },
  // 价目简介
  detailInfoManager:function(e){
    this.data.des = e.detail.value;
  },
  // 添加价目表
  sendRequestToAddPriceMenu:function(e){
    this.interfaceAddPriceMenuManager();
  },
  
  // 添加更多
  moreActionManager:function(e){
    this.setData({
      chooseAddPriceInfoShow:true,
    })
  },
  priceMenuItemManager:function(e){
    this.data.priceMenuItemInput = e.detail.value;
  },
  addPriceMenuItemManager:function(e){
    var priceMenuList = [];
    priceMenuList = this.data.priceMenuItemList;
    if (appInstance.util.isEmpty(this.data.priceMenuItemInput) != true){
      priceMenuList.push(this.data.priceMenuItemInput);
    }

    this.setData({
      priceMenuItemList: priceMenuList,
      priceMenuItemInput:"",
    })

    // 清空输入内容
    this.data.priceMenuItemInput = "";

    this.closeDialog();
  },
  // 关闭价目信息,
  closeDialog:function(){
    this.setData({
      chooseAddPriceInfoShow: false
    })
  },
  // 删除价目信息
  itemClickAction:function(e){
    var that = this;
    wx.showActionSheet({
      itemList: ["删除"],
      success: function (res) {
        if (!res.cancel) {
          // 1. 找到对应的内容进行删除
          var tempArr = that.data.priceMenuItemList;
          console.log(tempArr);
          var selectedIndex = tempArr.indexOf(e.currentTarget.id);
          tempArr.splice(selectedIndex, 1);          

          that.setData({
            priceMenuItemList:tempArr,
          })
        }
      }
    });
  },
  // 添加

  //接口
  interfaceAddPriceMenuManager:function(e){
    var that = this;
    var mainDes = "";
    if (this.data.priceMenuItemList.length > 0){
      for (var i = 0; i < this.data.priceMenuItemList.length;i++){
        var singleItem = this.data.priceMenuItemList[i];
        if (i == this.data.priceMenuItemList.length - 1){
          mainDes = mainDes + singleItem;
        } else {
          mainDes = mainDes + singleItem + ",";
        }
      }
    }
    var params = { name: this.data.nameInput, price: this.data.priceInput, des: mainDes};
    appInstance.network.fetchManager('PriceMenuAdd', params).then(res => {
      wx.navigateBack({
        
      });
    }); 
  },
    // 修改当前价目信息
  sendRequestToUpdatePriceMenu:function(){
    var that = this;
    var mainDes = "";
    if (this.data.priceMenuItemList.length > 0) {
      for (var i = 0; i < this.data.priceMenuItemList.length; i++) {
        var singleItem = this.data.priceMenuItemList[i];
        if (i == this.data.priceMenuItemList.length - 1) {
          mainDes = mainDes + singleItem;
        } else {
          mainDes = mainDes + singleItem + ",";
        }
      }
    }
    var params = { id: this.data.transferId, name: this.data.nameInput, price: this.data.priceInput, des: mainDes };
    appInstance.network.fetchManager('PriceMenuUpdate', params).then(res => {
      wx.navigateBack({

      });
    }); 

  },
  // 获取当前信息
  inferfaceGetPriceMenuInfoManager:function(){
    var that = this;
    var params = {id:this.data.transferId};
    appInstance.network.fetchManager('PriceMenuInfo',params).then(res=>{
      that.setData({
        nameInput:res.name,
        priceInput:res.price,
        priceMenuItemList:res.des,
      })
    })
  },
  // 删除方法
  deleteManager:function(){
    var that = this;
    var params = {id:this.data.transferId};
    appInstance.network.fetchManager('PriceMenuDel', params).then(res => {
      wx.navigateBack({
        
      });
    });
  }
})