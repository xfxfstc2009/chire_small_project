var appInstance = getApp();
var id = "";

Page({

  /**
   * 页面的初始数据
   */
  data: {
      rechargeModel : {},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    id = options.id;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.sendRequestToGetRechargeInfoManager();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },

  /**
   * 接口获取充值信息
   */
  sendRequestToGetRechargeInfoManager(){
    var that = this;
    var params = {id:id};
    appInstance.network.fetchManager('RechargeDetail', params).then(res => {
      that.data.rechargeModel = res;
      that.setData({
        rechargeModel:res,
      })
    });
  },
  chongzhiSuccessedManager:function(){
    var that = this;
    wx.navigateBack({
      
    });
  }
})