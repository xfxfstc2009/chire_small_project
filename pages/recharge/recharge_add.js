var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    studentList:[],
    studentPickerList:[],
    studentIndex:0,
    priceMenuList:[],
    priceMenuPickerList:[],
    priceMenuIndex:0,
    chongzhiJine:0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.sendRequestToGetStudentList();
    this.sendRequestToGetPriceMenuListManager();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },


  studentChangeManager:function(e){
    this.setData({
      studentIndex:e.detail.value,
    })
  },
  priceMenuChangeManager:function(e){
    this.setData({
      priceMenuIndex:e.detail.value,
    })
  },
  priceInputManager:function(e){
    this.data.chongzhiJine = e.detail.value;
  },

  /**
   * 1.获取学生列表
   */
  sendRequestToGetStudentList:function(){
    var that = this;
    var params = {};
    appInstance.network.fetchManager('StudentList', params).then(res => {
      var tempArr = res.list;
      var studentPickerTempList = [];
        tempArr.map(function (item) {
          studentPickerTempList.push(item.name);
        });
        that.setData({
          studentList:tempArr,
          studentPickerList: studentPickerTempList,
        })
    });
  },

   /**
   * 2.获取资费列表
   */
  sendRequestToGetPriceMenuListManager:function(){
    var that = this;
    var params = {};
    appInstance.network.fetchManager('PriceMenuList',params).then(res=>{
      var tempArr = res.list;
      var priceMenuPickerTempList = [];
      tempArr.map(function(item){
        priceMenuPickerTempList.push("￥" + item.price + "【" +item.name + "】");
      });
      that.setData({
        priceMenuList:res.list,
        priceMenuPickerList: priceMenuPickerTempList,
      })
    });
  },
 /**
   * 3.进行充值
   */
  sendRequestToRechargeAdd:function(){
    var that = this;
    var studentInfo = this.data.studentList[this.data.studentIndex];
    var priceMenuInfo = this.data.priceMenuList[this.data.priceMenuIndex];
    var params = { studentId: studentInfo.id, priceMenuId: priceMenuInfo.id, money:this.data.chongzhiJine};
  
    appInstance.network.fetchManager('RechargeAdd',params).then(res=>{
      wx.navigateTo({
        url: 'recharge_success',
      })
    });
  },
  actionDirectHistoryManager:function(){
    wx.navigateTo({
      url: 'history/recharge_history',
    })
  },
  


  // 弹框二次确认
  showAlertToCheckManager:function(){
    var studentInfo = this.data.studentList[this.data.studentIndex];
    var that = this;
    wx.showModal({
      title: '充值提醒',
      content: '您现在正将给【' + studentInfo.name +'】充值金额为【'+this.data.chongzhiJine+"】,请再次确认。",
      confirmText: "确认充值",
      cancelText: "取消",
      success: function (res) {
        if (res.confirm) {
          that.sendRequestToRechargeAdd();
        } else {
          
        }
      }
    });
  },
  chongzhiButtonAction:function(){
    this.showAlertToCheckManager();
  }
})