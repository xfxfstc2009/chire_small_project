
Page({
  data: {
    width: 0,
    height: 0,
    markers: [/*{
      latitude: 23.099994,
      longitude: 113.324520,
      name: 'T.I.T 创意园zz',
      desc: '我现在的位置'
    }*/],
    covers: [{
      latitude: 30.181706,
      longitude: 120.270831,
      iconPath: 'https://chire.oss-cn-hangzhou.aliyuncs.com/smallprogream/map/map_school_location.png?x-oss-process=image/resize,m_fill,w_70,h_70,limit_0/auto-orient,1/format,png'
    }]
  },
  //事件处理函数
  goParents() {
    wx.navigateTo({
      url: '../parents/parents'
    })
  },
  bindViewTap() {
    wx.getLocation({
      type: 'gcj02', //返回可以用于wx.openLocation的经纬度
      success: function (res) {
        var latitude = 30.181706;
        var longitude = 120.270831;

        wx.openLocation({
          latitude: latitude,
          longitude: longitude,
          name: "炽热日语",
          scale: 28
        })
      }
    })
  },

  onLoad() {
    var that = this
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          width: res.windowWidth,
          height: res.windowHeight - 55
        })
      }
    })

  }
})
