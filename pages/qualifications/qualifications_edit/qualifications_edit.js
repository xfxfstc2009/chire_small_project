var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    segmentSelectdIndex: 0,
    segmentSelectdId : "",
    segmentSelectdIndexName: "营业执照",
    segmentList: [],
    segmentListAddShow:false,
    zizhiInput:"",            // 资质
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.getZizhiList();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  nameInputManager:function(e){

    this.data.zizhiInput = e.detail.value;
    console.log(this.data.zizhiInput);
  },
  /**
   * segmentList点击方式
   */
  segmentListTap: function(e) {
    if (e.currentTarget.id == "-999"){
      console.log("添加");
      this.setData({
        segmentListAddShow:true,
      })
    } else {
      console.log(e);
      var selectIndex = 0;
      for (var i = 0 ; i < this.data.segmentList.length;i++){
        var singleItem = this.data.segmentList[i]
        if (singleItem.id == e.currentTarget.id){
          selectIndex = i;
          break;
        }
      }
      
      this.data.segmentSelectdIndex = selectIndex;
      var selectName = this.data.segmentList[this.data.segmentSelectdIndex].name;
      var selectId = this.data.segmentList[this.data.segmentSelectdIndex].id;
      this.setData({
        segmentSelectdId: selectId,
        segmentSelectdIndex: selectIndex,
        segmentSelectdIndexName: selectName,
      })
    }
  },

  imgSelectedManager: function(e) {
    console.log(e)
  },

  /**
   * 选择图片信息
   */
  chooseFiles: function(e) {
    var that = this;
    // 1. 选择图片
    appInstance.util.chooseImgAblumManager(1).then(res => {
      // 进行上传
      appInstance.util.uploadImgManager(res).then(res => { // 2. 上传图片
        that.data.segmentList[this.data.segmentSelectdIndex].img = res;
        var tempSegmentList = this.data.segmentList;
        var selectedSegmentItem = tempSegmentList[this.data.segmentSelectdIndex];
        selectedSegmentItem.img = res

        that.setData({
          segmentList: tempSegmentList,
        })
      });
    });
  },
  /**
   * 选择图片信息
   */
  actionManager:function(){
    var that = this;
    var nameStr = this.data.zizhiInput;
    var selectedImgList = this.data.segmentList[this.data.segmentSelectdIndex].img;
    var imgStr = "";
    if (selectedImgList.length > 0){
      imgStr = selectedImgList[0];
    }
    var params = { name: nameStr, img: imgStr};
    console.log(params);
    appInstance.network.fetchManager('QualificationsAdd', params).then(res => {
      wx.navigateBack({
        
      });
    });
  },

   /**
   * 获取列表
   */
    getZizhiList:function(){
      var that = this;
      appInstance.network.fetchManager('QualificationsList', {}).then(res => {
        var tempSegmentList = res.list;
        tempSegmentList.push({
          id:"-999",
          name:"添加条目",
          img:"",
        });
        var firstItems = tempSegmentList[0];

        that.setData({
          segmentSelectdIndex:0,
          segmentList: tempSegmentList,
        })
      });
    },

  zizhiInputManager:function(e){
    this.data.zizhiInput = e.detail.value;
  },

    // 添加资质
    addQualification:function(e){
      var tempSegmentList = this.data.segmentList;
      // 1. 获取最后一个
      var mainSegmentItem = this.data.segmentList[this.data.segmentList.length - 2];
      var segmentItem = { id: this.data.segmentList.length, name: this.data.zizhiInput,img:""};

      tempSegmentList.splice(tempSegmentList.length - 1, 0, segmentItem);

      this.setData({
        segmentList: tempSegmentList,
        segmentListAddShow: false,
      })

      console.log(tempSegmentList);
    },

    // 删除
    deleteQualificationManager:function(e){
      var params = {id:e};
      console.log(params);
      appInstance.network.fetchManager('QualificationsDelete', params).then(res => {
        wx.navigateBack({
          
        });
      });
    },

    // 长按删除
    bindlongtapManager:function(e){
      var that = this;
      wx.showActionSheet({
        itemList: ['删除'],
        success: function (res) {
          if (!res.cancel) {
            that.deleteQualificationManager(e.currentTarget.id);
          }
        }
      });
    },
  closeDialog:function(){
    this.setData({
      segmentListAddShow: false,
    })
  }
})