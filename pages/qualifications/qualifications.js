var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    qualificationsList:[],
    img:"",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.sendRequestToGetList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },

  sendRequestToGetList:function(){
    var that = this;
    appInstance.network.fetchManager('QualificationsList', {}).then(res => {
      var tempSegmentList = res.list;
      var firstItem ={};
      //1.获取第一个item
      if (tempSegmentList.length > 0){
        firstItem = tempSegmentList[0];
        
      }

      that.setData({
        qualificationsList: tempSegmentList,
        img: firstItem.img,
      })
    });
  },

  ziZhiManager:function(e){
    console.log(e);
    var infoId = e.currentTarget.id;
    var selectedItem = {};
    this.data.qualificationsList.map(function (item) {
      if (item.id == infoId){
        selectedItem = item;
      }
    });
    this.setData({
      img: selectedItem.img,
    })
    console.log(selectedItem.img);
  }
})