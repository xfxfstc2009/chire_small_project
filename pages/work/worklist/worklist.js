var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    workTypeSelected: true,
    workList1: [],
    workList0: [],
    workListMain: [],
    count1: 0,
    count0: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    var that = this;
    // 未结束作业
    getWorkListManager(1).then(res => {
      that.setData({
        workList1: res.list,
        count1: res.count,
        workListMain: res.list,
      })
    });
    // 已结束作业
    getWorkListManager(0).then(res => {
      that.setData({
        workList0: res.list,
        count0: res.count,
      })
    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  /**
   * 添加作业方法
   */
  addWorkManager: function() {
    wx.navigateTo({
      url: '../work',
    })
  },
  /**
   * segmentList点击
   */
  workTypeTap: function(e) {
    var tempSelect = false;
    if (e.currentTarget.id == '0') {
      tempSelect = false;
    } else {
      tempSelect= true;
    }
    var that = this;
    getWorkListManager(tempSelect == true ? 1 : 0).then(res => {

      that.setData({
        workListMain: res.list,
        workTypeSelected: tempSelect
      })
    });
  },

  workAction:function(e){
    wx.navigateTo({
      url: '../workStudentBack/workStudentBack?id='+e.currentTarget.id,
    })
  }
})


function getWorkListManager(invalidInput) {

  return new Promise(function(resolve, reject) {
    var params = {
      invalid: invalidInput
    };

    appInstance.network.fetchManager('workList', params).then(res => {

      resolve(res);
    });
  })
}