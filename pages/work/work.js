var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    workList:[],
    studentList:[],
    classList:[],
    finishTimeInput:"",
    hasPush:true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },

   /**
   * 添加作业信息
   */
  insertWork:function(){
    wx.navigateTo({ url: 'workadd/workadd' })
  },
  
  selectedStudent:function(){
    wx.navigateTo({ url: 'workstudent/workstudent' })
  },

  finishTimeInputChange:function(e){
    this.data.finishTimeInput = e.detail.value;
    this.setData({
      finishTimeInput:e.detail.value,
    });
  },

  switchChanged: function (e) {
    this.data.hasPush = e.detail.value;
  },
  /**
   * 添加作业方法
   */
  actionWorkAddManager:function(){
    var that = this;

    // 1. 作业
    var params_workStr = "";
    for (var i = 0 ; i < this.data.workList.length;i++){
      var singleInfo = this.data.workList[i];
      if (i == this.data.workList.length - 1){
        params_workStr = params_workStr + singleInfo.name;
      } else {
        params_workStr = params_workStr + singleInfo.name + ",";
      }
    }

    // 2. 类型
    var params_type = "";
    if (this.data.studentList.length > 0){
      params_type = "2";
    } else if (this.data.classList.length > 0){
      params_type = "1";
    }

    // 3. 用户
    var params_userStr = "";
    var tempList = [];
    if (this.data.studentList.length > 0) {
      tempList = this.data.studentList;
    } else if (this.data.classList.length > 0) {
      tempList = this.data.classList;
    }

    for (var i = 0; i < tempList.length; i++) {
      var singleInfo = tempList[i];
      if (i == tempList.length - 1) {
        params_userStr = params_userStr + singleInfo.id;
      } else {
        params_userStr = params_userStr + singleInfo.id + ",";
      }
    }
    
    // 推送
    var params_hasPush = "";
    if (this.data.hasPush == true){
      params_hasPush = "1";
    } else {
      params_hasPush = "0";
    }
    var params = { workInfo: params_workStr, pushType: params_type, pushUser: params_userStr, hasPush: params_hasPush, end_time: this.data.finishTimeInput};
    appInstance.network.fetchManager('WorkAdd', params).then(res => {
      wx.navigateBack();
    });
  },

})