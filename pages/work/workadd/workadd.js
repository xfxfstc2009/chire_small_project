var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    workArr: appInstance.page_work.workList,
    inputText:"",
    workCount:"0/200"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  addWorkInfo: function () {
    // 1. 获取输入内容
    var singleInfo = { name: this.inputText, checked: true, }
    // 2. 获取历史内容

    var tempWorkArr = [];
    appInstance.page_work.workList.push(singleInfo);
    tempWorkArr = appInstance.page_work.workList;
    
    console.log(appInstance.page_work.workList);

    // 3. 更新当前的内容数组    
    this.workArr = tempWorkArr;
    // 4. 返回
    var pages = getCurrentPages();
    var prevPage = pages[pages.length - 2];
    var that = this;
    prevPage.setData({
      workList: that.workArr,
    })
    wx.navigateBack({
      delta: 1,
    });
  },

  // 输入内容信息
  textViewInputManager:function(e){
    var that = this;
    this.inputText= e.detail.value

    this.setData({
      workCount: this.inputText.length + "/200",
    })
  }
})