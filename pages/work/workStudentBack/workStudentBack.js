var appInstance = getApp();
var transferId = "";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    studentList:[],
    tempStudentId:"",   // 选中的学生
    tempBeizhu:"",      // 填写的备注
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    if (appInstance.util.isEmpty(options.id)!=true){
      transferId = options.id;
      this.sendRequestToGetWorkStudentList();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 
    console.log("===");
    console.log(this.data.tempBeizhu);
    if (appInstance.util.isEmpty(this.data.tempBeizhu) != true){
      this.workBackUpload();
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },

  // 获取本次作业的学生
  sendRequestToGetWorkStudentList:function(){
    var params = { workid: transferId};
    var that = this;
    appInstance.network.fetchManager('Work_Single_AllStudent', params).then(res => {
      console.log(res);
      that.setData({
        studentList:res.list,
      })
    });

  },
  workBackTapManager:function(e){
    var that = this;
    wx.showActionSheet({
      itemList: ["查看学生作业","添加评价"],
      success: function (res) {
        if (!res.cancel) {
          if (res.tapIndex == 0) {   // 查看学生作业
            that.workgetStudentWork(e);
          } else if (res.tapIndex == 1) {    //添加评价
            that.workAddStudentWorkBack(e);
          }
        }
      }
    });


  },

// 查看学生作业
  workgetStudentWork:function(e){

    var tempStudent = {};
    this.data.studentList.map(function (item) {
      if (item.student_id == e.currentTarget.id) {
        tempStudent = item;
      }
    });

console.log("+++++");
    console.log(tempStudent);
    appInstance.tempHtml = tempStudent.student_back;
    wx.navigateTo({
      url: 'workStudentDetail',
    })
  },

  // 添加学生作业
  workAddStudentWorkBack: function (e) {
    this.data.tempStudentId = e.currentTarget.id;

    var tempStudent = {};
    this.data.studentList.map(function (item) {
      if (item.student_id == e.currentTarget.id) {
        tempStudent = item;
      }
    });

    appInstance.tempHtml = tempStudent.beizhu;
    wx.navigateTo({
      url: '../../editor/editor?type=work',
    })
  },

  // 进行修改学生反馈
  workBackUpload:function(){
    var params = { workid: transferId, studentid: this.data.tempStudentId,"html":this.data.tempBeizhu };
    var that = this;
    appInstance.network.fetchManager('Work_Back', params).then(res => {
        var tempList = that.data.studentList;
      tempList.map(function (item) {
        if (item.student_id == that.data.tempStudentId){
          item.beizhu = that.data.tempBeizhu;
        }
  
      });

      that.setData({
        studentList: tempList,
      })
    });

  }
})