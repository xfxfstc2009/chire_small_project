var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    segmentSelectedIndex: 1, // 选中的segmentItem
    segmentList: [{
      id: "1",
      name: "选择班级"
    }, {
      id: "2",
      name: "选择学生"
    }],
    classList: [],
    studentList: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.getClassInfoManager();
    this.getCurrentStudentListManager();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  /**
   * segmentList点击
   */
  segmentListTap: function(e) {
    if (e.currentTarget.id == 1) {
      var studentTempList = this.data.studentList;
      studentTempList.map(function (item) {
        item.selected = false;
      });
      this.setData({
        studentList: studentTempList,
      })
    } else if (e.currentTarget.id == 2) {
      var classTempList = this.data.classList;
      classTempList.map(function (item) {
          item.selected = false;
      });
      this.setData({
        classList: classTempList,
      })
    }

    var that = this;
    this.setData({
      segmentSelectedIndex: e.currentTarget.id,
    })
  },
  /**
   * 学生Cell点击
   */
  studentCellTap: function(e) {
    var studentTempList = this.data.studentList;

    studentTempList.map(function(item) {
      if (e.currentTarget.id == item.id) {
        item.selected = !item.selected;
      }
    });

    this.setData({
      studentList: studentTempList,
    })
  },
  /**
   * 班级Cell点击
   */
  classCellTap: function(e) {
    var classTempList = this.data.classList;

    classTempList.map(function(item) {
      if (e.currentTarget.id == item.id) {
        item.selected = !item.selected;
      }
    });

    this.setData({
      classList: classTempList,
    })
  },

  /**
   * 获取当前班级信息
   */
  getClassInfoManager() {
    var that = this;
    appInstance.appFetchManager("Class_List", "").then(res => {
      var tempArr = res.list;
      tempArr.map(function(item) {
        var items = item;
        let imgInfo = {
          url: item.ablum,
          width: 20,
          height: 20
        };

        item.ablum = appInstance.appUntilManager("fixedImgUrl", imgInfo);
      });
      that.setData({
        classList: tempArr,
      })
    });
  },
  /**
   * 获取当前学生信息
   */
  getCurrentStudentListManager: function() {
    var params = {};
    var that = this;
    appInstance.appFetchManager("StudentList", "").then(res => {
      var tempArr = res.list;
      tempArr.map(function(item) {
        var items = item;
        let imgInfo = {
          url: item.avatar,
          width: 30,
          height: 30
        };

        item.avatar = appInstance.appUntilManager("fixedImgUrl", imgInfo);
      });
      that.setData({
        studentList: tempArr,
      })
    });
  },

  /**
   * 选中按钮
   */
  actionManager: function() {
    var pages = getCurrentPages();
    var prevPage = pages[pages.length - 2];
    var that = this;
    // 1. 获取选择的是班级还是学生
    if (this.data.segmentSelectedIndex == 1){
      var selectedClassTempList = [];
      this.data.classList.map(function (item) {
        if (item.selected == true){
          selectedClassTempList.push(item);
        }
      });
      prevPage.setData({
        classList: selectedClassTempList,
      })

    } else if (this.data.segmentSelectedIndex == 2){
      var selectedStudentTempList = [];
      this.data.studentList.map(function (item) {
        if (item.selected == true) {
          selectedStudentTempList.push(item);
        }
      });
      prevPage.setData({
        studentList: selectedStudentTempList,
      })
    }

    wx.navigateBack({
      delta: 1,
    });
  }
})