var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showDialog: false,
    isShowLoading: true,
    chooseUserTypeHasShow: false, // 表示不弹出进行选择用户类型
    chooseUserType: [{
        name: "学生",
        info: ["1.开课、签到、签退提醒。", "2.下载炽热教学资料。", "3.查看学习进度以及学习曲线。", "4.实时掌握自己课时情况。"]
      },
      {
        name: "家长",
        info: ["1.获取学员签到、签退提醒。", "2.查看学员学习进度以及学习曲线。", "3.实时掌握学员课时情况。"]
      },
      {
        name: "老师",
        info: ["请先联系管理员，获取老师邀请码。"]
      }
    ],

    // 1. 老师
    teacherItems: [{
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_student.png"),
      normalText: "学生",
      tip: '123123',
      url: '17'
    }, {
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_exam.png"),
      normalText: "考试资料",
      tip: '123123',
      url: '15'
    }, {
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_work.png"),
      normalText: "作业",
      tip: '123123',
      url: '18'
    }],

    // 【管理员】
    adminList: [{
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_subject.png"),
      normalText: "科目",
      tip: '123123',
      url: '11'
    }, {
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_news.png"),
      normalText: "消息",
      tip: '123123',
      url: '20'
    }, {
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_priceMenu.png"),
      normalText: "资费套餐",
      tip: '123123',
      url: '21'
    }, {
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_recharge.png"),
      normalText: "充值",
      tip: '123123',
      url: '22'
    }, {
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_wechat.png"),
      normalText: "微信通知",
      tip: '123123',
      url: '16'
    }, {
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_class.png"),
      normalText: "班级",
      tip: '123123',
      url: '12'
    }, {
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_activity.png"),
      normalText: "荣誉活动",
      tip: '123123',
      url: '13'
    }, {
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_event.png"),
      normalText: "事件",
      tip: '123123',
      url: '14'
    }, {
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_test.png"),
      normalText: "资质",
      tip: '123123',
      url: '23'
    }, {
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_test.png"),
      normalText: "banner",
      tip: '123123',
      url: '24'
    }, {
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_test.png"),
      normalText: "工具",
      tip: '123123',
      url: '26'
    }, {
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_student.png"),
      normalText: "学生",
      tip: '123123',
      url: '17'
    }, {
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_exam.png"),
      normalText: "考试资料",
      tip: '123123',
      url: '15'
    }, {
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_work.png"),
      normalText: "作业",
      tip: '123123',
      url: '18'
    }, {
      icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_subject.png"),
      normalText: "认证",
      tip: '123123',
      url: '27'
    }],

    // 用户信息
    userInfo: appInstance.globalData.userInfo,

    // 签到
    sign: {
      sign_icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_sign.png"),
    },
    // 签到信息
    signMainModel: {},

    // 今日课表
    kb: {
      kb_icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_class.png"),
    },
    ks: {
      ks_icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_money.png"),
      last_time: "2019-2-3",
      yue: "15.2302"
    },

    // 今日课
    todayClassList: [],

    // 作业
    work: {
      work_icon: appInstance.appUntilManager("fixedImgUrl", "smallprogream/center/center_mywork.png"),
    },
    // 作业
    workModel: {},
    workUploadHtml:"",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      isShowLoading: true,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.setData({
      isShowLoading: false,
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.sendRequestToLoginManager();
    if (appInstance.util.isEmpty(this.data.workUploadHtml)!= true){
      console.log("哈哈");
      this.uploadWorkManager();
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },
  /**
   * 联系管理员
   */
  linkAdmin: function() {
    wx.makePhoneCall({
      phoneNumber: '15669969617',
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.setData({
      isShowLoading: true,
    })
    this.sendRequestToLoginManager();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    console.log("1")
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  /**
   * 点击跳转
   */
  navigateTo: function(e) {
    console.log(e);
    // 学生
    if (e.currentTarget.id == '1') {
      wx.navigateTo({
        url: '../signin/signin',
      })
    } else if (e.currentTarget.id == '11') { // 科目
      wx.navigateTo({
        url: '../subject/subjectList',
      })
    } else if (e.currentTarget.id == '12') { // 班级
      var keyList = [];
      keyList.push(appInstance.dingyueAdminShenqing);
      authWithKeyManager(keyList).then(res => {
        if (res == true) {
          wx.navigateTo({
            url: '../class/classlist',
          })
        }
      });
    } else if (e.currentTarget.id == '13') { // 荣誉活动
      wx.navigateTo({
        url: '../activity/activitylist',
      })
    } else if (e.currentTarget.id == '14') { // 事件
      wx.navigateTo({
        url: '../event/eventlist',
      })
    } else if (e.currentTarget.id == '15') { // 考试资料
      wx.navigateTo({
        url: '../exam/examList/examlist',
      })
    } else if (e.currentTarget.id == '16') { // 微信通知
      wx.navigateTo({
        url: '../wechatMessage/wechatMessage'
      })
    } else if (e.currentTarget.id == '17') { // 学生
      wx.navigateTo({
        url: '../studentList/studentlist'
      })
    } else if (e.currentTarget.id == '18') { // 作业
      wx.navigateTo({
        url: '../work/worklist/worklist'
      })
    } else if (e.currentTarget.url == '1') { // 签到
      wx.navigateTo({
        url: '../signin/signin'
      })
    } else if (e.currentTarget.id == '19') {
      this.kbTapManager();
    } else if (e.currentTarget.id == '20') {
      wx.navigateTo({
        url: '../news/newslist/newslistedit'
      })
    } else if (e.currentTarget.id == '21') {
      wx.navigateTo({
        url: '../priceMenu/priceMenu',
      })
    } else if (e.currentTarget.id == '22') {
      wx.navigateTo({
        url: '../recharge/recharge_add',
      })
    } else if (e.currentTarget.id == '23') {
      wx.navigateTo({
        url: '../qualifications/qualifications_edit/qualifications_edit',
      })
    } else if (e.currentTarget.id == '24') {
      wx.navigateTo({
        url: '../banner/banner_list',
      })
    } else if (e.currentTarget.id == '25') {
      wx.navigateTo({
        url: '../exam/examList/examlist',
      })
    } else if (e.currentTarget.id == '26') {
      wx.navigateTo({
        url: '../tool/tool_list',
      })
    } else if (e.currentTarget.id == '27') {
      var temp = [];
      temp.push(appInstance.dingyueAdminShenqing); // 1. 添加认证权限申请

      wx.requestSubscribeMessage({
        tmplIds: temp,
        success(res) {
          wx.showToast({
            title: '授权成功。',
          })
          wx.navigateTo({
            url: '../auth/authList',
          })
        },
        fail(res) {
          wx.showToast({
            title: '请授权后进行使用。否则影响消息推送。',
          })
        }
      })

    }
  },

  /**
   * 头像点击
   */
  headerTapManager: function(e) {
    
    if (appInstance.globalData.userInfo.hasLogin == false) {
      authMainManager(e.currentTarget.id).then(res => {
        console.log(res);
        if (res == true) {
          wx.navigateTo({
            url: '../login/login'
          })
        }
      });
    } else {
      if (appInstance.globalData.userInfo.user_type == 0) { //    普通人
        // 进行弹出用户类型申请
        this.setData({
          istrue: true
        })
      } else if (appInstance.globalData.userInfo.user_type == 1) {
        wx.navigateTo({
          url: '../teacherList/teacherInfo/teacherInfo'
        })
      } else if (appInstance.globalData.userInfo.user_type == 2) {
        wx.navigateTo({
          url: '../studentInfo/studentInfo'
        })
      } else if (appInstance.globalData.userInfo.user_type == 3) {
        console.log("家长");
      } else if (appInstance.globalData.userInfo.user_type == 4) {
        console.log("管理员");
      } else {
        this.setData({
          istrue: true
        })
      }
    }
  },





  /**
   * 获取当前签到状态
   */
  getCurrentSignStatus: function() {
    var that = this;
    console.log("=====");
    var params = {
      openid: appInstance.globalData.userInfo.studentInfo.openid
    };
    appInstance.network.fetchManager('Sign_Status', params).then(res => {
      console.log("=====");
      console.log(res);
      that.setData({
        signMainModel: res,
      })
    })
  },
  /**
   * 获取今天的课程
   */
  getTodayClassListManager: function() {
    var that = this;
    // 1.获取今天礼拜几
    var currentToday = this.getTodayWeekDay();
    var params = {
      openid: appInstance.globalData.userInfo.studentInfo.openid
    }
    appInstance.network.fetchManager('Sign_Today_Class', params).then(res => {
      var classList = res.list;
      var tempClassList = [];

      classList.map(function(item) {
        console.log(item.classtimelist.length);;
        for (var i = 0; i < item.classtimelist.length; i++) {
          var singleInfo = item.classtimelist[i];
          if (singleInfo.week == currentToday) {
            item.sandetime = singleInfo.begintime + "-" + singleInfo.endtime;
            break;
          }
        }
        tempClassList.push(item);
      });

      console.log(tempClassList);
      that.setData({
        todayClassList: tempClassList,
      })
    })
  },
  /**
   * 获取我的作业信息
   */
  sendRequestToGetWorkList: function() {
    var that = this;
    var params = {
      openid: appInstance.globalData.userInfo.studentInfo.openid
    }
    appInstance.network.fetchManager('workStudent', params).then(res => {
      var workTempList = res.list;
      console.log("作业");
      console.log(workTempList);
      if (workTempList.length > 0) {
        that.setData({
          workModel: workTempList[0],
        })
      } else {
        var tempModel = {};
        tempModel.workList = [];
        tempModel.workName = "我的作业";
        that.setData({
          workModel: tempModel,
        })
      }
    });
  },
  /**
   * 获取我的资产
   */
  sendRequestToGetMyBalance: function() {
    var params = {
      openid: appInstance.globalData.userInfo.studentInfo.openid
    }
    var that = this;
    appInstance.network.fetchManager('UserBalance', params).then(res => {
      var tempUserInfo = this.data.userInfo;
      tempUserInfo.money = res.balance;
      appInstance.globalData.userInfo = tempUserInfo;
      that.setData({
        userInfo: tempUserInfo,
      })
    });
  },

  /*** 绑定用户状态*/
  logoutManager: function() {
    console.log(this.data.userInfo);
    console.log("退出登录");

  },
  jianyi: function() {
    wx.navigateTo({
      url: '../proposal/proposal',
    })
  },

  /*** 绑定用户状态*/
  // 打开绑定用户弹窗
  showDialog: function() {
    this.setData({
      chooseUserTypeHasShow: true,
    })
  },
  // 关闭绑定用户弹窗
  closeDialog: function() {
    this.setData({
      chooseUserTypeHasShow: false,
    })
  },
  // 弹窗进行选择用户类型
  userTypeChooseManager: function(e) {
    this.closeDialog();
    authMainManager(e.currentTarget.id).then(res => {
      console.log(res);
      if (res == true) {
        if (e.currentTarget.id == '学生') {
          wx.navigateTo({
            url: '../studentInfo/studentInfo'
          })
        } else if (e.currentTarget.id == '老师') {
          wx.navigateTo({
            url: '../teacherList/teacherInfo/teacherInfo'
          })
        } else if (e.currentTarget.id == '家长') {
          wx.navigateTo({
            url: '../parent/binding/parentbinding'
          })
        }
      } else {
        console.log("授权失败");
      }
    });
  },

  myworkManager: function() {
    var mainUrl = "https://www.chire.net/chireh5/Work/work_list.html";
    var params = "openid#" + appInstance.globalData.userInfo.open_id;
    wx.navigateTo({
      url: '../../utils/webview/webview?url=' + mainUrl + "&params=" + params,
    });
  },
  // 课表进行点击
  kbTapManager: function(e) {
    var mainUrl = "https://www.chire.net/chireh5/Memorandum/Memorandum.html";
    var params = "openid#" + appInstance.globalData.userInfo.open_id;
    wx.navigateTo({
      url: '../../utils/webview/webview?url=' + mainUrl + "&params=" + params
    });
  },
  // 签到点击
  signManager: function(e) {
    // 如果是家长添加请假通知
    // 如果是学生 添加未签到通知
    var keyList = [];
    if (appInstance.globalData.userInfo.user_type == "2"){      // 学生 
      keyList.push(appInstance.dingyueNoSign);          // 未签到
    } else if (appInstance.globalData.userInfo.user_type == "3") {      // 家长
      keyList.push(appInstance.dingyueAllQingjia);      // 请假
    }
    authWithKeyManager(keyList).then(res=>{
      if (res == true){
        wx.navigateTo({
          url: '../signin/signin',
        })
      }
    });
  },

  // 我的课时
  myClassTimeManager:function(){
    var keyList = [];
    keyList.push(appInstance.dingyueAllKeshibuzu);      // 课时不足
    keyList.push(appInstance.dingyueStudentKechengGoumai);    // 课时购买
    authWithKeyManager(keyList).then(res => {
      if (res == true) {
        wx.navigateTo({
          url: '../order/order',
        })
      }
    });
  },
  //学生提交作业
  uploadWorkManager:function(){
    var that = this;
    var params = { workid: this.data.workModel.linkId, studentid: appInstance.globalData.userInfo.open_id, html: this.data.workUploadHtml};

    appInstance.network.fetchManager('Work_Student_Back', params).then(res => {
      var temp = that.data.workModel;
      temp.workback.student_back = that.data.workUploadHtml;
      that.setData({
        workModel: temp,
      })
      that.data.workUploadHtml = "";
    });
  },
  gotoWorkEdit:function(){
    appInstance.tempHtml = this.data.workModel.workback.student_back;
    console.log(this.data.workModel);
    wx.navigateTo({
      url: '../editor/editor?type=studentWork',
    })
  },
  
  // 查看老师评论
  gotoWorkBack:function(){
    appInstance.tempHtml = this.data.workModel.workback.beizhu;

    wx.navigateTo({
      url: '../work/workStudentBack/workStudentDetail',
    })
  },
  // 查看学生作业
  gotoWorkInfo:function(){
    appInstance.tempHtml = this.data.workModel.workback.student_back;

    wx.navigateTo({
      url: '../work/workStudentBack/workStudentDetail',
    })
  },
  // 获取今天礼拜几
  getTodayWeekDay: function() {
    var today = new Date().getDay();
    var week = ['0', '1', '2', '3', '4', '5', '6'];
    return week[today];
  },

  /**
   * 进行登录
   */
  sendRequestToLoginManager: function() {

    var that = this;
    lazyLoginMangaer().then(res => {
      if (res != null) {
        appInstance.globalData.userInfo = res;
        appInstance.globalData.userInfo.hasLogin = true;
        that.setData({
          userInfo: appInstance.globalData.userInfo,
        })
        // 表示当前已登录=>进行修改当前状态
        that.userStatusManager();
      }
    });
  },

  /**
   * 进行用户状态修改
   */
  userStatusManager: function() {
    if (appInstance.util.isEmpty(appInstance.globalData.userInfo.open_id) != true) {
      // 表示已经登录
      if (appInstance.globalData.userInfo.user_type == 0) {
        console.log("普通人");
      } else if (appInstance.globalData.userInfo.user_type == 1) { // 老师
        // 未认证状态
        if (appInstance.globalData.userInfo.has_renzhen == null || appInstance.globalData.userInfo.has_renzhen == "0" || appInstance.globalData.userInfo.has_renzhen == "" || appInstance.globalData.userInfo.has_renzhen == "0" || appInstance.globalData.userInfo.has_renzhen == false || appInstance.globalData.userInfo.has_renzhen == 0) {
          // 【表示当前未认证】
          var tempUserInfo = this.data.userInfo;
          tempUserInfo.user_type = -2;
          console.log("当前未认证");
          this.setData({
            userInfo: tempUserInfo
          })
        } else { // 【表示当前已认证】

          var authTeacher = wx.getStorageSync('authTeacher');
          console.log(authTeacher);
          if (appInstance.util.isEmpty(authTeacher) != true) { // 表示有值
            if (authTeacher == "1") {
              var tempUserInfo = this.data.userInfo;
              tempUserInfo.user_type = 1;
              this.setData({
                userInfo: tempUserInfo
              })
            }
          } else {
            // 将当前家长设置为没授权过
            var tempUserInfo = this.data.userInfo;
            tempUserInfo.user_type = -21;
            console.log("当前未认证");
            this.setData({
              userInfo: tempUserInfo
            })
          }
        }
      } else if (appInstance.globalData.userInfo.user_type == 2) { // 【学生】
        // 未认证状态
        console.log(appInstance.globalData.userInfo);
        if (appInstance.globalData.userInfo.studentInfo.authStatus == null || appInstance.globalData.userInfo.studentInfo.authStatus == "0" || appInstance.globalData.userInfo.studentInfo.authStatus == "" || appInstance.globalData.userInfo.has_renzhen == "0" || appInstance.globalData.userInfo.has_renzhen == false || appInstance.globalData.userInfo.has_renzhen == 0) {
          // 【表示当前未认证】
          var tempUserInfo = this.data.userInfo;
          tempUserInfo.user_type = -2;
          console.log("当前未认证");
          this.setData({
            userInfo: tempUserInfo
          })
        } else { // 【表示当前已认证】
          // 1. 判断本地家长是否授权过
          var authStudent = wx.getStorageSync('authStudent');
          console.log("====");
          console.log(authParent);
          if (appInstance.util.isEmpty(authStudent) != true) {
            // 获取当前的签到状态
            this.getCurrentSignStatus();
            // 2.获取今天的课程
            this.getTodayClassListManager();
            // 3. 获取我的作业
            this.sendRequestToGetWorkList();
            // 4. 获取我的资产
            this.sendRequestToGetMyBalance();
          } else {
            // 将当前家长设置为没授权过
            var tempUserInfo = this.data.userInfo;
            tempUserInfo.user_type = -22;
            console.log("当前未认证");
            this.setData({
              userInfo: tempUserInfo
            })
          }
        }
      } else if (appInstance.globalData.userInfo.user_type == 3) { // 家长
        // 未认证状态
        if (appInstance.globalData.userInfo.studentInfo.authStatus == null || appInstance.globalData.userInfo.studentInfo.authStatus == "0" || appInstance.globalData.userInfo.studentInfo.authStatus == "" || appInstance.globalData.userInfo.has_renzhen == "0" || appInstance.globalData.userInfo.has_renzhen == false || appInstance.globalData.userInfo.has_renzhen == 0) {
          // 【表示当前未认证】
          var tempUserInfo = this.data.userInfo;
          tempUserInfo.user_type = 0;
          console.log("当前未认证");
          this.setData({
            userInfo: tempUserInfo
          })

        } else { // 【表示当前已认证】
          // 1. 判断本地家长是否授权过
          var authParent = wx.getStorageSync('authParent');
          console.log("====");
          console.log(authParent);
          if (appInstance.util.isEmpty(authParent) != true) {
            // 获取当前的签到状态
            this.getCurrentSignStatus();
            // 2.获取今天的课程
            this.getTodayClassListManager();
            // 3. 获取我的作业
            this.sendRequestToGetWorkList();
            // 4. 获取我的资产
            this.sendRequestToGetMyBalance();
          } else {
            // 将当前家长设置为没授权过
            var tempUserInfo = this.data.userInfo;
            tempUserInfo.user_type = -23;
            console.log("当前未认证");
            this.setData({
              userInfo: tempUserInfo
            })
          }
        }
      } else if (appInstance.globalData.userInfo.user_type == 4) {
        // 进行认证申请权限申请
      }
    } else {
      // 表示没有登录
      console.log("没有登录")

    }
  },

  // 授权按钮
  authRootManager: function(res) {
    console.log("未认证");
    authMainManager().then(res => {
      if (res == true) {
        if (appInstance.globalData.userInfo.user_type == 3 || appInstance.globalData.userInfo.user_type == -23) { // 家长
          appInstance.globalData.userInfo.user_type = 3;
          wx.setStorageSync('authParent', "1");
          this.userStatusManager();
        } else if (appInstance.globalData.userInfo.user_type == 2 || appInstance.globalData.userInfo.user_type == -22) { //学生
          appInstance.globalData.userInfo.user_type = 2;
          wx.setStorageSync('authStudent', "1");
          this.userStatusManager();
        } else if (appInstance.globalData.userInfo.user_type == 1 || appInstance.globalData.userInfo.user_type == -21) { //老师
          appInstance.globalData.userInfo.user_type = 1;
          wx.setStorageSync('authTeacher', "1");
          this.userStatusManager();
        }
      }
    });
  }
})

///懒登录方法
function lazyLoginMangaer() {
  return new Promise(function(resolve, reject) {

    if (appInstance.globalData.userInfo.hasLogin == true) { // 【已登录】  
      resolve(appInstance.globalData.userInfo);
    } else { // 【未登录】
      var openId = wx.getStorageSync('openId');
      if (appInstance.util.isEmpty(openId) != true) { // 如果有openid
        var params = {
          open_id: openId
        };
        appInstance.network.fetchManager('UserLogin', params).then(res => {
          console.log("请求成功");
          resolve(res);
        });
      } else { // 如果没有openid

        console.log("没有openid");
        resolve(null);
      }
    }
  });
}

// 进行授权
function authMainManager(type) {
  return new Promise(function(resolve, reject) {
    console.log(appInstance.globalData.userInfo);
    if (appInstance.globalData.userInfo.hasLogin == false) {
      console.log("普通授权");
      authWithNormal().then(res => {
        console.log("请求成功");
        resolve(res);
      });
    } else {
      console.log(appInstance.globalData.userInfo);
      if (appInstance.globalData.userInfo.user_type == 1 || type == '老师' || type == 1 || appInstance.globalData.userInfo.user_type == -21) { // 老师
        authWithTeacher().then(res => {
          console.log("请求成功");
          resolve(res);
        });
      } else if (appInstance.globalData.userInfo.user_type == 2 || type == '学生' || type == 2 || appInstance.globalData.userInfo.user_type == -22) { // 学生
        authWithStudent().then(res => {
          console.log("请求成功");
          resolve(res);
        });
      } else if (appInstance.globalData.userInfo.user_type == 3 || type == '家长' || type == 3 || appInstance.globalData.userInfo.user_type == -23) { // 家长
        authWithParent().then(res => {
          console.log("请求成功");
          resolve(res);
        });
      }
    }
  });
}

//  学生授权
function authWithStudent() {
  return new Promise(function(resolve, reject) {
    var temp = [];
    temp.push(appInstance.dingyueStudentWork); // 1. 添加作业通知
    temp.push(appInstance.dingyueStudentMorningMsg); // 2. 添加学习提醒
    temp.push(appInstance.dingyueStudentParentJoin); // 3. 添加家长绑定

    wx.requestSubscribeMessage({
      tmplIds: temp,
      success(res) {
        var info = temp[0];
        var sub = res[info];
        if (sub == "reject") { // 表示拒绝
          wx.showToast({
            title: '请授权后进行使用。否则影响消息推送。',
          })
        } else { // 表示授权成功
          resolve(true);
        }
      },
      fail(res) {
        resolve(false);
        wx.showToast({
          title: '请授权后进行使用。否则影响消息推送。',
        })
      }
    })
  });
}

// 签到授权
function authWithKeyManager(pushKeys) {
  return new Promise(function (resolve, reject) {
    var infoKey = pushKeys[0];
    var hasData = wx.getStorageSync(infoKey);
    console.log("===");
    console.log(hasData);
    if (hasData == null || hasData == ""){
      wx.requestSubscribeMessage({
        tmplIds: pushKeys,
        success(res) {
          var info = pushKeys[0];
          var sub = res[info];
          if (sub == "reject") { // 表示拒绝
            wx.showToast({
              title: '请授权后进行使用。否则影响消息推送。',
            })
          } else { // 表示授权成功
            wx.setStorageSync(infoKey, "1");
            resolve(true);
          }
        },
        fail(res) {
          resolve(false);
          wx.showToast({
            title: '请授权后进行使用。否则影响消息推送。',
          })
        }
      })
    } else {
      resolve(true);
    }
  });
}


//  家长授权授权
function authWithParent() {
  return new Promise(function(resolve, reject) {
    var temp = [];
    temp.push(appInstance.dingyueStudentWork); // 2. 添加作业提醒
    temp.push(appInstance.dingyuePrentSign); // 3. 添加学生签到
    temp.push(appInstance.dingyueAllQingjia); // 3. 添加请假

    wx.requestSubscribeMessage({
      tmplIds: temp,
      success(res) {
        console.log(res);
        var info = temp[0];
        var sub = res[info];
        if (sub == "reject") { // 表示拒绝
          wx.showToast({
            title: '请授权后进行使用。否则影响消息推送。',
          })
        } else { // 表示授权成功
          resolve(true);
        }
      },
      fail(res) {
        resolve(false);
        wx.showToast({
          title: '请授权后进行使用。否则影响消息推送。',
        })
      },
      complete(res) {
        console.log(res);
        console.log("====");
      }
    })
  });
}

//  老师授权授权
function authWithTeacher() {
  return new Promise(function(resolve, reject) {
    var temp = [];
    temp.push(appInstance.dingyueAllQingjia); // 1. 学生请假信息
    temp.push(appInstance.dingyueStudentMorningMsg); // 2. 今天有课

    wx.requestSubscribeMessage({
      tmplIds: temp,
      success(res) {
        var info = temp[0];
        var sub = res[info];
        if (sub == "reject") { // 表示拒绝
          wx.showToast({
            title: '请授权后进行使用。否则影响消息推送。',
          })
        } else { // 表示授权成功
          resolve(true);
        }
      },
      fail(res) {
        resolve(false);
        wx.showToast({
          title: '请授权后进行使用。否则影响消息推送。',
        })
      }
    })
  });
}

//  普通人授权授权
function authWithNormal() {
  return new Promise(function(resolve, reject) {
    var temp = [];
    temp.push(appInstance.dingyueAllMsg); // 4. 添加推送消息

    wx.requestSubscribeMessage({
      tmplIds: temp,
      success(res) {
        var info = temp[0];
        var sub = res[info];
        if (sub == "reject") { // 表示拒绝
          wx.showToast({
            title: '请授权后进行使用。否则影响消息推送。',
          })
        } else { // 表示授权成功
          resolve(true);
        }
      },
      fail(res) {
        resolve(false);
        wx.showToast({
          title: '请授权后进行使用。否则影响消息推送。',
        })
      }
    })
  });
}