var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    card_bg: appInstance.appUntilManager("fixedImgUrl", "smallprogream/order/card-bg.png"),
    balance: "",
    card_lasttime: appInstance.util.formatTime(new Date()),
    chongzhiCount: 0, // 充值次数
    rechargeList: [], // 充值列表
    xiaofeiCount: 0, // 充值次数
    xiaofeiList: [], // 消费列表
    orderType: 0, // 0表示消费记录，1表示充值记录 
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(appInstance.globalData.userInfo);
    this.sendRequestToGetChongzhijilu();
    this.sendRequestToGetOrderList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.setData({
      balance: appInstance.globalData.userInfo.money,
      student_card: appInstance.globalData.userInfo.im_id,
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  orderTypeTapManager: function(e) {
    this.setData({
      orderType: e.currentTarget.id,
    })
  },

  //获取充值记录
  sendRequestToGetChongzhijilu() {
    var that = this;
    var params = {
      page: 1,
      size: 20,
      studentid: appInstance.globalData.userInfo.id
    };
    appInstance.network.fetchManager('RechargeList', params).then(res => {
      console.log(res);
      that.setData({
        chongzhiCount: res.count,
        rechargeList: res.list,
      })
    })
  },
  // 消费记录
  sendRequestToGetOrderList: function() {
    var that = this;
    var params = {
      page: 1,
      size: 20
    };
    appInstance.network.fetchManager('Order_Student', params).then(res => {
      that.setData({
        xiaofeiCount: res.count,
        xiaofeiList: res.list,
      })
    })
  }
})