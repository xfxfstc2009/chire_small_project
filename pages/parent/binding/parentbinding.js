var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    studentList:[],
    studentPickerList:[],
    selectedIndex:0,
    istrue:false,
    bindingCode:"",
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      isShow: false,
    })
    this.sendRequestToGetStudentList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  /**
   * 接口获取学生信息
   */
  sendRequestToGetStudentList:function(){
    var that = this;
    appInstance.appFetchManager("StudentList", "").then(res => {
      var tempArr = res.list;
      var tempPickerList = [];
      tempArr.map(function (item) {
        tempPickerList.push(item.name);
      });
      that.setData({
        studentList: tempArr,
        studentPickerList: tempPickerList,
      })
    });
  },

  /**
   * 进行绑定
   */
  sendRequestToBindingInfoManager:function(){
    var that = this;
    var singleItem = this.data.studentList[this.data.selectedIndex];
    var params = { auth_type: "1", openid: appInstance.globalData.userInfo.open_id, auth_key: singleItem.id};
    console.log(params);

    appInstance.appFetchManager("Identity_Auth_Notice", params).then(res=>{
      if (appInstance.util.isEmpty(res) != true){       // 表示成功
        wx.showModal({
          title: '提示',
          content: res.errMsg,
          showCancel: false,
          success(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            }
          }
        })
      } else {
        that.setData({
          isShow: true,
        })
      }
    })
  },
 /**
   * 进行绑定
   */
  sendRequestToActionBindingManager: function () {
    var that = this;
    var singleItem = this.data.studentList[this.data.selectedIndex];
    var params = { auth_type: "1", openid: appInstance.globalData.userInfo.open_id, authCode: this.data.bindingCode, studentId:singleItem.id};
    console.log(params);
    appInstance.appFetchManager("Identity_Auth", params).then(res => {
      // 绑定成功
      console.log("====");
      console.log(res);
      if (appInstance.util.isEmpty(res.errCode) != true){
        if (res.errCode != "200"){
          wx.showModal({
            title: '提示',
            content: res.errMsg,
            showCancel: false,
            success(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              }
            }
          })
        }
      } else {
        wx.navigateTo({
          url: '../bindingend/bindingsuccess?name=' + singleItem.name,
        })
      }
    })
  },

  bindingRootManager:function(e){
    this.sendRequestToActionBindingManager();
  },

  studentsChange:function(e){
    this.data.selectedIndex = e.detail.value;
    this.setData({
      selectedIndex:e.detail.value,
    })
  },
  /**
   * 获取到的输入内容
   */
  smsInputManager:function(e){
    this.data.bindingCode = e.detail.value;
  },
/**
   * 取消绑定
   */
  cancelBinding:function(){
    this.setData({
      isShow:false,
    })
  },

  bindingActionManager:function(e){
    // 1. 进行绑定
    this.sendRequestToBindingInfoManager();

  },
  closeDialog:function(){
    this.setData({
      isShow: false
    })
  },
  stopEvent:function(){
    this.setData({
      istrue: false
    })
  }
  
})