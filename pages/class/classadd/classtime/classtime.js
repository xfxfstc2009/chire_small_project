var appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    weekList: [{ name: "星期一", checked:true},
      { name: "星期二", checked: false },
      { name: "星期三", checked: false },
      { name: "星期四", checked: false },
      { name: "星期五", checked: false },
      { name: "星期六", checked: false },
      { name: "星期日", checked: false }],
      startTime:"9:00",
      endTime: "9:00",
      checkedWeek:"星期一",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  weekChange:function(e){

    var checkboxItems = this.data.weekList, values = e.detail.value;
    for (var i = 0, lenI = checkboxItems.length; i < lenI; ++i) {
      checkboxItems[i].checked = false;
      if (checkboxItems[i].name == values){
        checkboxItems[i].checked = true;
        this.data.checkedWeek = checkboxItems[i].name;
      }
    }

    this.setData({
      weekList: checkboxItems
    });
  },
  // 开始时间
  startTimeChange:function(e){
    this.setData({
      startTime:e.detail.value,
    })
  },
  // 结束时间
  endTimeChange:function(e){
    this.setData({
      endTime: e.detail.value,
    })
  },
  addClassTime:function(e){
    var classTimeModel = {};
    classTimeModel.week = this.data.checkedWeek;
    classTimeModel.starttime = this.data.startTime;
    classTimeModel.endtime = this.data.endTime;
    classTimeModel.name = this.data.checkedWeek + " " + this.data.startTime + "-" + this.data.endTime;

    appInstance.page_class.classTimeList.push(classTimeModel);
    wx.navigateBack();
  }

})