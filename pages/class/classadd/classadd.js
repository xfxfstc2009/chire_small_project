var appInstance = getApp();
var transferId = "";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    className: "", // 班级名称
    classTime: "", // 课时数量
    classStartTime: "", // 开课时间
    kemuPickerList: [], // 科目picker
    kemuList: [], // 科目列表
    kemuIndex: 0, // 科目picker Index
    teacherList: [], // 老师列表
    teacherPickerList: [], // 老师picker
    teacherPickerIndex: 0, // 老师picker index
    imageList: [], // 上传的图片
    weekList: [], // 课表
    beizhu: "", // 备注内容
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // 1. 获取当前的科目
    this.getCurrentKemu();
    // 2. 获取当前的老师
    this.getCurrentTeacherList();
    // 3. 如果是修改，就传递参数

    if (appInstance.util.isEmpty(options.id) != true) {
      transferId = options.id;
      this.setData({
        transferId: options.id,
      })
      // 1. 获取当前的班级信息
      this.getCurrentClass();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var sd = [];
    if (appInstance.page_class.classTimeList.length > 0) {
      var tempWeek = [];
      tempWeek = this.data.weekList;
      tempWeek.push(appInstance.page_class.classTimeList[0]);
      // 清空
      appInstance.page_class.classTimeList = [];

      this.setData({
        weekList: tempWeek,
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  /**
   * ============== 输入内容信息===============
   */
  // 班级名称
  nameInput: function(e) {
    this.data.className = e.detail.value;
  },
  // 班级课时
  classtimeInput: function(e) {
    this.data.classTime = e.detail.value;
  },
  // 备注
  beizhuInputManager: function(e) {
    this.data.beizhu = e.detail.value;
  },

  /**
   * ============== 接口信息===============
   */
  // 获取当前的班级信息
  getCurrentClass: function() {
    var that = this;
    var params = {
      id: transferId
    };

    appInstance.network.fetchManager('Class_Detail', params).then(res => {
      var imgLists = [];
      if (appInstance.util.isEmpty(res.uploads) != true) {
        imgLists = res.uploads.split(',');
      }

      // 1.根据传入的内容进行数据整理
      var tempClassTime = [];
      if (res.classtimelist.length > 0) {
        for (var i = 0; i < res.classtimelist.length; i++) {
          var singleClassTime = res.classtimelist[i]
          var singleInfo = singleClassTime.week + " " + singleClassTime.begintime + "-" + singleClassTime.endtime;
          var singleModel = {
            week: singleClassTime.week,
            starttime: singleClassTime.starttime,
            endtime: singleClassTime.endtime,
            name: singleInfo
          };
          tempClassTime.push(singleModel);
        }
      }

      // 科目筛选
      var tempSubjectIndex = 0;

      for (var i = 0; i < that.data.kemuPickerList.length; i++) {
        var singleItems = that.data.kemuPickerList[i];
        if (singleItems == res.subject.name) {
          tempSubjectIndex = i;
        }
      }

      // 老师选择
      var tempTeacherIndex = 0;
      for (var i = 0; i < that.data.teacherPickerList.length; i++) {
        var singleItems = that.data.teacherPickerList[i];
        if (singleItems == res.teacher.name) {
          tempTeacherIndex = i;
        }
      }

      that.setData({
        className: res.name, // 班级名称
        classTime: res.classtime, // 班级课时
        classStartTime: res.datetime, // 开课时间
        beizhu: res.marks, // 备注
        imageList: imgLists, // 图片
        weekList: tempClassTime, //课程周期列表
        kemuIndex: tempSubjectIndex, // 科目index
        teacherPickerIndex: tempTeacherIndex,
      })
    });
  },

  // 获取当前的科目
  getCurrentKemu: function() {
    var that = this;
    appInstance.network.fetchManager('SubjectList', '').then(res => {
      that.data.kemuList = res.list;
      var tempList = res.list;

      var tempClassPicker = [];
      tempClassPicker.push("请选择科目");
      tempList.map(function(item) {
        tempClassPicker.push(item.name);
      });

      that.setData({
        kemuPickerList: tempClassPicker,
      });
    });
  },
  // 科目picker 选择
  kemuChange: function(e) {
    this.setData({
      kemuIndex: e.detail.value,
    })
  },
  // 获取当前的授课老师
  getCurrentTeacherList: function() {
    var that = this;
    appInstance.network.fetchManager('Teacher_list', '').then(res => {
      that.data.teacherList = res.list;
      var tempList = res.list;

      var tempTeacherPickerList = [];
      tempTeacherPickerList.push("请选择授课老师");
      tempList.map(function(item) {
        tempTeacherPickerList.push(item.name);
      });

      that.setData({
        teacherPickerList: tempTeacherPickerList,
      });
    });
  },
  // 老师 picker 选择
  teacherPickerChange: function(e) {
    this.setData({
      teacherPickerIndex: e.detail.value,
    })
  },

  // 开课时间picker
  startTimePickerChange: function(e) {
    this.setData({
      classStartTime: e.detail.value,
    })
    this.data.classStartTime = e.detail.value;
  },

  // 图片选择器
  chooseFiles: function(e) {
    var that = this;
    // 1. 选择图片
    appInstance.util.chooseImgAblumManager(9).then(res => {
      // 进行上传
      appInstance.util.uploadImgManager(res).then(res => { // 2. 上传图片
        that.imageList = res;
        that.setData({
          imageList: res,
        })
      });
    });
  },

  // 图片点击
  imgSelectedManager: function(e) {
    var selectedImgUrl = e.currentTarget.id;
    console.log(selectedImgUrl);
    var that = this;
    wx.showActionSheet({
      itemList: ['查看图片', '删除图片'],
      success: function(res) {
        if (!res.cancel) {
          if (res.tapIndex == 0) { // 图片放大
            that.imgShowsManager(selectedImgUrl);
          } else if (res.tapIndex == 1) { // 删除图片
            if (appInstance.util.isEmpty(transferId) == true) {
              that.imgDeleteManager(selectedImgUrl);
            } else {
              that.imgDeleteWithUploadManager(selectedImgUrl);
            }
          }
        }
      }
    })
  },
  // 图片放大
  imgShowsManager: function(selectedImg) {
    var that = this;
    console.log(this.data.imageList);
    wx.previewImage({
      current: selectedImg,
      urls: that.data.imageList,
    })
  },
  // 删除图片
  imgDeleteManager: function(selectedImg) {
    var that = this;
    var tempImgList = [];
    tempImgList = this.data.imageList;

    var selectedIndex = tempImgList.indexOf(selectedImg);
    tempImgList.splice(selectedIndex, 1);

    this.imageList = tempImgList;
    this.setData({
      imageList: tempImgList,
    })
  },

  imgDeleteWithUploadManager: function(selectedImg) {
    var that = this;
    var tempImgList = [];
    tempImgList = this.data.imageList;
    var selectedIndex = tempImgList.indexOf(selectedImg);
    tempImgList.splice(selectedIndex, 1);

    var tempImgUrls = "";
    for (var i = 0; i < tempImgList.length; i++) {
      var singleImg = tempImgList[i];
      if (i == tempImgList.length - 1) {
        tempImgUrls = tempImgUrls + singleImg;
      } else {
        tempImgUrls = tempImgUrls + singleImg + ",";
      }
    }

    var params = {
      "id": transferId,
      "imgs": tempImgUrls
    };
    appInstance.network.fetchManager('Class_Img_Del', params).then(res => {

      that.imageList = tempImgList;
      that.setData({
        imageList: tempImgList,
      })
    })

  },


  // 添加
  addClassTime: function(e) {
    console.log(e);
    wx.navigateTo({
      url: 'classtime/classtime',
    })
  },
  // 修改
  updateManager:function(e){

  },

  // 添加课程周期
  sendRequestToAddClass: function(e) {
 
    var params = {};
    params.name = this.data.className;
    params.teacher = this.data.teacherList[this.data.teacherPickerIndex - 1].id;
    params.betime = this.data.classStartTime;
    params.classtime = this.data.classTime;
    params.subject = this.data.kemuList[this.data.kemuIndex - 1].id;
    var uploaderImgs = "";
    for (var i = 0; i < this.data.imageList.length; i++) {
      var url = this.data.imageList[i];
      if (i == this.data.imageList.length - 1) {
        uploaderImgs = uploaderImgs + url;
      } else {
        uploaderImgs = uploaderImgs + url + ",";
      }
    }
    params.upload = uploaderImgs;
    params.marks = this.data.beizhu;

    var weekStr = "";
    for (var i = 0; i < this.data.weekList.length; i++) {
      var singleWeek = this.data.weekList[i];
      if (i == this.data.weekList.length - 1) {
        weekStr = weekStr + singleWeek.name;
      } else {
        weekStr = weekStr + singleWeek.name + ",";
      }
    }

    params.weekstime = weekStr;
    params.ablum = this.data.imageList[0];
    var that = this;
    console.log(params);

    appInstance.network.fetchManager('Class_Add', params).then(res => {
      wx.navigateBack({});
    });
  },

  // 添加课程按钮点击
  addManager: function(e) {

    this.sendRequestToAddClass();
  },

  // 课程删除
  deleteManager: function() {
    var that = this;
    var params = {
      id: transferId
    };
    appInstance.network.fetchManager('Class_Del', params).then(res => {
      wx.navigateBack({});
    });
  },

  checkboxChange: function(e) {

  }



})