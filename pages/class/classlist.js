var appInstance = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //班级列表
    classList: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.sendRequestToGetClassList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  showTopTips: function() {
    wx.navigateTo({
      url: 'classadd/classadd',
    })
  },

  /**
   * 接口-获取班级信息
   */
  sendRequestToGetClassList: function() {
    var that = this;
    appInstance.network.fetchManager('Class_List', '').then(res => {
      var tempList = res.list;
      tempList.map(function(item) {
        var items = item;
        if (appInstance.util.isEmpty(items.ablum) != true) {
          let imgInfo = {
            url: items.ablum,
            width: 150,
            height: 150
          };

          items.ablum = appInstance.appUntilManager("fixedImgUrl", imgInfo);
        }
      });

      this.setData({
        classList: tempList,
      })
    });
  }
})