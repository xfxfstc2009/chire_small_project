
//获取应用实例
const appInstance = getApp()
let Charts =  require('../../utils/charts/wxchart/wxcharts.js')
var lineChart = null;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 1. 科目
    subjectList: [],
    // 2. 老师
    teacherList: [],
    // 3. banner
    banners: [],
    // 4.下拉加载
    loadtxt: '炽热',
    isShowLoading: true,
    loading: false,
    // 5. 工具
    toolList: [],
    // 7.获取箴言
    mottoList: [],
    // 8. 获取活动信息
    activityList: [],
    // 9.获取资费
    priceMenuList:[],
    // 10.图表
    classSelected:"",
    classList:[],
    showChart:true,
    // 11. 是否显示video
    showVideo:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.loadAllInterfaceManager();

  },

  loadAllInterfaceManager: function() {
    // 1. 获取当前的banner
    this.getBannerListManger();
    // 2. 获取当前的滚动信息
    this.getSubjectListManager();
    // // 3. 获取活动信息
    this.getActivityListManager();
    // // 4. 获取老师信息
    this.getTeacherListManager();
    // 5. 获取箴言
    this.getZhenyanManager();
    // 6. 获取工具列表
    this.sendRequestGetToolList();
    // 7.设置资费列表
    this.getPriceMenuListManager();
    // 8.获取所有的班级
    this.sendRequestToGetAllClassList();
    // 9. 获取是否有视频
    this.getVideoListManager();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.setData({
      isShowLoading: false
    })

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.setData({
      loadtxt: 'smart',
      loading: true,
      isShowLoading: true
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },


  // Interface 接口
  /**
   * 1.banner
   */
  getBannerListManger() {
    var that = this;
    var params = {
      invalid: "1"
    };
    appInstance.appFetchManager("BannerList", params).then(res => {
      var tempArr = res.list;

      tempArr.map(function(item) {
        let imgInfo = {
          url: item.img,
          width: wx.getSystemInfoSync().windowWidth,
          height: 180
        };

        item.picUrl = appInstance.appUntilManager("fixedImgUrl", imgInfo);
      });
      console.log(tempArr);
      that.setData({
        banners: tempArr
      });
    });
  },
  /**
   * 1.1 banner 点击事件
   */
  bannerActionClick: function(e) {
    console.log(e.currentTarget.id);
    wx.navigateTo({
      url: '../banner/bannerDetail/banner_detail?id='+e.currentTarget.id,
    })
  },

  /**
   * 2.科目
   */
  getSubjectListManager() {
    var that = this;
    appInstance.appFetchManager("SubjectList", "").then(res => {
      var tempArr = res.list;
      tempArr.map(function(item) {
        let imgInfo = {
          url: item.img,
          width: 300,
          height: 150
        };

        item.picUrl = appInstance.appUntilManager("fixedImgUrl", imgInfo);
        item.name = item.name;
        item.id = item.id;
      });

      that.setData({
        subjectList: tempArr
      });
    });
  },

  /**
   * 2.2科目点击方法
   */
  subjectSelectedManager: function(e) {
    console.log("科目点击");

  },

  /**
   * 3获取老师信息
   */
  getTeacherListManager() {
    var that = this;
    appInstance.appFetchManager("Teacher_list", "").then(res => {
      var tempList = res.list;

      tempList.map(function(item) {
        let imgInfo = {
          url: item.avatar,
          width: 300,
          height: 150
        };

        item.avatar = appInstance.appUntilManager("fixedImgUrl", imgInfo);
      });

      that.setData({
        
        teacherList: tempList,
      })
    });
  },

  /**
   * 3.1 老师点击方法
   */
  teacherActionManager: function(e) {
    var infoId = e.currentTarget.id;
    var mainUrl = "https://www.chire.net/chireh5/TeacherInfo/teacherInfo.html";

    var params = "id#" + infoId;
    wx.navigateTo({
      url: '../../utils/webview/webview?url=' + mainUrl + "&params=" + params
    });
  },



  /**
   * 4.1 教学工具点击
   */
  sendRequestGetToolList: function() {
    var params = {};
    var that = this;
    appInstance.network.fetchManager('ToolList', params).then(res => {
      var tempList = res.list;

      tempList.map(function(item) {
        let imgInfo = {};

        if (item.important == 1) { // 重要
          imgInfo = {
            url: item.img,
            width: parseInt(wx.getSystemInfoSync().windowWidth * 0.9),
            height: 200
          };
        } else { // 不重要
          imgInfo = {
            url: item.img,
            width: parseInt(wx.getSystemInfoSync().windowWidth * 0.46),
            height: 200
          };
        }

        item.img = appInstance.appUntilManager("fixedImgUrl", imgInfo);
      });

      this.setData({
        toolList: tempList,
      })
    });
  },

  tapToolManager: function(e) {
    console.log(e);
    if (e.currentTarget.id == '日语翻译') {
      wx.navigateTo({
        url: '../studytool/translate/translate',
      })
    } else if (e.currentTarget.id == '50音图') {
      
        var mainUrl = "https://www.chire.net/50toneMap.html";
    
        wx.navigateTo({ url: '../../utils/webview/webview?url=' + mainUrl })
    } else if (e.currentTarget.id == '天气预报') {
      var mainUrl = "https://www.chire.net/chireh5/Weather/weather.html";
      wx.navigateTo({
        url: '../../utils/webview/webview?url=' + mainUrl
      })
    } else if (e.currentTarget.id == '成绩查询') {
      wx.navigateTo({
        url: '../cj/cjtypes/cjtypes',
      })
    } else if (e.currentTarget.id == '资质查询') {
      wx.navigateTo({
        url: '../qualifications/qualifications',
      })
    } else if (e.currentTarget.id == '教学地址') {
      wx.navigateTo({
        url: '../map/map',
      })
    }
  },

  // 5. 获取箴言
  getZhenyanManager() {
    var that = this;

    appInstance.appFetchManager("MottoList", "").then(res => {
      that.setData({
        mottoList: res.list,
      })
    });
  },



  // 5. 获取活动列表
  getActivityListManager() {
    var that = this;
    appInstance.appFetchManager("ActivityList", {}).then(res => {
  
      res.list.map(function (item) {
        item.time = appInstance.util.dateManager('Y-m-d', item.timeInter);
      })

      that.setData({
        activityList: res.list,
      })

      that.setData({
        loadtxt: 'smart',
        loading: true,
        isShowLoading: false
      })
    });
  },

  activityTap:function(res){
    console.log(res.currentTarget.id);
    wx.navigateTo({
      url: '../activity/activityDetail/activitydetail?id='+res.currentTarget.id,
    })
  },



  // 5. 获取资费列表
  getPriceMenuListManager() {
    var that = this;
    var params = {};
    appInstance.appFetchManager("PriceMenuList", params).then(res => {
      that.setData({
        priceMenuList:res.list,
      })
    });
  },

    // 5. 获取video列表
  getVideoListManager() {
      var that = this;
   
      var paramas = {"page":"1","size":"10"};
      appInstance.network.fetchManager('Video_List', paramas).then(res => {
        var tempList = res.list;
        var tempShow = false;
        if (tempList.length > 0){
          tempShow = true;
        } 
        that.setData({
          showVideo : tempShow
        });
        console.log(this.data.showVideo);
    })
  },
  tapVideoManager(){
    wx.navigateTo({
      url: '../video/video'
    })
  },

  // 7.获取当前的数据表
  sendRequestToGetAllClassList:function(){
    var that = this;
    var params = {};
    appInstance.appFetchManager("Class_List", params).then(res => {
      if (res.list != null){
        var first = res.list[0];
        that.classSelected = first.id;
        
        that.sendRequestToGetChengjiManager();
        that.setData({
          classList: res.list,
          classSelected:first.id,
        })
      }
      
    });
  },

  sendRequestToGetChengjiManager:function(){
    var that = this;
    var params = {classId:this.classSelected};

    appInstance.appFetchManager("Exam_Chart_List", params).then(res => {
      var tempStudentList = [];
      res.chartList.map(function (item) {
        var singleInfo = {
          name : item.studentModel.name,
          data : item.info,
          format: function (val) {
            return val.toFixed(2) + '分';
          }
        }
        tempStudentList.push(singleInfo);
      });

      if (res.dateList.length > 0 && tempStudentList.length > 0){
        that.lineChartManager(res.dateList, tempStudentList);
        console.log("有数据 ");
        this.showChart = true;
      }else {
        console.log("没数据");
        this.showChart = false;
      }
      
    });
  },
  segmentListTap:function(e){
  
   this.setData({
     classSelected: e.currentTarget.id,
   })
   this.classSelected = e.currentTarget.id;

   this.sendRequestToGetChengjiManager();
   
  },

  lineChartManager:function(titleList,scoreList){
    lineChart = new Charts({
      canvasId: 'areacanvas',
      type: 'line',
      animation: true,  //是否开启动画
      enableScroll: true,
      categories: titleList.length>0?titleList:[],
      series: scoreList.length > 0?scoreList:[],
      xAxis: {   //是否隐藏x轴分割线
        disableGrid: true,
      },
      yAxis: {
        title: '学生每次测验分数',
        format: function (val) {
          return val.toFixed(1);
        },
      },
      width: wx.getSystemInfoSync().windowWidth, //图表展示内容宽度
      height: 300, //图表展示内容高度
      dataLabel: true,    //是否在图表上直接显示数据
      dataPointShape: true, //是否在图标上显示数据点标志
      extra: {
        lineStyle: 'curve'  //曲线
      },
    });
  },
  touchstart: function (e) {
    lineChart.showToolTip(e, {
      // background: '#7cb5ec',
      format: function (item, category) {
        return category + ' ' + item.name + ':' + item.data
      }
    });
    lineChart.scrollStart(e);//开始滚动
  },

  touchmove: function (e) {
    lineChart.scroll(e);
  },

  touchend: function (e) {


    lineChart.scrollEnd(e);

  },

})